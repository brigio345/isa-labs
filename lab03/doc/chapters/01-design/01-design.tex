\graphicspath{{./chapters/01-design/figures/}}

\chapter{Design}
\label{chap:design}

The \emph{RISC-L} is a processor architecture based on the RISC-V
\emph{RV32I Base Instruction Set}.

\bigskip
RISC-L main characteristics include:
\begin{itemize}
	\item Instruction set: RV32I Base Instruction Set (except for
		\texttt{FENCE}, \texttt{ECALL} and \texttt{EBREAK} instructions)
		plus optional \texttt{ABS} instruction
	\item Memory addressing modes: dedicated Load \& Store instructions only
	\item Data types and parallelism: integer, 32 bits
	\item Endianness: little endian
	\item Datapath: in-order 5 stages pipeline
	\item Control unit: hardwired
\end{itemize}

\section{Outside}

\subsection{Interface}
The RISC-L core is designed to be inserted in an environment which provides:
\begin{itemize}
	\item the \textbf{clock signal}
	\item the \textbf{instruction memory}
	\item the \textbf{data memory}
\end{itemize}

\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.5\textwidth]{top_mounted}}
	\caption{The RISC-L core inserted in the appropriate environment.}
	\label{fig:env_top}
\end{figure}

\subsubsection{Memories}
Both instruction and data memory must support \textbf{asynchronous reading} and
the \textbf{delay} of the response must be below \textbf{one clock cycle}.

The \textbf{data memory} must also provide \textbf{synchronous writing} capabilities.

\bigskip
Table \ref{tab:i_mem_specs} and \ref{tab:d_mem_specs} show the required memory
specifications in detail.

\begin{table}[H]
	\centering
	\begin{tabular}{llll}
		\hline
		\rowcolor{gray!50}
		\textbf{Access type} & \textbf{Data size} & \textbf{Address} & \textbf{Timing} \\
		\hline
		Read & Word & Word aligned & Asynchronous \\
		\hline
	\end{tabular}
	\caption{Instruction memory specifications.}
	\label{tab:i_mem_specs}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{lllll}
		\hline
		\rowcolor{gray!50}
		\textbf{Access type} & \textbf{Data size} & \textbf{Address} & \textbf{Timing} & \textbf{Control signal} \\
		\hline
		Read & Word & Word aligned & Asynchronous & \texttt{RD = "11"} \\
		\rowcolor{gray!25}
		Read & Half word & Half word aligned & Asynchronous & \texttt{RD = "10"} \\
		Read & Byte & Any & Asynchronous & \texttt{RD = "01"} \\
		\rowcolor{gray!25}
		Write & Word & Word aligned & Synchronous & \texttt{WR = "11"} \\
		Write & Half word & Half word aligned & Synchronous & \texttt{WR = "10"} \\
		\rowcolor{gray!25}
		Write & Byte & Any & Synchronous & \texttt{WR = "01"} \\
		\hline
	\end{tabular}
	\caption{Data memory specifications.}
	\label{tab:d_mem_specs}
\end{table}

\subsection{Instruction set}
The RISC-L instruction set is the \textbf{RV32I Base Instruction Set} (except for
\texttt{FENCE}, \texttt{ECALL} and \texttt{EBREAK} instructions) and it
optionally supports the \texttt{ABS} custom instruction.

\bigskip
There are \textbf{32 general purpose integer 32-bits registers} (\texttt{x0-x31})
which can be used as sources or destination of instructions.

\texttt{x0} has a special behavior: when it is used as source it always
\textbf{evaluates to 0} and when it is used as destination the writeback
has no effect.

\bigskip
Figure \ref{fig:encoding} shows the encoding of each instruction type, while
tables \ref{tab:r_type_inst} to \ref{tab:j_type_inst} display all the supported
instructions.

\begin{figure}[H]
	\centering
	\includegraphics[width=.75\textwidth]{encoding}
	\caption{The RISC-L instruction encoding.}
	\label{fig:encoding}
\end{figure}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\texttt{SLL} & \texttt{RF(RD) <= RF(RS1) logic shifted to the left by uns(RF(RS2)) bits} \\
		\rowcolor{gray!25}
		\texttt{SRL} & \texttt{RF(RD) <= RF(RS1) logic shifted to the right by uns(RF(RS2)) bits} \\
		\texttt{SRA} & \texttt{RF(RD) <= RF(RS1) arithmetic shifted to the right by uns(RF(RS2)) bits} \\
		\rowcolor{gray!25}
		\texttt{ADD} & \texttt{RF(RD) <= sig(RF(RS1)) + sig(RF(RS2))} \\
		\texttt{SUB} & \texttt{RF(RD) <= sig(RF(RS1)) - sig(RF(RS2))} \\
		\rowcolor{gray!25}
		\texttt{AND} & \texttt{RF(RD) <= RF(RS1) AND RF(RS2)} \\
		\texttt{OR} & \texttt{RF(RD) <= RF(RS1) OR RF(RS2)} \\
		\rowcolor{gray!25}
		\texttt{XOR} & \texttt{RF(RD) <= RF(RS1) XOR RF(RS2)} \\
		\texttt{SLT} & \texttt{RF(RD) <= 1 when (sig(RF(RS1)) < sig(RF(RS2))) else 0} \\
		\rowcolor{gray!25}
		\texttt{SLTU} & \texttt{RF(RD) <= 1 when (uns(RF(RS1)) < uns(RF(RS2))) else 0} \\
		(\texttt{ABS}) & \texttt{RF(RD) <= abs\_val(sig(RF(RS1)))} \\
		\hline
	\end{tabular}
	\caption{R-type instructions supported by RISC-L.}
	\label{tab:r_type_inst}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\texttt{ADDI} & \texttt{RF(RD) <= sig(RF(RS)) + sig(I\_IMM)} \\
		\rowcolor{gray!25}
		\texttt{ANDI} & \texttt{RF(RD) <= RF(RS) AND uns(I\_IMM)} \\
		\texttt{ORI} & \texttt{RF(RD) <= RF(RS) OR uns(I\_IMM)} \\
		\rowcolor{gray!25}
		\texttt{XORI} & \texttt{RF(RD) <= RF(RS) XOR uns(I\_IMM)} \\
		\texttt{SLLI} & \texttt{RF(RD) <= RF(RS) logic shifted to the left by uns(low\_5\_bit(I\_IMM)) bits} \\
		\rowcolor{gray!25}
		\texttt{SRLI} & \texttt{RF(RD) <= RF(RS) logic shifted to the right by uns(low\_5\_bit(I\_IMM)) bits} \\
		\texttt{SRAI} & \texttt{RF(RD) <= RF(RS) arithmetic shifted to the right by uns(low\_5\_bits(I\_IMM)) bits} \\
		\rowcolor{gray!25}
		\texttt{SLTI} & \texttt{RF(RD) <= 1 when (sig(RF(RS1)) < sig(I\_IMM)) else 0} \\
		\texttt{SLTIU} & \texttt{RF(RD) <= 1 when (uns(RF(RS1)) < uns(I\_IMM)) else 0} \\
		\rowcolor{gray!25}
		\texttt{LB} & \texttt{RF(RD) <= sig(low\_byte(MEM(RF(RS) + sig(I\_IMM))))} \\
		\texttt{LH} & \texttt{RF(RD) <= sig(low\_half(MEM(RF(RS) + sig(I\_IMM))))} \\
		\rowcolor{gray!25}
		\texttt{LW} & \texttt{RF(RD) <= MEM(RF(RS) + sig(I\_IMM))} \\
		\texttt{LBU} & \texttt{RF(RD) <= uns(low\_byte(MEM(RF(RS) + sig(I\_IMM))))} \\
		\rowcolor{gray!25}
		\texttt{LHU} & \texttt{RF(RD) <= uns(low\_half(MEM(RF(RS) + sig(I\_IMM))))} \\
		\texttt{JALR} & \texttt{PC <= RF(RS) + sig(I\_IMM); RF(RD) <= PC + 4} \\
		\hline
	\end{tabular}
	\caption{I-type instructions supported by RISC-L.}
	\label{tab:i_type_inst}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\rowcolor{gray!25}
		\texttt{SB} & \texttt{MEM(RF(RS) + sig(S\_IMM)) <= low\_byte(RF(RD))} \\
		\texttt{SH} & \texttt{MEM(RF(RS) + sig(S\_IMM)) <= low\_half(RF(RD))} \\
		\rowcolor{gray!25}
		\texttt{SW} & \texttt{MEM(RF(RS) + sig(S\_IMM)) <= RF(RD)} \\
		\hline
	\end{tabular}
	\caption{S-type instructions supported by RISC-L.}
	\label{tab:s_type_inst}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\texttt{BEQ} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (RF(RS1) = RF(RS2)) else (PC + 4)} \\
		\rowcolor{gray!25}
		\texttt{BNE} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (RF(RS1) != RF(RS2)) else (PC + 4)} \\
		\texttt{BLT} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (sig(RF(RS1)) < sig(RF(RS2))) else (PC + 4)} \\
		\rowcolor{gray!25}
		\texttt{BGE} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (sig(RF(RS1)) >= sig(RF(RS2))) else (PC + 4)} \\
		\texttt{BLTU} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (uns(RF(RS1)) < uns(RF(RS2))) else (PC + 4)} \\
		\rowcolor{gray!25}
		\texttt{BGEU} & \texttt{PC <= (PC + 4 + sig(B\_IMM)) when (uns(RF(RS1)) >= uns(RF(RS2))) else (PC + 4)} \\
		\hline
	\end{tabular}
	\caption{B-type instructions supported by RISC-L.}
	\label{tab:b_type_inst}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\texttt{LUI} & \texttt{RF(RD) <= U\_IMM logic shifted to the left by 12 bits} \\
		\rowcolor{gray!25}
		\texttt{AUIPC} & \texttt{RF(RD) <= PC + 4 + (U\_IMM logic shifted to the left by 12 bits)} \\
		\hline
	\end{tabular}
	\caption{U-type instructions supported by RISC-L.}
	\label{tab:u_type_inst}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\rowcolor{gray!50}
		\textbf{Symbol} & \textbf{Behavior} \\
		\hline
		\texttt{JAL} & \texttt{PC <= PC + 4 + sig(J\_IMM); RF(RD) <= PC + 4} \\
		\hline
	\end{tabular}
	\caption{J-type instructions supported by RISC-L.}
	\label{tab:j_type_inst}
\end{table}

\subsubsection{Assembler}
The assembly code can be converted to machine code by the assembler located in
\texttt{assembly/rars.jar}. It is a build of \href{https://github.com/TheThirdOne/rars}{\underline{RARS}},
customized in order to support the \texttt{ABS} instruction.

In \texttt{assembly/code} there are also some example programs.

\section{Architecture}
The RISC-L core is made up of two components: an hardwired \textbf{control unit}
and a pipelined \textbf{datapath}.

\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.35\textwidth]{architecture}}
	\caption{The RISC-L architecture.}
	\label{fig:architecture}
\end{figure}

\subsection{Datapath}
The RISC-L datapath is organized as an \textbf{in-order 5 stages} pipeline,
supporting data forwarding and anticipated branch target computation.

\bigskip
The pipeline stages are:
\begin{enumerate}
	\item \textbf{Fetch stage} (IF):
		\begin{itemize}
			\item select program counter according to previous branch
				taken status
			\item read instruction memory at address corresponding
				to program counter and forward the read data to
				the next stage
			\item compute next sequential program counter
		\end{itemize}
	\item \textbf{Decode stage} (ID):
		\begin{itemize}
			\item forward data from following stages, if necessary
			\item extract data from encoded instruction
			\item extend immediate operands
			\item read from registerfile
			\item compute branch target
			\item compare source addresses with 0 and with destination
				address of other instructions in the pipeline
			\item compare values read from registerfile
		\end{itemize}
	\item \textbf{Execute stage} (EX):
		\begin{itemize}
			\item forward data from following stages, if necessary
			\item execute the selected operation
		\end{itemize}
	\item \textbf{Memory stage} (MEM):
		\begin{itemize}
			\item forward data from following stages, if necessary
			\item read/write data memory
		\end{itemize}
	\item \textbf{Writeback stage} (WB):
		\begin{itemize}
			\item write instruction result to register file
		\end{itemize}
\end{enumerate}

At the \textbf{steady state} (when no hazard happens) all the 5 stages operates
in parallel on different data, completing one instruction per clock cycle, as
shown in figure \ref{fig:pipeline_steady}.
\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.75\textwidth]{pipeline_steady}}
	\caption{The RISC-L pipeline at the steady state.}
	\label{fig:pipeline_steady}
\end{figure}

\subsection{Control unit}
The RISC-L control unit operates in the \textbf{decode stage} of each instruction
and is in charge of the generation of control signals which depend on:
\begin{itemize}
	\item \underline{instruction code} $\Rightarrow$ \textbf{instruction decoding}
	\item \underline{other instructions} in the pipeline $\Rightarrow$
		\textbf{hazard management}
\end{itemize}

\subsubsection{Hazard management}

\paragraph{Control hazards} \mbox{} \\
At the steady state the fetch unit automatically updates the program counter
with the address of the next sequential instruction.
When the instruction in execution is a \textbf{taken branch} this automatic
mechanism leads to the fetch of a wrong instruction: this is a
\textbf{control hazard}.

\bigskip
In order to solve control hazards, the control unit \textbf{converts
the instruction following a taken branch into a \texttt{NOP}} (disabling any
possible write, both to the register file and to the data memory, and disabling
any possible taken branch), since that instruction shouldn't have been fetched,
and informs the fetch unit to \textbf{use the target value} computed in the
decode stage as next program counter. This solution introduces therefore a
\textbf{penalty of one clock cycle} for each taken branch.

\bigskip
If a conditional branch is not taken the execution can proceed without any
penalty to pay: this technique is called \textbf{static untaken branch prediction}.

\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.75\textwidth]{ctrl_hazard}}
	\caption{The RISC-L pipeline when a control hazard occurs.}
	\label{fig:ctrl_hazard}
\end{figure}

\paragraph{Data hazards} \mbox{} \\
In an in-order pipeline the only possible data hazard is the \textbf{Read After
Write} hazard, which occurs when an instruction $I_B$ source operand matches
another instruction $I_A$ destination operand and $I_B$ enters the pipeline
before $I_A$ exits it.

\bigskip
The RISC-L solves these hazards forwarding data or inserting stalls, depending on
$I_A$ and $I_B$ instruction types and their relative position in the pipeline.

\bigskip
A \textbf{stall} consists in disabling:
\begin{itemize}
	\item writes to both register file and data memory
	\item branches
	\item load to registers between decode and fetch units
\end{itemize}

\bigskip
The \textbf{producer result types} are:
\begin{enumerate}
	\item \textbf{ALU}: it is produced by the ALU, therefore it will be
		ready after the EX stage
	\item \textbf{LOAD}: it is loaded from the data memory, therefore it
		will be ready after the MEM stage
	\item \textbf{NULL}: it does not affect the registerfile, therefore it
		cannot cause any data hazard
\end{enumerate}

\bigskip
The \textbf{consumer operand types} are:
\begin{enumerate}
	\item \textbf{ID}: it is needed in the ID stage
	\item \textbf{EX}: it is needed in the EX stage
	\item \textbf{MEM}: it is needed in the MEM stage
\end{enumerate}

\bigskip
Table \ref{tab:data_hazard} displays the data hazard management in detail.

\begin{table}[H]
	\centering
	\begin{tabular}{cccc}
		\hline
		\rowcolor{gray!50}
		\textbf{Producer result type} & \textbf{Producer instruction stage} &
			\textbf{Consumer operand type} & \textbf{Hazard solution} \\
		\hline
		ALU &	EX &	ID &	Stall \\
		\rowcolor{gray!25}
		ALU &	MEM &	ID &	Forward \\
		ALU &	WB &	ID &	Forward \\
		\hdashline
		\rowcolor{gray!25}
		ALU & 	EX &	EX &	Forward \\
		ALU & 	MEM &	EX &	Forward \\
		\rowcolor{gray!25}
		ALU & 	WB &	EX &	Forward \\
		\hdashline
		ALU & 	EX &	MEM &	Forward \\
		\rowcolor{gray!25}
		ALU & 	MEM &	MEM &	Forward \\
		ALU & 	WB &	MEM &	Forward \\
		\hdashline
		\rowcolor{gray!25}
		LOAD & 	EX &	ID &	Stall \\
		LOAD & 	MEM &	ID &	Stall \\
		\rowcolor{gray!25}
		LOAD & 	WB &	ID &	Forward \\
		\hdashline
		LOAD & 	EX &	EX &	Stall \\
		\rowcolor{gray!25}
		LOAD & 	MEM &	EX &	Forward \\
		LOAD & 	WB &	EX &	Forward \\
		\hdashline
		\rowcolor{gray!25}
		LOAD & 	EX &	MEM &	Forward \\
		LOAD & 	MEM &	MEM &	Forward \\
		\rowcolor{gray!25}
		LOAD & 	WB &	MEM &	Forward \\
		\hline
	\end{tabular}
	\caption{Data hazard management (producer instruction stage is where
		$I_A$ is located when $I_B$ is in ID stage)}
	\label{tab:data_hazard}
\end{table}

\section{Implementation}
The RISC-L has been implemented in \texttt{VHDL}; most of the code is written in a
\textbf{behavioral} style: this makes it more future proof for two main reasons:
\begin{itemize}
	\item better \textbf{maintainability}: since behavioral code is much
		more readable, it is easier for the designer to find possible
		bugs and modifying existing code
	\item bigger \textbf{design space} for synthesis tools: the behavioral
		code specifies what the ciruit should do and the tool decides
		how this should be done and it is free to apply any optimization:
		advancement in the tools technology can be reflected in better
		optimized circuit
\end{itemize}

\subsection{Datapath}
The RISC-L datapath is made up of \textbf{one unit for each pipeline stage}.
Between each stage and the previous one there are some \textbf{pipeline registers},
which contain both control and data signals and are loaded with new values every
clock cycle.

There is also a group of registers between the decode and the fetch units, which
can be disabled in case of data hazards.

\bigskip
Figure \ref{fig:datapath} shows an overview of the datapath internals.

\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.75\textwidth]{datapath}}
	\caption{The RISC-L datapath.}
	\label{fig:datapath}
\end{figure}

\subsubsection{Forwarding subsystem}
The forwarding subsystem is distributed in ID, EX and MEM stages.

If it is not possible to forward a certain data to a certain stage, the control
unit would insert a stall, until it is possible to ensure that all data is
up-to-date when it is used.

Figures \ref{fig:fwd_a} and \ref{fig:fwd_b} show the forwarding subsystem alone,
extracted from the datapath (the forwarding of A operand ends in EX stage since
there is no instruction requiring its value in MEM stage).
\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.5\textwidth]{fwd_a}}
	\caption{The RISC-L forwarding subsystem for A operand.}
	\label{fig:fwd_a}
\end{figure}
\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.8\textwidth]{fwd_b}}
	\caption{The RISC-L forwarding subsystem for B operand.}
	\label{fig:fwd_b}
\end{figure}

\subsection{Control unit}
\subsubsection{Design style considerations}
The RISC-L control unit is made up of different components (as shown in figure
\ref{fig:control_unit}):
\begin{itemize}
	\item \textbf{Instruction decoder}: combinational circuit which translates
		the code of the instruction in the decode stage into control signals
	\item \textbf{Data forwarder}: combinational circuit which generates the
		signals controlling the forwarding subsystem
	\item \textbf{Stall generator}: combinational circuit which introduces
		stalls in the pipeline, when it is necessary for ensuring
		correct results
\end{itemize}

It is worth noting that the control unit generates all the control signals
needed by any instruction, in every pipeline stage: the \textbf{correct timing}
of these signals is then ensured by the \textbf{pipeline registers}.

\bigskip
The implementation style follows the \textbf{hardwired} principles: it is an
ad-hoc combinational circuit (except for the configuration register), therefore
it allows to get \underline{better performance} and \underline{lower power}
consumption (with respect to other styles, such as microprogrammed control units)
at the expense of \underline{lower maintainability}.

\bigskip
Since the RISC-L is quite a simple core, the hardwired design style had been the
natural choice.

\begin{figure}[H]
	\centering
	\frame{\includegraphics[width=.7\textwidth]{control_unit}}
	\caption{The RISC-L control unit.}
	\label{fig:control_unit}
\end{figure}

