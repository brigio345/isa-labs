	.data
	.align 2
var:
	.word 34

	.text
	.globl __start

__start:
	la x4,var
	addi x1,x1,-1	# decrement x1
	sw x1,0(x4)	# store the random value
	lw x2,0(x4)	# load the random value to be forwarded
	bnez x2,__start	# generate the hazard: x2 fxom LOADED currently in EX stage
	xor x3,x1,x2	# random instruction

