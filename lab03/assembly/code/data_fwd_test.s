	.data
	.align 2
var:
	.word 0
	
	.text
	.align 2
	.globl __start
__start:
	la x10,var
	addi x1,x0,185
	addi x2,x0,1234
	add x3,x1,x2	# forward x1 from ALUOUT (MEM) to ID and x2 from ALUOUT (EX) to EX
	add x4,x1,x2	# forward x1 from ALUOUT (WB) to ID and x2 from ALUOUT (MEM) to ID
	addi x5,x1,56	# read x1 from RF
	sw x2,0(x10)	# read x2 from RF
	lw x6,0(x10) 
	xor x7,x6,x2	# stall (cannot forward from LOADED(EX)) + forward from LOADED(MEM)
	xor x8,x6,x0	# forward x6 from LOADED(WB)
	xor x9,x6,x1	# read x6 from RF
	bne x9,x0,__start	# forward x9 from ALUOUT(EX) to ID
end:
	j end
