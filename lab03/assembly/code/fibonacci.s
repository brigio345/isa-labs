	.data
	.align 2

	.text
	.globl __start

__start:
	addi a0,zero,10
	jal ra,fibonacci

end:
	j end

# compute the n-th Fibonacci number (n -> a0; return -> a0)
fibonacci:
	addi t0,a0,0		# backup n
	addi a0,zero,0		# set return to 0
	beq t0,zero,fib_ret	# return 0 if n = 0
	addi a0,zero,1		# set return to 1
	beq t0,a0,fib_ret	# return 1 if n = 1

	addi t1,zero,0		# initialize addend
	addi t2,t0,-1		# initialize counter
fib_loop:
	add t0,a0,zero		# save previous Fibonacci number
	add a0,a0,t1		# compute new Fibonacci number
	add t1,t0,zero		# restore previous Fibonacci number
	addi t2,t2,-1		# update counter
	bne t2,zero,fib_loop

fib_ret:
	jalr ra

