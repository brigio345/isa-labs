	.data
stack:
	.space 256

	.text
	.globl __start

__start:
	addi sp,zero,0
	addi a0,zero,1
	jal push	# push 1
	addi a0,zero,2
	jal push	# push 2
	addi a0,zero,3
	jal push	# push 3
	jal pop		# pop 3
	jal pop		# pop 2
	addi a0,zero,4
	jal push	# push 4
	jal pop		# pop 4
	jal pop		# pop 1
	jal pop		# pop empty -> 0

end:
	j end

# push a0 to the stack if it's not full (256 elements)
push:
	addi t0,zero,1024
	bge sp,t0,push_ret	# check if stack is not full
	la t0,stack
	add t0,t0,sp
	sw a0,0(t0)		# store to stack
	addi sp,sp,4		# update stack pointer
push_ret:
	jr ra			# return

# pop to a0 from stack if it's not empty (otherwise a0 is set to 0)
pop:
	addi a0,zero,0		# set return value to 0
	beq sp,zero,pop_ret	# if stack is empty return 0
	addi sp,sp,-4	# update stack pointer
	la t0,stack
	add t0,t0,sp
	lw a0,0(t0)	# load from stack
pop_ret:
	jr ra		# return

