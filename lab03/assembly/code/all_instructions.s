	.data
	.align 2
var:
	.word -573
	
	.text
	.align 2
	.globl __start
__start:
	# set random values
	addi x1,x0,257
	addi x2,x0,672
	addi x3,x0,-824
	addi x5,x0,2

	# R-type instructions
	sll x4,x1,x5
	srl x4,x1,x5
	sra x4,x4,x5
	add x4,x1,x3
	sub x4,x1,x2
	and x4,x1,x2
	or x4,x1,x2
	xor x4,x1,x1
	slt x4,x1,x3
	slt x4,x3,x1
	sltu x4,x1,x2
	sltu x4,x2,x1
	abs x4,x2
	abs x4,x3

	# I-type instructions
	addi x4,x3,15
	andi x4,x3,666
	ori x4,x1,315
	xori x4,x2,22
	slli x4,x1,3
	srli x4,x1,2
	srai x4,x3,2
	slti x4,x1,257
	slti x4,x1,256
	sltiu x4,x3,-823
	sltiu x4,x3,-824
	la x5,var
	lb x4,0(x5)
	lh x4,0(x5)
	lw x4,0(x5)
	lbu x4,0(x5)
	lhu x4,0(x5)
	la x4,target0
	jalr x4,x4,0
	xor x4,x1,x1

target0:
	# S-type instructions
	sb x1,0(x5)
	sh x2,2(x5)
	sw x3,4(x5)

	# B-type instructions
	beq x1,x0,end
	bne x1,x1,end
	blt x1,x1,end
	bge x1,x2,end
	bltu x3,x1,end
	bgeu x1,x3,end

	# U-type instructions
	lui x4,0x54
	auipc x4,0x54

	# J-type instructions
end:
	jal end

	addi x0,x0,0

