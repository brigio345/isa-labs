library ieee;
use ieee.std_logic_1164.all;

-- utilities: collection of common general purpose functions
package utilities is
	function sign_extend (
		I_DATA:		std_logic_vector;
		I_MSB:		integer;
		I_LENGTH:	integer
	)
	return std_logic_vector;

	function zero_extend (
		I_DATA:		std_logic_vector;
		I_LENGTH:	integer
	)
	return std_logic_vector;

	function log2 (
		i:	natural
	)
	return integer;

	function log2_ceil (
		i:	natural
	)
	return integer;
end package utilities;

package body utilities is
	function sign_extend (
		I_DATA:		std_logic_vector;
		I_MSB:		integer;
		I_LENGTH:	integer
	)
	return std_logic_vector is
		variable tmp:	std_logic_vector(I_DATA'length - 1 downto 0);
	begin
		tmp := I_DATA;

		return (I_LENGTH - 1 downto I_MSB + 1 => tmp(I_MSB)) & tmp(I_MSB downto 0);
	end function sign_extend;

	function zero_extend (
		I_DATA:		std_logic_vector;
		I_LENGTH:	integer
	)
	return std_logic_vector is
		variable tmp:	std_logic_vector(I_DATA'length - 1 downto 0);
	begin
		tmp := I_DATA;

		return (I_LENGTH - 1 downto tmp'length => '0') & tmp;
	end function zero_extend;

	function log2(i : natural) return integer is
		variable temp    : integer := i;
		variable ret_val : integer := 0; 
	begin					
		while temp > 1 loop
			ret_val := ret_val + 1;
			temp    := temp / 2;     
		end loop;
					
		return ret_val;
	end function;

	function log2_ceil(i : natural) return integer is
		variable temp    : integer := i;
		variable ret_val : integer := 0; 
	begin					
		while temp > 1 loop
			ret_val := ret_val + 1;
			temp    := temp / 2;     
		end loop;

		if (2**ret_val < i) then
			ret_val := ret_val + 1;
		end if;
					
		return ret_val;
	end function;
end package body utilities;

