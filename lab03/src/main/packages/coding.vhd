library ieee;
use ieee.std_logic_1164.all;

-- coding: contains all the constants related to instruction coding
package coding is
	-- Instruction fields sizes
	constant INST_SZ:	integer := 32;
	constant OPCODE_SZ:	integer := 7;
	constant RF_ADDR_SZ:	integer := 5;
	constant RF_DATA_SZ:	integer := 32;
	constant FUNCT3_SZ:	integer := 3;

	constant OPCODE_START:	integer := 0;
	constant OPCODE_END:	integer := OPCODE_START + OPCODE_SZ - 1;
	subtype OPCODE_RANGE is natural range OPCODE_END downto OPCODE_START;

	constant DST_START:	integer := OPCODE_END + 1;
	constant DST_END:	integer := DST_START + RF_ADDR_SZ - 1;
	subtype DST_RANGE is natural range DST_END downto DST_START;

	constant FUNCT3_START:	integer := DST_END + 1;
	constant FUNCT3_END:	integer := FUNCT3_START + FUNCT3_SZ - 1;
	subtype FUNCT3_RANGE is natural range FUNCT3_END downto FUNCT3_START;

	constant SRC1_START:	integer := FUNCT3_END + 1;
	constant SRC1_END:	integer := SRC1_START + RF_ADDR_SZ - 1;
	subtype SRC1_RANGE is natural range SRC1_END downto SRC1_START;

	constant SRC2_START:	integer := SRC1_END + 1;
	constant SRC2_END:	integer := SRC2_START + RF_ADDR_SZ - 1;
	subtype SRC2_RANGE is natural range SRC2_END downto SRC2_START;

	-- R-type instructions
	constant OPCODE_RTYPE:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0110011";

	constant FUNCT7_SZ:	integer := 7;

	constant FUNCT7_START:	integer := SRC2_END + 1;
	constant FUNCT7_END:	integer := FUNCT7_START + FUNCT7_SZ - 1;
	subtype FUNCT7_RANGE is natural range FUNCT7_END downto FUNCT7_START;

	constant FUNCT_SZ:	integer := FUNCT3_SZ + FUNCT7_SZ;

	constant FUNCT_ADD:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000000";
	constant FUNCT_SUB:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0100000000";
	constant FUNCT_SLL:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000001";
	constant FUNCT_SLT:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000010";
	constant FUNCT_SLTU:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000011";
	constant FUNCT_XOR:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000100";
	constant FUNCT_SRL:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000101";
	constant FUNCT_SRA:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0100000101";
	constant FUNCT_OR:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000110";
	constant FUNCT_AND:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "0000000111";
	constant FUNCT_ABS:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "1111111111";

	-- I(L/J)-type instructions
	constant OPCODE_ITYPE:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0010011";
	constant OPCODE_ILTYPE:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0000011";
	constant OPCODE_JALR:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "1100111";

	constant I_IMM_SZ:	integer := 12;

	constant I_IMM_START:	integer := SRC1_END + 1;
	constant I_IMM_END:	integer := I_IMM_START + I_IMM_SZ - 1;
	subtype I_IMM_RANGE is natural range I_IMM_END downto I_IMM_START;

	-- IL-type instructions
	constant FUNCT_LB:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "000";
	constant FUNCT_LH:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "001";
	constant FUNCT_LW:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "010";
	constant FUNCT_LBU:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "100";
	constant FUNCT_LHU:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "101";

	-- I-type instructions
	subtype SHAMNT_RANGE is natural range 4 downto 0;
	constant FUNCT_ADDI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "000";
	constant FUNCT_SLTI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "010";
	constant FUNCT_SLTIU:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "011";
	constant FUNCT_XORI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "100";
	constant FUNCT_ORI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "110";
	constant FUNCT_ANDI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "111";
	constant FUNCT_SLLI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "001";
	constant FUNCT_SRI:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "101";

	-- S-type instructions
	constant OPCODE_STYPE:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0100011";

	constant S_IMM_L_SZ:	integer := 5;
	constant S_IMM_H_SZ:	integer := 7;
	constant S_IMM_SZ:	integer := S_IMM_L_SZ + S_IMM_H_SZ;

	constant S_IMM_L_START:	integer := OPCODE_END + 1;
	constant S_IMM_L_END:	integer := S_IMM_L_START + S_IMM_L_SZ - 1;
	subtype S_IMM_L_RANGE is natural range S_IMM_L_END downto S_IMM_L_START;
	constant S_IMM_H_START:	integer := SRC2_END + 1;
	constant S_IMM_H_END:	integer := S_IMM_H_START + S_IMM_H_SZ - 1;
	subtype S_IMM_H_RANGE is natural range S_IMM_H_END downto S_IMM_H_START;

	constant FUNCT_SB:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "000";
	constant FUNCT_SH:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "001";
	constant FUNCT_SW:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "010";

	-- B-type instructions
	constant OPCODE_BTYPE:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "1100011";

	constant B_IMM_LL_SZ:	integer := 4;
	constant B_IMM_LH_SZ:	integer := 6;
	constant B_IMM_HL_SZ:	integer := 1;
	constant B_IMM_HH_SZ:	integer := 1;
	constant B_IMM_SZ:	integer := B_IMM_LL_SZ + B_IMM_LH_SZ + B_IMM_HL_SZ + B_IMM_HH_SZ;

	constant B_IMM_HL:	integer := OPCODE_END + 1;
	constant B_IMM_LL_START:integer := B_IMM_HL + 1;
	constant B_IMM_LL_END:	integer := B_IMM_LL_START + B_IMM_LL_SZ - 1;
	subtype B_IMM_LL_RANGE is natural range B_IMM_LL_END downto B_IMM_LL_START;
	constant B_IMM_LH_START:integer := SRC2_END + 1;
	constant B_IMM_LH_END:	integer := B_IMM_LH_START + B_IMM_LH_SZ - 1;
	subtype B_IMM_LH_RANGE is natural range B_IMM_LH_END downto B_IMM_LH_START;
	constant B_IMM_HH:	integer := B_IMM_LH_END + 1;

	constant FUNCT_BEQ:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "000";
	constant FUNCT_BNE:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "001";
	constant FUNCT_BLT:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "100";
	constant FUNCT_BGE:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "101";
	constant FUNCT_BLTU:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "110";
	constant FUNCT_BGEU:	std_logic_vector(FUNCT3_SZ - 1 downto 0) := "111";

	-- U-type instructions
	constant U_IMM_SZ:	integer := 20;

	constant U_IMM_START:	integer := DST_END + 1;
	constant U_IMM_END:	integer := U_IMM_START + U_IMM_SZ - 1;
	subtype U_IMM_RANGE is natural range U_IMM_END downto U_IMM_START;

	constant OPCODE_LUI:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0110111";
	constant OPCODE_AUIPC:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "0010111";

	-- J-type instructions
	constant J_IMM_LL_SZ:	integer := 10;
	constant J_IMM_LH_SZ:	integer := 1;
	constant J_IMM_HL_SZ:	integer := 8;
	constant J_IMM_HH_SZ:	integer := 1;
	constant J_IMM_SZ:	integer := J_IMM_LL_SZ + J_IMM_LH_SZ + J_IMM_HL_SZ + J_IMM_HH_SZ;

	constant J_IMM_HL_START:integer := DST_END + 1;
	constant J_IMM_HL_END:	integer := J_IMM_HL_START + J_IMM_HL_SZ - 1;
	subtype J_IMM_HL_RANGE is natural range J_IMM_HL_END downto J_IMM_HL_START;
	constant J_IMM_LH:	integer := J_IMM_HL_END + 1;
	constant J_IMM_LL_START:integer := J_IMM_LH + 1;
	constant J_IMM_LL_END:	integer := J_IMM_LL_START + J_IMM_LL_SZ - 1;
	subtype J_IMM_LL_RANGE is natural range J_IMM_LL_END downto J_IMM_LL_START;
	constant J_IMM_HH:	integer := J_IMM_LL_END + 1;

	constant OPCODE_JAL:	std_logic_vector(OPCODE_SZ - 1 downto 0) := "1101111";

	-- additional ALU instructions
	constant FUNCT_LINK:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "1110000000";
	constant FUNCT_B:	std_logic_vector(FUNCT_SZ - 1 downto 0) := "1110001000";
end coding;

