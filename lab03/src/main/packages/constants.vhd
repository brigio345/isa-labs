package constants is
	constant INCLUDE_ABS:	boolean := true;
	constant I_BASE_ADDR:	natural := 16#0040_0000#;
	constant D_BASE_ADDR:	natural := 16#1001_0000#;
end constants;

