library IEEE;
use IEEE.std_logic_1164.all;
use work.coding.all;
use work.types.all;

-- inst_decoder: generate all ctrl signals for current instruction
entity inst_decoder is
	port (
		-- from ID stage
		I_FUNCT3:	in std_logic_vector(FUNCT3_SZ - 1 downto 0);
		I_FUNCT7:	in std_logic_vector(FUNCT7_SZ - 1 downto 0);
		I_OPCODE:	in std_logic_vector(OPCODE_SZ - 1 downto 0);
		I_EQ:		in std_logic;
		I_LT:		in std_logic;
		I_LTU:		in std_logic;

		-- to ID stage
		O_TAKEN:	out std_logic;
		O_JMP_REG:	out std_logic;
		O_SEL_IMM:	out imm_t;

		-- to EX stage
		O_ALUOP:	out std_logic_vector(FUNCT_SZ - 1 downto 0);
		O_SEL_A_PC:	out std_logic;
		O_SEL_B_IMM:	out std_logic;

		-- to MEM stage
		O_LD:		out std_logic_vector(1 downto 0);
		O_LD_SIGN:	out std_logic;
		O_STR:		out std_logic_vector(1 downto 0);

		-- to WB stage
		O_WB:		out std_logic;

		-- to CU
		O_A_NEEDED_ID:	out std_logic;
		O_B_NEEDED_ID:	out std_logic;
		O_A_NEEDED_EX:	out std_logic;
		O_B_NEEDED_EX:	out std_logic
	);
end inst_decoder;

architecture BEHAVIORAL of inst_decoder is
begin
	process (I_FUNCT3, I_FUNCT7, I_OPCODE, I_EQ, I_LT, I_LTU)
	begin
		case I_OPCODE is
			when OPCODE_RTYPE	=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';		-- not meaningful
				O_SEL_IMM	<= IMM_I_SIG;	-- not meaningful
				O_ALUOP		<= (I_FUNCT7 & I_FUNCT3);
				O_SEL_A_PC	<= '0';		-- A
				O_SEL_B_IMM	<= '0';		-- B
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';	
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '1';
				if ((I_FUNCT7 & I_FUNCT3) = FUNCT_ABS) then
					O_B_NEEDED_EX	<= '0';
				else
					O_B_NEEDED_EX	<= '1';
				end if;

			when OPCODE_ITYPE	=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';		-- not meaningful
				O_SEL_IMM	<= IMM_I_SIG;
				if (I_FUNCT3 = FUNCT_SRI) then
					O_ALUOP	<= (I_FUNCT7 & I_FUNCT3);
				else
					O_ALUOP	<= ((I_FUNCT7'range => '0') & I_FUNCT3);
				end if;
				O_SEL_A_PC	<= '0';		-- A
				O_SEL_B_IMM	<= '1';		-- IMM
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';	
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '1';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_ILTYPE	=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';	-- not meaningful
				O_SEL_IMM	<= IMM_I_SIG;
				O_ALUOP		<= FUNCT_ADD;
				O_SEL_A_PC	<= '0';		-- A
				O_SEL_B_IMM	<= '1';		-- IMM
				case I_FUNCT3 is
					when FUNCT_LB	=>
						O_LD		<= "01";	-- load byte
						O_LD_SIGN	<= '1';		-- signed
					when FUNCT_LH	=>
						O_LD		<= "10";	-- load half
						O_LD_SIGN	<= '1';		-- signed
					when FUNCT_LW	=>
						O_LD		<= "11";	-- load word
						O_LD_SIGN	<= '1';		-- signed
					when FUNCT_LBU	=>
						O_LD		<= "01";	-- load byte
						O_LD_SIGN	<= '0';		-- unsigned
					when FUNCT_LHU	=>
						O_LD		<= "10";	-- load half
						O_LD_SIGN	<= '0';		-- unsigned
					when others	=>
						O_LD		<= "00";	-- no load
						O_LD_SIGN	<= '0';		-- not meaningful
				end case;
				O_STR		<= "00";	-- no store
				O_WB		<= '1';	
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '1';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_JALR	=>
				O_TAKEN		<= '1';
				O_JMP_REG	<= '1';
				O_SEL_IMM	<= IMM_I_SIG;
				O_ALUOP		<= FUNCT_LINK;
				O_SEL_A_PC	<= '0';		-- A
				O_SEL_B_IMM	<= '1';		-- IMM
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';		-- link
				O_A_NEEDED_ID	<= '1';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_STYPE	=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '1';
				O_SEL_IMM	<= IMM_S_SIG;
				O_ALUOP		<= FUNCT_ADD;
				O_SEL_A_PC	<= '0';		-- A
				O_SEL_B_IMM	<= '1';		-- IMM
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				case I_FUNCT3 is
					when FUNCT_SB	=>
						O_STR	<= "01";	-- store byte
					when FUNCT_SH	=>
						O_STR	<= "10";	-- store half
					when FUNCT_SW	=>
						O_STR	<= "11";	-- store word
					when others	=>
						O_STR	<= "00";	-- no store
				end case;
				O_WB		<= '0';	
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '1';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_BTYPE	=>
				case I_FUNCT3 is
					when FUNCT_BEQ	=>
						O_TAKEN	<= I_EQ;
					when FUNCT_BNE	=>
						O_TAKEN	<= (NOT I_EQ);
					when FUNCT_BLT	=>
						O_TAKEN	<= I_LT;
					when FUNCT_BGE	=>
						O_TAKEN	<= (NOT I_LT);
					when FUNCT_BLTU	=>
						O_TAKEN	<= I_LTU;
					when FUNCT_BGEU	=>
						O_TAKEN	<= (NOT I_LTU);
					when others	=>
						O_TAKEN <= '0';
				end case;
				O_JMP_REG	<= '0';
				O_SEL_IMM	<= IMM_B_SIG;
				O_ALUOP		<= FUNCT_ADD;	-- not meaningful
				O_SEL_A_PC	<= '0';		-- not meaningful
				O_SEL_B_IMM	<= '1';		-- not meaningful
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '0';	
				O_A_NEEDED_ID	<= '1';
				O_B_NEEDED_ID	<= '1';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_LUI		=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';		-- not meaningful
				O_SEL_IMM	<= IMM_U_SIG;
				O_ALUOP		<= FUNCT_B;
				O_SEL_A_PC	<= '0';		-- not meaningful
				O_SEL_B_IMM	<= '1';		-- IMM
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_AUIPC	=>
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';	-- not meaningful
				O_SEL_IMM	<= IMM_U_SIG;
				O_ALUOP		<= FUNCT_ADD;
				O_SEL_A_PC	<= '1';		-- PC
				O_SEL_B_IMM	<= '1';		-- IMM
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';

			when OPCODE_JAL		=>
				O_TAKEN		<= '1';
				O_JMP_REG	<= '0';
				O_SEL_IMM	<= IMM_J_SIG;
				O_ALUOP		<= FUNCT_LINK;
				O_SEL_A_PC	<= '0';		-- not meaningful
				O_SEL_B_IMM	<= '1';		-- not meaningful
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '1';
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';

			when others		=>
				-- unsupported instructions: disable writes and branches
				O_TAKEN		<= '0';
				O_JMP_REG	<= '0';		-- not meaningful
				O_SEL_IMM	<= IMM_I_SIG;	-- not meaningful
				O_ALUOP		<= FUNCT_ADD;	-- not meaningful
				O_SEL_A_PC	<= '0';		-- not meaningful
				O_SEL_B_IMM	<= '1';		-- not meaningful
				O_LD		<= "00";	-- no load
				O_LD_SIGN	<= '0';		-- not meaningful
				O_STR		<= "00";	-- no store
				O_WB		<= '0';		-- no write
				O_A_NEEDED_ID	<= '0';
				O_B_NEEDED_ID	<= '0';
				O_A_NEEDED_EX	<= '0';
				O_B_NEEDED_EX	<= '0';
		end case;
	end process;
end BEHAVIORAL;

