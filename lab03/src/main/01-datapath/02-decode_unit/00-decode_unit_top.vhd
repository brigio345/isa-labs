library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.coding.all;
use work.types.all;
use work.utilities.all;

-- decode_unit:
--	* forward data from following stages
--	* extract data from encoded instruction
--	* extend immediate operands
--	* read from registerfile
--	* compute branch target
--	* compare sources with 0 and with destination of other instructions
--		in the pipeline
entity decode_unit is
	port (
		-- I_IR: from IF stage; encoded instruction
		I_IR:			in std_logic_vector(INST_SZ - 1 downto 0);
		I_PC:			in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- I_RDx_DATA: from rf; data read from rf at address O_RDx_ADDR
		I_RD1_DATA:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
		I_RD2_DATA:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- from CU
		I_JMP_REG:		in std_logic;
		I_SEL_IMM:		in imm_t;
		I_SEL_A:		in source_t;
		I_SEL_B:		in source_t;
		I_WB:			in std_logic;

		-- from MEM
		I_ALUOUT_MEM:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- from WB
		I_ALUOUT_WB:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
		I_LOADED_WB:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		I_DST_EX:		in std_logic_vector(RF_ADDR_SZ - 1 downto 0);
		I_DST_MEM:		in std_logic_vector(RF_ADDR_SZ - 1 downto 0);
		I_DST_WB:		in std_logic_vector(RF_ADDR_SZ - 1 downto 0);

		-- O_RDx_ADDR: to rf; address at which rf has to be read
		O_RD1_ADDR:		out std_logic_vector(RF_ADDR_SZ - 1 downto 0);
		O_RD2_ADDR:		out std_logic_vector(RF_ADDR_SZ - 1 downto 0);

		-- O_DST: to WB stage; address at which rf has to be written
		O_DST:			out std_logic_vector(RF_ADDR_SZ - 1 downto 0);

		-- to CU
		O_OPCODE:		out std_logic_vector(OPCODE_SZ - 1 downto 0);
		O_FUNCT3:		out std_logic_vector(FUNCT3_SZ - 1 downto 0);
		O_FUNCT7:		out std_logic_vector(FUNCT7_SZ - 1 downto 0);
		O_EQ:			out std_logic;
		O_LT:			out std_logic;
		O_LTU:			out std_logic;
		O_ZERO_SRC_A:		out std_logic;
		O_ZERO_SRC_B:		out std_logic;
		O_SRC_A_EQ_DST_EX:	out std_logic;
		O_SRC_B_EQ_DST_EX:	out std_logic;
		O_SRC_A_EQ_DST_MEM:	out std_logic;
		O_SRC_B_EQ_DST_MEM:	out std_logic;
		O_SRC_A_EQ_DST_WB:	out std_logic;
		O_SRC_B_EQ_DST_WB:	out std_logic;

		-- to EX stage; ALU operands
		O_A:			out std_logic_vector(RF_DATA_SZ - 1 downto 0);
		O_B:			out std_logic_vector(RF_DATA_SZ - 1 downto 0);
		O_IMM:			out std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- O_TARGET: to IF stage; address of next instruction
		O_TARGET:		out std_logic_vector(RF_DATA_SZ - 1 downto 0)
	);
end decode_unit;

architecture MIXED of decode_unit is
	component id_data_forwarder is
		port (
			-- from CU
			I_SEL_A:		in source_t;
			I_SEL_B:		in source_t;

			-- I_RDx_DATA: read from rf
			I_RD1_DATA:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
			I_RD2_DATA:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

			-- from MEM
			I_ALUOUT_MEM:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

			-- from WB
			I_ALUOUT_WB:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
			I_LOADED_WB:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

			-- forwarded data
			O_A:			out std_logic_vector(RF_DATA_SZ - 1 downto 0);
			O_B:			out std_logic_vector(RF_DATA_SZ - 1 downto 0)
		);
	end component id_data_forwarder;

	signal SRC_A:	std_logic_vector(RF_ADDR_SZ - 1 downto 0);
	signal SRC_B:	std_logic_vector(RF_ADDR_SZ - 1 downto 0);
	signal A:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal B:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM_I:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM_S:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM_B:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM_U:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM_J:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal IMM:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
	signal BASE:	signed(RF_DATA_SZ - 1 downto 0);
	signal TARGET:	std_logic_vector(RF_DATA_SZ - 1 downto 0);
begin
	id_data_forwarder_0: id_data_forwarder
		port map (
			I_SEL_A		=> I_SEL_A,
			I_SEL_B		=> I_SEL_B,
			I_RD1_DATA	=> I_RD1_DATA,
			I_RD2_DATA	=> I_RD2_DATA,
			I_ALUOUT_MEM	=> I_ALUOUT_MEM,
			I_ALUOUT_WB	=> I_ALUOUT_WB,
			I_LOADED_WB	=> I_LOADED_WB,
			O_A		=> A,
			O_B		=> B
		);

	SRC_A	<= I_IR(SRC1_RANGE);
	SRC_B	<= I_IR(SRC2_RANGE);

	-- extract immediates
	IMM_I	<= sign_extend(I_IR(I_IMM_RANGE), I_IMM_SZ - 1, RF_DATA_SZ);
	IMM_S	<= sign_extend(I_IR(S_IMM_H_RANGE) & I_IR(S_IMM_L_RANGE),
		 S_IMM_SZ - 1, RF_DATA_SZ);
	IMM_B	<= sign_extend(I_IR(B_IMM_HH) & I_IR(B_IMM_HL) & I_IR(B_IMM_LH_RANGE) &
		 I_IR(B_IMM_LL_RANGE) & '0', B_IMM_SZ, RF_DATA_SZ);
	IMM_U	<= I_IR(U_IMM_RANGE) & ((RF_DATA_SZ - U_IMM_SZ) - 1 downto 0 => '0');
	IMM_J	<= sign_extend((I_IR(J_IMM_HH) & I_IR(J_IMM_HL_RANGE) &
		     I_IR(J_IMM_LH) & I_IR(J_IMM_LL_RANGE) & '0'), J_IMM_SZ, RF_DATA_SZ);

	with I_SEL_IMM select IMM	<=
		IMM_I	when IMM_I_SIG,
		IMM_S	when IMM_S_SIG,
		IMM_B	when IMM_B_SIG,
		IMM_U	when IMM_U_SIG,
		IMM_J	when others;

	-- rf addresses
	O_RD1_ADDR	<= SRC_A;
	O_RD2_ADDR	<= SRC_B;

	-- outputs to CU
	O_ZERO_SRC_A		<= '1' when (SRC_A = (SRC_A'range => '0')) else '0';
	O_ZERO_SRC_B		<= '1' when (SRC_B = (SRC_B'range => '0')) else '0';
	O_SRC_A_EQ_DST_EX	<= '1' when (SRC_A = I_DST_EX) else '0';
	O_SRC_B_EQ_DST_EX	<= '1' when (SRC_B = I_DST_EX) else '0';
	O_SRC_A_EQ_DST_MEM	<= '1' when (SRC_A = I_DST_MEM) else '0';
	O_SRC_B_EQ_DST_MEM	<= '1' when (SRC_B = I_DST_MEM) else '0';
	O_SRC_A_EQ_DST_WB	<= '1' when (SRC_A = I_DST_WB) else '0';
	O_SRC_B_EQ_DST_WB	<= '1' when (SRC_B = I_DST_WB) else '0';
	O_EQ			<= '1' when (A = B) else '0';
	O_LT			<= '1' when (signed(A) < signed(B)) else '0';
	O_LTU			<= '1' when (unsigned(A) < unsigned(B)) else '0';
	O_OPCODE		<= I_IR(OPCODE_RANGE);
	O_FUNCT3		<= I_IR(FUNCT3_RANGE);
	O_FUNCT7		<= I_IR(FUNCT7_RANGE);

	-- outputs to IF stage
	BASE 		<= signed(I_PC) when (I_JMP_REG = '0') else signed(A);
	TARGET 		<= std_logic_vector(BASE + signed(IMM));
	O_TARGET 	<= TARGET(O_TARGET'left downto 1) & '0';

	-- outputs to EX stage
	O_A	<= A;
	O_B	<= B;
	O_IMM	<= IMM;

	-- outputs to WB stage
	O_DST <= I_IR(DST_RANGE) when (I_WB = '1') else (others => '0');
end MIXED;

