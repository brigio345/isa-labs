library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.coding.all;
use work.types.all;
use work.utilities.all;

-- memory_unit:
--	* forward data from following stages
--	* disable memory access if address exceeds data memory size
--	* align address according to access size
--	* read/write data memory
--	* manage endianness conversions
entity memory_unit is
	port (
		-- from CU
		I_LD:		in std_logic_vector(1 downto 0);
		I_LD_SIGN:	in std_logic;
		I_STR:		in std_logic_vector(1 downto 0);
		I_SEL_DATA:	in source_t;

		-- from EX stage
		I_ADDR:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- from ID stage
		I_DATA:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- data forwarded from EX/MEM stages
		I_LOADED_EX:	in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- from d-memory
		I_RD_DATA:	in std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- to d-memory
		O_ADDR:		out std_logic_vector(RF_DATA_SZ - 1 downto 0);
		O_RD:		out std_logic_vector(1 downto 0);
		O_WR:		out std_logic_vector(1 downto 0);
		O_WR_DATA:	out std_logic_vector(RF_DATA_SZ - 1 downto 0);

		-- to WB stage
		O_LOADED:	out std_logic_vector(RF_DATA_SZ - 1 downto 0)
	);
end memory_unit;

architecture BEHAVIORAL of memory_unit is
	component mem_data_forwarder is
		port (
			-- from CU
			I_SEL_B:	in source_t;

			-- from EX stage forward unit
			I_B:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);

			-- value loaded from memory by the instruction which was in EX
			--	stage when current instruction was in ID stage
			I_LOADED_EX:	in std_logic_vector(RF_DATA_SZ - 1 downto 0);

			-- forwarded data
			O_B:		out std_logic_vector(RF_DATA_SZ - 1 downto 0)
		);
	end component mem_data_forwarder;
begin
	mem_data_forwarder_0: mem_data_forwarder
		port map (
			I_SEL_B		=> I_SEL_DATA,
			I_B		=> I_DATA,
			I_LOADED_EX	=> I_LOADED_EX,
			O_B		=> O_WR_DATA
		);

	O_ADDR	<= I_ADDR;
	O_RD	<= I_LD;
	O_WR	<= I_STR;

	extend: process (I_RD_DATA, I_LD_SIGN, I_LD)
	begin
		if (I_LD_SIGN = '1') then
			case I_LD is
				when "10"	=>
					O_LOADED <= sign_extend(I_RD_DATA, 15, RF_DATA_SZ);
				when "01"	=>
					O_LOADED <= sign_extend(I_RD_DATA, 7, RF_DATA_SZ);
				when others	=>
					O_LOADED <= I_RD_DATA;
			end case;
		else
			O_LOADED <= I_RD_DATA;
		end if;
	end process extend;
end BEHAVIORAL;

