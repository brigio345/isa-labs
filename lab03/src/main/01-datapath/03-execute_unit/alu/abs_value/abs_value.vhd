library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity abs_val is
	generic (
		N_BITS:	natural := 32	
	);
	port (
		I_D:	in std_logic_vector(N_BITS - 1 downto 0);
		O_ABS:	out std_logic_vector(N_BITS - 1 downto 0)
	);
end abs_val;

architecture BEHAVIORAL of abs_val is
begin 
	O_ABS	<= std_logic_vector(abs(signed(I_D)));
end BEHAVIORAL;

