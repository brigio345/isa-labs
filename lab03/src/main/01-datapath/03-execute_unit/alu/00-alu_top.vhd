library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.coding.all;

-- alu: execute an arithmetic or logic operation
entity alu is
	generic (
		N_BIT:		integer := 32;
		INCLUDE_ABS:	boolean := true
	);
	port (
		I_OP:		in std_logic_vector(FUNCT_SZ - 1 downto 0);
		I_A:		in std_logic_vector(N_BIT - 1 downto 0);
		I_B:		in std_logic_vector(N_BIT - 1 downto 0);
		O_DATA:		out std_logic_vector(N_BIT - 1 downto 0)
	);
end alu;

architecture MIXED of alu is
	component abs_val is
		generic (
			N_BITS:	natural := 32	
		);
		port (
			I_D:	in std_logic_vector(N_BITS - 1 downto 0);
			O_ABS:	out std_logic_vector(N_BITS - 1 downto 0)
		);
	end component abs_val;

	-- adder signals
	signal C_IN:	std_logic_vector(0 downto 0);
	signal B_ADDER:	std_logic_vector(N_BIT - 1 downto 0);
	signal SUM:	std_logic_vector(N_BIT - 1 downto 0);

	-- shifter signals
	signal SHAMNT:	integer;

	-- abs_val signals
	signal ABS_V:	std_logic_vector(N_BIT - 1 downto 0);
begin
	abs_val_true_if: if (INCLUDE_ABS) generate
		abs_val_0: abs_val
			generic map (
				N_BITS	=> N_BIT
			)
			port map (
				I_D	=> I_A,
		       		O_ABS	=> ABS_V		
			);
	end generate abs_val_true_if;

	abs_val_false_if: if (NOT INCLUDE_ABS) generate
		ABS_V	<= (others => '0');
	end generate abs_val_false_if;

	SUM <= std_logic_vector(unsigned(I_A) + unsigned(B_ADDER) + unsigned(C_IN));

	-- perform A - B when an SUB is required,
	-- ((NOT B) + 1 = -B)
	-- otherwise perform A - B
	B_ADDER	<= (NOT I_B) when (I_OP = FUNCT_SUB) else I_B;
	C_IN	<= "0" when (I_OP = FUNCT_ADD) else "1";

	SHAMNT	<= to_integer(unsigned(I_B(SHAMNT_RANGE)));

	output_sel: process(I_OP, I_A, I_B, SHAMNT, SUM, ABS_V)
	begin
		O_DATA <= (others => '0');
		case I_OP is
			when FUNCT_SLL	=>
				O_DATA	<= to_stdlogicvector(to_bitvector(I_A) SLL SHAMNT);
			when FUNCT_SRL	=>
				O_DATA	<= to_stdlogicvector(to_bitvector(I_A) SRL SHAMNT);
			when FUNCT_SRA	=>
				O_DATA	<= to_stdlogicvector(to_bitvector(I_A) SRA SHAMNT);
			when FUNCT_ADD | FUNCT_SUB =>
				O_DATA <= SUM;
			when FUNCT_AND	=>
				O_DATA	<= (I_A AND I_B);
			when FUNCT_OR	=>
				O_DATA	<= (I_A OR I_B);
			when FUNCT_XOR	=>
				O_DATA	<= (I_A XOR I_B);
			when FUNCT_SLT	=>
				if (signed(I_A) < signed(I_B)) then
					O_DATA(0) <= '1';
				else
					O_DATA(0) <= '0';
				end if;
			when FUNCT_SLTU	=>
				if (unsigned(I_A) < unsigned(I_B)) then
					O_DATA(0) <= '1';
				else
					O_DATA(0) <= '0';
				end if;
			when FUNCT_B	=>
				O_DATA	<= I_B;
			when FUNCT_ABS	=>
				O_DATA	<= ABS_V;
			when others	=>
				null;
		end case;
	end process output_sel;
end architecture MIXED;

