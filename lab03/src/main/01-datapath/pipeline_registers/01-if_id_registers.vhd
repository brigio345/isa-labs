library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.coding.all;
use work.constants.all;

-- if_id_registers: pipeline registers between IF and ID stages
entity if_id_registers is
	port (
		I_CLK:		in std_logic;
		I_RST:		in std_logic;
		I_EN:		in std_logic;

		-- from IF stage
		I_PC:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
		I_NPC:		in std_logic_vector(RF_DATA_SZ - 1 downto 0);
		I_IR:		in std_logic_vector(INST_SZ - 1 downto 0);

		-- to ID stage
		O_PC:		out std_logic_vector(RF_DATA_SZ - 1 downto 0);
		O_NPC:		out std_logic_vector(RF_DATA_SZ - 1 downto 0);
		O_IR:		out std_logic_vector(INST_SZ - 1 downto 0)
	);
end if_id_registers;

architecture BEHAVIORAL of if_id_registers is
begin
	reg: process (I_CLK)
	begin
		if (I_CLK = '1' AND I_CLK'event) then
			if (I_RST = '1') then
				O_PC	<= std_logic_vector(to_unsigned(I_BASE_ADDR, O_PC'length));
				O_NPC	<= std_logic_vector(to_unsigned(I_BASE_ADDR, O_PC'length));
				O_IR	<= (others => '0');
			elsif (I_EN = '1') then
				O_PC	<= I_PC;
				O_NPC	<= I_NPC;
				O_IR	<= I_IR;
			end if;
		end if;
	end process reg;
end BEHAVIORAL;

