source ~/.synopsys_dc.setup

# analyze_dir:
# 	* Argument(s):
# 		- dir: name of the directory containing source code to be analyzed
# 		- src_format: format of source files to be compiled;
# 			it can be "vhdl" (default) or "verilog"
# 	* Result:
# 		analysis of all the source file recursively found in dir
# 	* Return:
# 		none
proc analyze_dir {dir {src_format "vhdl"}} {
	# recur on every subdirectory of $dir, in reverse alphanumerical order
	foreach sub_dir [lsort -decreasing [glob -nocomplain -type d $dir/*]] {
		analyze_dir $sub_dir
	}

	# analyze every file with the specified format in $dir
	foreach src_file [lsort -decreasing [glob -nocomplain $dir/*.vhd]] {
		analyze -library WORK -format $src_format $src_file
	}
}

# synthesize
#	* Argument(s):
#		- top_entity: name of top level entity to be synthesized
#		- clk_period: maximum delay between inputs and outputs
#			and clock period
#	* Result:
#		Synthesize top_entity
#		- elaborate output (written to $top_entity-YYMMDDHHmm/elaborate.rpt)
#		- post-synthesis netlist (written to $top_entity-YYMMDDHHmm/$top_entity.v)
#		- sdc file (written to $top_entity.sdc)
#		- sdf file (written to $top_entity.sdf)
#		- timing report (written to $top_entity-YYMMDDHHmm/timing.rpt)
#		- power report (written to $top_entity-YYMMDDHHmm/power.rpt)
#		- area report (written to $top_entity-YYMMDDHHmm/area.rpt)
#		- clock gating report (written to $top_entity-YYMMDDHHmm/gating.rpt)
#	* Return:
#		none
proc synthesize {{top_entity "riscv"} {clk_period 2}} {
	set out_dir "$top_entity-[clock format [clock seconds] -format %Y%m%d%H%M]"
	
	exec mkdir $out_dir

	set clk_uncertainty 0.07
	set io_delay 0.5
	set library [get_object_name [index_collection [get_lib] 0]]
	set out_load [load_of $library/BUF_X4/A]

	set power_preserve_rtl_hier_names true

	elaborate $top_entity > "$out_dir/elaborate.rpt"

	# set constraints
	create_clock -name "I_CLK" -period $clk_period I_CLK
	set_dont_touch_network I_CLK
	set_clock_uncertainty $clk_uncertainty [get_clocks I_CLK]
	set_input_delay $io_delay -max -clock I_CLK [remove_from_collection [all_inputs] I_CLK]
	set_output_delay $io_delay -max -clock I_CLK [all_outputs]
	set_load $out_load [all_outputs]
	ungroup -all -flatten
	
	# synthesize
	compile_ultra -gate_clock -retime

	# write outputs
	write -format verilog -hierarchy -output "$out_dir/$top_entity.v"
	write_sdc "$out_dir/$top_entity.sdc"
	write_sdf "$out_dir/$top_entity.sdf"
	report_timing > "$out_dir/timing.rpt"
	report_power > "$out_dir/power.rpt"
	report_area > "$out_dir/area.rpt"
	report_clock_gating > "$out_dir/gating.rpt"
}

