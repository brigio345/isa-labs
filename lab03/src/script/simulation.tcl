set sim_root "."
set asm_root "../assembly"
set library "/software/dk/nangate45/verilog/msim6.5c"

# simulate_riscv
# 	* Argument(s):
# 		- asm_file: assembly source code to be loaded to instruction memory
# 		- run_time: duration (in ns) of the simulation
# 		- sdf_file: sdf file used for generating vcd file; no vcd file
# 			generated if sdf_file is not specified
# 	* Result:
# 		make the RISC-V run asm_file for run_time nanoseconds and
# 		generate vcd file riscv-$sdf_file.vcd (if sdf_file is specified)
# 	* Return:
# 		none
proc simulate_riscv {asm_file {run_time 100} {sdf_file ""}} {
	global sim_root asm_root library

	if {[file exists $asm_file] == 0} {
		puts "asm_file $asm_file not found. Exiting."
		return
	}

	# assemble asm code
	exec java -jar $asm_root/rars.jar a dump .text HexText inst.hex dump .data HexText data.hex $asm_file

	# simulate assembled code
	if {[string compare $sdf_file ""] != 0} {
		vsim -L $library -sdftyp /tb_riscv/dut=$sdf_file tb_riscv
		vcd file riscv-[file tail [file rootname $asm_file]].vcd
		vcd add /tb_riscv/dut/*
		run $run_time ns
	} else {
		vsim -L $library tb_riscv
		if {[file exists $asm_root/riscv.do]} {
			do $asm_root/riscv.do
		} else {
			add wave *
		}

		run $run_time ns
	}
}

# compile_dir:
# 	* Argument(s):
# 		- dir: name of the directory containing source code to be compiled
# 		- file_extension: extension of source files to be compiled;
# 			it can be "vhd" (default) for VHDL code or
# 			"v" for Verilog code
# 	* Result:
# 		compilation of all the source file recursively found in dir
# 	* Return:
# 		none
proc compile_dir {dir {file_extension "vhd"}} {
	# recur on every subdirectory of $dir, in reverse alphanumerical order
	foreach subdir [lsort -decreasing [glob -nocomplain -type d $dir/*]] {
		compile_dir $subdir
	}

	# compile every file with the specified extension in $dir
	foreach src_file [glob -nocomplain $dir/*.$file_extension] {
		vcom -pedanticerrors -check_synthesis -bindAtCompile $src_file
	}
}

