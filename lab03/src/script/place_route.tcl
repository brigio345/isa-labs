# Initialize
set IN_DIR "../../syn/riscv-abs"
set init_top_cell "riscv"
set init_verilog "${IN_DIR}/${init_top_cell}.v"
set in_sdc_filename "${IN_DIR}/${init_top_cell}.sdc"
set LIB_DIR /software/dk/nangate45/liberty
set MyTimingLib ${LIB_DIR}/NangateOpenCellLibrary_typical_ecsm_nowlm.lib
set LEF_DIR /software/dk/nangate45/lef
set init_lef_file ${LEF_DIR}/NangateOpenCellLibrary.lef
set init_design_netlisttype "verilog"
set init_design_settop 1
set aspect_ratio 1.0
set target_row_utilization 0.6
set CustomDelayLimit 1000
set CustomNetDelay 1000.0ps
set CustomNetLoad 0.5pf
set CustomInputTranDelay 0.1ps
set MycapTable $LEF_DIR/captables/NCSU_FreePDK_45nm.capTbl
set init_gnd_net {VSS}
set init_pwr_net {VDD}
set init_mmmc_file ../mmm_design.tcl
set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default

# Importing the design
init_design
setIoFlowFlag 0

# Structuring the floorplan
floorPlan -coreMarginsBy die -site FreePDK45_38x28_10R_NP_162NW_34O -r 1 0.6 5 5 5 5

# Inserting Power Rings (and Stripes)
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
setAddRingMode -ring_target default -extend_over_row 0 -ignore_rows 0 \
	-avoid_short 0 -skip_crossing_trunks none -stacked_via_top_layer metal10 \
	-stacked_via_bottom_layer metal1 -via_using_exact_crossover_size 1 \
	-orthogonal_only true -skip_via_on_pin {  standardcell } \
	-skip_via_on_wire_shape {  noshape }
addRing -nets {VDD VSS} -type core_rings -follow core \
	-layer {top metal9 bottom metal9 left metal10 right metal10} \
	-width {top 0.8 bottom 0.8 left 0.8 right 0.8} \
	-spacing {top 0.8 bottom 0.8 left 0.8 right 0.8} \
	-offset {top 1.8 bottom 1.8 left 1.8 right 1.8} \
	-center 1 -extend_corner {} -threshold 0 -jog_distance 0 \
	-snap_wire_center_to_grid None
setAddStripeMode -ignore_block_check false -break_at none \
	-route_over_rows_only false -rows_without_stripes_only false \
	-extend_to_closest_target none -stop_at_last_wire_for_area false \
	-partial_set_thru_domain false -ignore_nondefault_domains false \
	-trim_antenna_back_to_shape none -spacing_type edge_to_edge \
	-spacing_from_block 0 -stripe_min_length 0 -stacked_via_top_layer metal10 \
	-stacked_via_bottom_layer metal1 -via_using_exact_crossover_size false \
	-split_vias false -orthogonal_only true \
	-allow_jog { padcore_ring  block_ring }
addStripe -nets {VDD VSS} -layer metal10 -direction vertical -width 0.8 \
	-spacing 0.8 -set_to_set_distance 40 -start_from left -start_offset 35 \
	-switch_layer_over_obs false -max_same_layer_jog_length 2 \
	-padcore_ring_top_layer_limit metal10 \
	-padcore_ring_bottom_layer_limit metal1 -block_ring_top_layer_limit metal10 \
	-block_ring_bottom_layer_limit metal1 -use_wire_group 0 \
	-snap_wire_center_to_grid None -skip_via_on_pin {  standardcell } \
	-skip_via_on_wire_shape {  noshape }

# Standard cell power routing
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst * -module {}
globalNetConnect VSS -type pgpin -pin VSS -inst * -module {}
setPlaceMode -prerouteAsObs {1 2 3 4 5 6 7 8}
setSrouteMode -viaConnectToShape { noshape }
sroute -connect { blockPin padPin padRing corePin floatingStripe } \
	-layerChangeRange { metal1(1) metal10(10) } -blockPinTarget { nearestTarget } \
	-padPinPortConnect { allPort oneGeom } -padPinTarget { nearestTarget } \
	-corePinTarget { firstAfterRowEnd } \
	-floatingStripeTarget { blockring padring ring stripe ringpin blockpin followpin } \
	-allowJogging 1 -crossoverViaLayerRange { metal1(1) metal10(10) } \
	-nets { VSS VDD } -allowLayerChange 1 -blockPin useLef \
	-targetViaLayerRange { metal1(1) metal10(10) }

# Placement
setMultiCpuUsage -localCpu 8 -cpuPerRemoteHost 1 -remoteHost 0 -keepLicense true
setDistributeHost -local
setPlaceMode -fp false
placeDesign

# Pin assignment
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Top -layer 1 -spreadType side \
	-pin {{I_CLK} {I_RST} \
	{I_D_RD_DATA[0]} {I_D_RD_DATA[1]} {I_D_RD_DATA[2]} {I_D_RD_DATA[3]} \
	{I_D_RD_DATA[4]} {I_D_RD_DATA[5]} {I_D_RD_DATA[6]} {I_D_RD_DATA[7]} \
	{I_D_RD_DATA[8]} {I_D_RD_DATA[9]} {I_D_RD_DATA[10]} {I_D_RD_DATA[11]} \
	{I_D_RD_DATA[12]} {I_D_RD_DATA[13]} {I_D_RD_DATA[14]} {I_D_RD_DATA[15]} \
	{I_D_RD_DATA[16]} {I_D_RD_DATA[17]} {I_D_RD_DATA[18]} {I_D_RD_DATA[19]} \
	{I_D_RD_DATA[20]} {I_D_RD_DATA[21]} {I_D_RD_DATA[22]} {I_D_RD_DATA[23]} \
	{I_D_RD_DATA[24]} {I_D_RD_DATA[25]} {I_D_RD_DATA[26]} {I_D_RD_DATA[27]} \
	{I_D_RD_DATA[28]} {I_D_RD_DATA[29]} {I_D_RD_DATA[30]} {I_D_RD_DATA[31]} \
	{O_D_RD[0]} {O_D_RD[1]} \
	{O_D_WR[0]} {O_D_WR[1]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Left -layer 1 -spreadType side \
	-pin {{I_I_RD_DATA[0]} {I_I_RD_DATA[1]} {I_I_RD_DATA[2]} {I_I_RD_DATA[3]} \
	{I_I_RD_DATA[4]} {I_I_RD_DATA[5]} {I_I_RD_DATA[6]} {I_I_RD_DATA[7]} \
	{I_I_RD_DATA[8]} {I_I_RD_DATA[9]} {I_I_RD_DATA[10]} {I_I_RD_DATA[11]} \
	{I_I_RD_DATA[12]} {I_I_RD_DATA[13]} {I_I_RD_DATA[14]} {I_I_RD_DATA[15]} \
	{I_I_RD_DATA[16]} {I_I_RD_DATA[17]} {I_I_RD_DATA[18]} {I_I_RD_DATA[19]} \
	{I_I_RD_DATA[20]} {I_I_RD_DATA[21]} {I_I_RD_DATA[22]} {I_I_RD_DATA[23]} \
	{I_I_RD_DATA[24]} {I_I_RD_DATA[25]} {I_I_RD_DATA[26]} {I_I_RD_DATA[27]} \
	{I_I_RD_DATA[28]} {I_I_RD_DATA[29]} {I_I_RD_DATA[30]} {I_I_RD_DATA[31]} \
	{O_D_ADDR[0]} {O_D_ADDR[1]} {O_D_ADDR[2]} {O_D_ADDR[3]} {O_D_ADDR[4]} \
	{O_D_ADDR[5]} {O_D_ADDR[6]} {O_D_ADDR[7]} {O_D_ADDR[8]} {O_D_ADDR[9]} \
	{O_D_ADDR[10]} {O_D_ADDR[11]} {O_D_ADDR[12]} {O_D_ADDR[13]} {O_D_ADDR[14]} \
	{O_D_ADDR[15]} {O_D_ADDR[16]} {O_D_ADDR[17]} {O_D_ADDR[18]} {O_D_ADDR[19]} \
	{O_D_ADDR[20]} {O_D_ADDR[21]} {O_D_ADDR[22]} {O_D_ADDR[23]} {O_D_ADDR[24]} \
	{O_D_ADDR[25]} {O_D_ADDR[26]} {O_D_ADDR[27]} {O_D_ADDR[28]} {O_D_ADDR[29]} \
	{O_D_ADDR[30]} {O_D_ADDR[31]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Bottom -layer 1 \
	-spreadType side -pin {{O_D_WR_DATA[0]} {O_D_WR_DATA[1]} {O_D_WR_DATA[2]} \
	{O_D_WR_DATA[3]} {O_D_WR_DATA[4]} {O_D_WR_DATA[5]} {O_D_WR_DATA[6]} \
	{O_D_WR_DATA[7]} {O_D_WR_DATA[8]} {O_D_WR_DATA[9]} {O_D_WR_DATA[10]} \
	{O_D_WR_DATA[11]} {O_D_WR_DATA[12]} {O_D_WR_DATA[13]} {O_D_WR_DATA[14]} \
	{O_D_WR_DATA[15]} {O_D_WR_DATA[16]} {O_D_WR_DATA[17]} {O_D_WR_DATA[18]} \
	{O_D_WR_DATA[19]} {O_D_WR_DATA[20]} {O_D_WR_DATA[21]} {O_D_WR_DATA[22]} \
	{O_D_WR_DATA[23]} {O_D_WR_DATA[24]} {O_D_WR_DATA[25]} {O_D_WR_DATA[26]} \
	{O_D_WR_DATA[27]} {O_D_WR_DATA[28]} {O_D_WR_DATA[29]} {O_D_WR_DATA[30]} \
	{O_D_WR_DATA[31]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Right -layer 1 \
	-spreadType side -pin {{O_I_RD_ADDR[0]} {O_I_RD_ADDR[1]} {O_I_RD_ADDR[2]} \
	{O_I_RD_ADDR[3]} {O_I_RD_ADDR[4]} {O_I_RD_ADDR[5]} {O_I_RD_ADDR[6]} \
	{O_I_RD_ADDR[7]} {O_I_RD_ADDR[8]} {O_I_RD_ADDR[9]} {O_I_RD_ADDR[10]} \
	{O_I_RD_ADDR[11]} {O_I_RD_ADDR[12]} {O_I_RD_ADDR[13]} {O_I_RD_ADDR[14]} \
	{O_I_RD_ADDR[15]} {O_I_RD_ADDR[16]} {O_I_RD_ADDR[17]} {O_I_RD_ADDR[18]} \
	{O_I_RD_ADDR[19]} {O_I_RD_ADDR[20]} {O_I_RD_ADDR[21]} {O_I_RD_ADDR[22]} \
	{O_I_RD_ADDR[23]} {O_I_RD_ADDR[24]} {O_I_RD_ADDR[25]} {O_I_RD_ADDR[26]} \
	{O_I_RD_ADDR[27]} {O_I_RD_ADDR[28]} {O_I_RD_ADDR[29]} {O_I_RD_ADDR[30]} \
	{O_I_RD_ADDR[31]} \
	{O_I_RD_ADDR[0]} {O_I_RD_ADDR[1]} {O_I_RD_ADDR[2]} {O_I_RD_ADDR[3]} \
	{O_I_RD_ADDR[4]} {O_I_RD_ADDR[5]} {O_I_RD_ADDR[6]} {O_I_RD_ADDR[7]} \
	{O_I_RD_ADDR[8]} {O_I_RD_ADDR[9]} {O_I_RD_ADDR[10]} {O_I_RD_ADDR[11]} \
	{O_I_RD_ADDR[12]} {O_I_RD_ADDR[13]} {O_I_RD_ADDR[14]} {O_I_RD_ADDR[15]} \
	{O_I_RD_ADDR[16]} {O_I_RD_ADDR[17]} {O_I_RD_ADDR[18]} {O_I_RD_ADDR[19]} \
	{O_I_RD_ADDR[20]} {O_I_RD_ADDR[21]} {O_I_RD_ADDR[22]} {O_I_RD_ADDR[23]} \
	{O_I_RD_ADDR[24]} {O_I_RD_ADDR[25]} {O_I_RD_ADDR[26]} {O_I_RD_ADDR[27]} \
	{O_I_RD_ADDR[28]} {O_I_RD_ADDR[29]} {O_I_RD_ADDR[30]} {O_I_RD_ADDR[31]}}
setPinAssignMode -pinEditInBatch false

# Post Clock-Tree-Synthesis (CTS) optimization
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postCTS
optDesign -postCTS -hold

# Place filler
getFillerMode -quiet
addFiller -cell FILLCELL_X8 FILLCELL_X4 FILLCELL_X32 FILLCELL_X2 FILLCELL_X16 \
	FILLCELL_X1 -prefix FILLER

# Routing
setNanoRouteMode -quiet -timingEngine {}
setNanoRouteMode -quiet -routeWithSiPostRouteFix 0
setNanoRouteMode -quiet -drouteStartIteration default
setNanoRouteMode -quiet -routeTopRoutingLayer default
setNanoRouteMode -quiet -routeBottomRoutingLayer default
setNanoRouteMode -quiet -drouteEndIteration default
setNanoRouteMode -quiet -routeWithTimingDriven false
setNanoRouteMode -quiet -routeWithSiDriven false
routeDesign -globalDetail

# Post routing optimization
setAnalysisMode -analysisType onChipVariation
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postRoute
optDesign -postRoute -hold
saveDesign ${init_top_cell}.enc

# Parasitics extraction
reset_parasitics
extractRC
rcOut -setload ${init_top_cell}.setload -rc_corner my_rc
rcOut -setres ${init_top_cell}.setres -rc_corner my_rc
rcOut -spf ${init_top_cell}.spf -rc_corner my_rc
rcOut -spef ${init_top_cell}.spef -rc_corner my_rc

# Timing analysis
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -postRoute -pathReports -drvReports -slackReports -numPaths 50 \
	-prefix ${init_top_cell}_postRoute -outDir timingReports
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -preCTS -hold -pathReports -slackReports -numPaths 50 \
	-prefix ${init_top_cell}_preCTS -outDir timingReports

# Design analysis and verification
verifyConnectivity -type all -error 1000 -warning 50
setVerifyGeometryMode -area { 0 0 0 0 } -minWidth true -minSpacing true \
	-minArea true -sameNet true -short true -overlap true -offRGrid false \
	-offMGrid true -mergedMGridCheck true -minHole true -implantCheck true \
	-minimumCut true -minStep true -viaEnclosure true -antenna false \
	-insuffMetalOverlap true -pinInBlkg false -diffCellViol true \
	-sameCellViol false -padFillerCellsOverlap true -routingBlkgPinOverlap true \
	-routingCellBlkgOverlap true -regRoutingOnly false -stackedViasOnRegNet false \
	-wireExt true -useNonDefaultSpacing false -maxWidth true \
	-maxNonPrefLength -1 -error 1000
verifyGeometry
setVerifyGeometryMode -area { 0 0 0 0 }
reportGateCount -level 5 -limit 100 -outfile ${init_top_cell}.gateCount
saveNetlist ${init_top_cell}.v
all_hold_analysis_views 
all_setup_analysis_views 
write_sdf  -ideal_clock_network ${init_top_cell}.sdf

