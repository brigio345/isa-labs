library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.utilities.all;

-- inst_mem: instruction memory, filled by a process reading from file
entity inst_mem is
	generic (
		MEM_FILE:	string := "inst.hex";
		RAM_DEPTH:	integer := 512;
		I_SIZE:		integer := 32;
		BASE_ADDR:	natural := 16#0000_0000#
	);
	port (
		I_ADDR:	in std_logic_vector(I_SIZE - 1 downto 0);
		O_DATA:	out std_logic_vector(I_SIZE - 1 downto 0)
	);
end inst_mem;

architecture BEHAVIORAL of inst_mem is
	type mem_t is array (BASE_ADDR / 4 to (BASE_ADDR + RAM_DEPTH) / 4 - 1) of
		std_logic_vector(I_SIZE - 1 downto 0);

	signal MEM:	mem_t;
	signal ADDR:	integer;
begin
	-- fill the instruction memory with the firmware
	fill_mem: process
		file mem_fp:		text;
		variable file_line:	line;
		variable tmp_data_u:	std_logic_vector(I_SIZE - 1 downto 0);
	begin
		file_open(mem_fp, MEM_FILE, READ_MODE);

		for I in MEM'range loop
			if (not endfile(mem_fp)) then
				readline(mem_fp, file_line);
				hread(file_line, tmp_data_u);
				MEM(I) <= tmp_data_u;       
			else
				MEM(I) <= (others => '0');
			end if;
		end loop;

		file_close(mem_fp);

		wait;
	end process fill_mem;

	ADDR <= to_integer(unsigned(I_ADDR)) / 4;

	O_DATA <= MEM(ADDR) when ((ADDR >= MEM'left) and (ADDR <= MEM'right))
		  else (others => '0');
end BEHAVIORAL;

