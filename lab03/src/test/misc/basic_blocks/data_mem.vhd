library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.utilities.all;

-- data_mem: data memory, filled by a process reading from file
entity data_mem is
	generic (
		MEM_FILE:	string := "data.hex";
		DATA_SZ:	integer := 32;
		N_LINES:	integer := 1024;
		BASE_ADDR:	natural := 16#1001_0000#
	);
	port (
		I_CLK:	in std_logic;
		I_RST:	in std_logic;

		I_ADDR:	in std_logic_vector(31 downto 0);
		I_DATA:	in std_logic_vector(DATA_SZ - 1 downto 0);
		I_RD:	in std_logic_vector(1 downto 0);
		I_WR:	in std_logic_vector(1 downto 0);

		O_DATA:	out std_logic_vector(DATA_SZ - 1 downto 0)
	);
end data_mem;

architecture BEHAVIORAL of data_mem is
	type mem_t is array (BASE_ADDR to BASE_ADDR + N_LINES - 1) of
		std_logic_vector(7 downto 0);

	subtype HH_RANGE is natural range DATA_SZ - 1 downto DATA_SZ * 3 / 4;
	subtype HL_RANGE is natural range DATA_SZ * 3 / 4 - 1 downto DATA_SZ / 2;
	subtype LH_RANGE is natural range DATA_SZ / 2 - 1 downto DATA_SZ / 4;
	subtype LL_RANGE is natural range DATA_SZ / 4 - 1 downto 0;

	signal MEM_CURR:	mem_t;
	signal MEM_NEXT:	mem_t;
	signal ADDR:		integer;
	signal VALID:		boolean;
	signal RD:		std_logic_vector(1 downto 0);
	signal WR:		std_logic_vector(1 downto 0);
begin
	reg_proc: process (I_CLK)
		file mem_fp:		text;
		variable file_line:	line;
		variable tmp_data_u:	std_logic_vector(O_DATA'range);
		variable I:		integer;
	begin
		if (I_CLK = '0' AND I_CLK'event) then
			if (I_RST = '1') then
				file_open(mem_fp, MEM_FILE, READ_MODE);

				I := BASE_ADDR;
				while (I < BASE_ADDR + N_LINES) loop
					if (not endfile(mem_fp)) then
						readline(mem_fp, file_line);
						hread(file_line, tmp_data_u);
						MEM_CURR(I) <= tmp_data_u(LL_RANGE);       
						MEM_CURR(I + 1) <= tmp_data_u(LH_RANGE);       
						MEM_CURR(I + 2) <= tmp_data_u(HL_RANGE);       
						MEM_CURR(I + 3) <= tmp_data_u(HH_RANGE);       
					else
						MEM_CURR(I) <= (others => '0');       
						MEM_CURR(I + 1) <= (others => '0');       
						MEM_CURR(I + 2) <= (others => '0');       
						MEM_CURR(I + 3) <= (others => '0');       
					end if;

					I := I + 4;
				end loop;

				file_close(mem_fp);
			else
				MEM_CURR <= MEM_NEXT;
			end if;
		end if;
	end process reg_proc;

	ADDR <= to_integer(unsigned(I_ADDR));

	VALID <= ((ADDR >= MEM_CURR'left) and (ADDR <= MEM_CURR'right));

	WR <= I_WR when (VALID) else "00";
	RD <= I_RD when (VALID) else "00";

	write: process(MEM_CURR, WR, I_DATA, ADDR)
	begin
		MEM_NEXT <= MEM_CURR;
		case WR is
			when "11"	=>
				MEM_NEXT(ADDR)		<= I_DATA(LL_RANGE);
				MEM_NEXT(ADDR + 1)	<= I_DATA(LH_RANGE);
				MEM_NEXT(ADDR + 2)	<= I_DATA(HL_RANGE);
				MEM_NEXT(ADDR + 3)	<= I_DATA(HH_RANGE);
			when "10"	=>
				MEM_NEXT(ADDR)		<= I_DATA(LL_RANGE);
				MEM_NEXT(ADDR + 1)	<= I_DATA(LH_RANGE);
			when "01"	=>
				MEM_NEXT(ADDR)		<= I_DATA(LL_RANGE);
			when others	=>
				null;
		end case;
	end process write;

	with RD select O_DATA	<=
		MEM_CURR(ADDR + 3) & MEM_CURR(ADDR + 2) & MEM_CURR(ADDR + 1) & MEM_CURR(ADDR)	when "11",
		(HH_RANGE => '0') & (HL_RANGE => '0') & MEM_CURR(ADDR + 1) & MEM_CURR(ADDR)	when "10",
		(HH_RANGE => '0') & (HL_RANGE => '0') & (LH_RANGE => '0') & MEM_CURR(ADDR)	when "01",
		(others => '0')									when others;
		
end BEHAVIORAL;

