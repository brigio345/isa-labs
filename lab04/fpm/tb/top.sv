import uvm_pkg::*;
`include "uvm_macros.svh"
`include "../fpm/src/dut_if.sv"
`include "../fpm/src/DUT.sv"
`include "../fpm/tb/packet_in.sv"
`include "../fpm/tb/packet_out.sv"
`include "../fpm/tb/sequence_in.sv"
`include "../fpm/tb/sequencer.sv"
`include "../fpm/tb/driver.sv"
`include "../fpm/tb/driver_out.sv"
`include "../fpm/tb/monitor.sv"
`include "../fpm/tb/monitor_out.sv"
`include "../fpm/tb/agent.sv"
`include "../fpm/tb/agent_out.sv"
`include "../fpm/tb/refmod.sv"
`include "../fpm/tb/comparator.sv"
`include "../fpm/tb/env.sv"
`include "../fpm/tb/simple_test.sv"

//Top
module top;
  logic clk;
  logic rst;
  
  initial begin
    clk = 0;
    rst = 1;
    #22 rst = 0;
    
  end
  
  always #5 clk = !clk;
  
  logic [1:0] state;
  
  dut_if in(clk, rst);
  dut_if out(clk, rst);
  
  DUT dut(in, out, state);

  initial begin
    `ifdef INCA
      $recordvars();
    `endif
    `ifdef VCS
      $vcdpluson;
    `endif
    `ifdef QUESTA
      $wlfdumpvars();
      set_config_int("*", "recording_detail", 1);
    `endif
    
    uvm_config_db#(input_vif)::set(uvm_root::get(), "*.env_h.mst.*", "vif", in);
    uvm_config_db#(output_vif)::set(uvm_root::get(), "*.env_h.slv.*",  "vif", out);
    
    run_test("simple_test");
  end
endmodule

