library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg_single is
  Port (	
     EN:	In	std_logic;
   	 D:	    In	std_logic;
	 CK:	In	std_logic;
	 RESET:	In	std_logic;
	 Q:	Out	std_logic
	   );
end reg_single;


architecture beh of reg_single is
	
	signal data: std_logic;
begin
	process(ck, RESET)
	begin
		if (RESET = '1') then
			data <= '0';
		elsif ( EN = '1' ) then 
			if (ck'event and ck = '1') then
				data <= D;
			end if;
		end if;
	end process;

	Q <= data;

end beh;
