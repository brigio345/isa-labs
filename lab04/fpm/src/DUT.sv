module DUT(dut_if.port_in in_inter, dut_if.port_out out_inter,
      output enum logic [1:0] {INITIAL,WAIT,SEND} state);
    
    FPmul_dadda_fine dut(.FP_A(in_inter.A), .FP_B(in_inter.B), .clk(in_inter.clk),
        .rst(in_inter.rst), .FP_Z(out_inter.data));

    always_ff @(posedge in_inter.clk)
    begin
        if(in_inter.rst) begin
            in_inter.ready <= 0;
            out_inter.valid <= 0;
            state <= INITIAL;
        end
        else case(state)
                INITIAL: begin
                    in_inter.ready <= 1;
		    out_inter.valid <= 0;
                    state <= WAIT;
                end
                
                WAIT: begin
		    in_inter.ready <= 0;
                    if(in_inter.valid) begin
                        $display("dut: input A = %f, input B = %f, output OUT = %f",
			  $bitstoshortreal(in_inter.A), $bitstoshortreal(in_inter.B),
			  $bitstoshortreal(out_inter.data));
                        $display("dut: input A = %x, input B = %x, output OUT = %x",
			  in_inter.A, in_inter.B, out_inter.data);
                        out_inter.valid <= 1;
                        state <= SEND;
                    end else begin
			out_inter.valid <= 0;
			state <= WAIT;
		    end
                end
                
                SEND: begin
                    if(out_inter.ready) begin
                        in_inter.ready <= 1;
                        out_inter.valid <= 0;
                        state <= WAIT;
                    end else begin
                        in_inter.ready <= 0;
                        out_inter.valid <= 1;
                        state <= SEND;
		    end
                end
        endcase
    end
endmodule: DUT

