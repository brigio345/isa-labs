set proj_root ".." 

set src_vhdl [list                     		\
	"/mbe/src/basic_blocks/full_adder.vhd"	\
	"/mbe/src/basic_blocks/half_adder.vhd"	\
	"/mbe/src/basic_blocks/mux5to1.vhd"	\
	"/mbe/src/utils/mbe_constants.vhd"	\
	"/mbe/src/utils/mbe_types.vhd"		\
	"/mbe/src/op_gen/mbe_encoder.vhd"	\
	"/mbe/src/op_gen/mbe_op_gen.vhd"	\
	"/mbe/src/tree/mbe_d_funcs.vhd"		\
	"/mbe/src/tree/mbe_functions.vhd"	\
	"/mbe/src/tree/mbe_dadda_tree.vhd"	\
	"/mbe/src/mbe_multiplier.vhd"		\
]

foreach vhdl_file $src_vhdl {
	vcom $proj_root$vhdl_file
}

exit

