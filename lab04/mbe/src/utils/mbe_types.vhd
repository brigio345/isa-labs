library ieee;
use ieee.std_logic_1164.all;
use work.mbe_constants.all;

package mbe_types is
	type booth_mat_t is
		array (0 to (N_ROWS - 1)) of
		std_logic_vector((N_COLS - 1) downto 0);
end package mbe_types;

