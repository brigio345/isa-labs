import uvm_pkg::*;
`include "uvm_macros.svh"
`include "../src/dut_if.sv"
`include "../mbe/src/DUT.sv"
`include "../mbe/tb/packet_in.sv"
`include "../mbe/tb/packet_out.sv"
`include "../mbe/tb/sequence_in.sv"
`include "../mbe/tb/sequencer.sv"
`include "../mbe/tb/driver.sv"
`include "../mbe/tb/driver_out.sv"
`include "../mbe/tb/monitor.sv"
`include "../mbe/tb/monitor_out.sv"
`include "../mbe/tb/agent.sv"
`include "../mbe/tb/agent_out.sv"
`include "../mbe/tb/refmod.sv"
`include "../mbe/tb/comparator.sv"
`include "../mbe/tb/env.sv"
`include "../mbe/tb/simple_test.sv"

//Top
module top;
  logic clk;
  logic rst;
  
  initial begin
    clk = 0;
    rst = 1;
    #22 rst = 0;
    
  end
  
  always #5 clk = !clk;
  
  logic [1:0] state;
  
  dut_if in(clk, rst);
  dut_if out(clk, rst);
  
  DUT dut(in, out, state);

  initial begin
    `ifdef INCA
      $recordvars();
    `endif
    `ifdef VCS
      $vcdpluson;
    `endif
    `ifdef QUESTA
      $wlfdumpvars();
      set_config_int("*", "recording_detail", 1);
    `endif
    
    uvm_config_db#(input_vif)::set(uvm_root::get(), "*.env_h.mst.*", "vif", in);
    uvm_config_db#(output_vif)::set(uvm_root::get(), "*.env_h.slv.*",  "vif", out);
    
    run_test("simple_test");
  end
endmodule

