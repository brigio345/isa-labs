import uvm_pkg::*;
`include "uvm_macros.svh"
`include "../add/src/adder.sv"
`include "../add/src/dut_if.sv"
`include "../add/src/DUT.sv"
`include "../add/tb/packet_in.sv"
`include "../add/tb/packet_out.sv"
`include "../add/tb/sequence_in.sv"
`include "../add/tb/sequencer.sv"
`include "../add/tb/driver.sv"
`include "../add/tb/driver_out.sv"
`include "../add/tb/monitor.sv"
`include "../add/tb/monitor_out.sv"
`include "../add/tb/agent.sv"
`include "../add/tb/agent_out.sv"
`include "../add/tb/refmod.sv"
`include "../add/tb/comparator.sv"
`include "../add/tb/env.sv"
`include "../add/tb/simple_test.sv"

//Top
module top;
  logic clk;
  logic rst;
  
  initial begin
    clk = 0;
    rst = 1;
    #22 rst = 0;
    
  end
  
  always #5 clk = !clk;
  
  logic [1:0] state;
  
  dut_if in(clk, rst);
  dut_if out(clk, rst);
  
  DUT sum(in, out, state);

  initial begin
    `ifdef INCA
      $recordvars();
    `endif
    `ifdef VCS
      $vcdpluson;
    `endif
    `ifdef QUESTA
      $wlfdumpvars();
      set_config_int("*", "recording_detail", 1);
    `endif
    
    uvm_config_db#(input_vif)::set(uvm_root::get(), "*.env_h.mst.*", "vif", in);
    uvm_config_db#(output_vif)::set(uvm_root::get(), "*.env_h.slv.*",  "vif", out);
    
    run_test("simple_test");
  end
endmodule

