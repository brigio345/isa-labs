LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_textio.all;
USE std.textio.all;

ENTITY TB_FPmul_dadda_fine IS
END TB_FPmul_dadda_fine ;

ARCHITECTURE tb_arch OF TB_FPmul_dadda_fine IS
	COMPONENT FPmul_dadda_fine IS
	   PORT( 
	      FP_A : IN     std_logic_vector (31 DOWNTO 0);
	      FP_B : IN     std_logic_vector (31 DOWNTO 0);
	      clk  : IN     std_logic;
	      rst  : IN     std_logic;
	      FP_Z : OUT    std_logic_vector (31 DOWNTO 0)
	   );
	END COMPONENT FPmul_dadda_fine;

	SIGNAL FP_A:	std_logic_vector(31 downto 0);
	SIGNAL FP_B:	std_logic_vector(31 downto 0);
	SIGNAL clk:	std_logic := '0';
	SIGNAL rst: std_logic := '0';
	SIGNAL FP_Z:	std_logic_vector(31 downto 0);

	CONSTANT clk_period:	time := 10 ns;
	CONSTANT latency:	natural := 6;

	CONSTANT stimuli_file_name:	string := "./fp_samples.hex";
	CONSTANT expected_file_name:	string := "./fp_prod.hex";
BEGIN
	DUT: FPmul_dadda_fine
		PORT MAP (
			FP_A	=> FP_A,
			FP_B	=> FP_B,
			clk	=> clk,
			rst	=> rst,
			FP_Z	=> FP_Z
		);

	clk <= NOT clk AFTER (clk_period / 2);

	stimuli: PROCESS
		FILE stimuli_file:	TEXT;
		VARIABLE stimulus_line:	LINE;
		VARIABLE stimulus:	std_logic_vector(FP_A'range);
	BEGIN
		WAIT UNTIL ((clk = '1') AND clk'event);
		WAIT FOR clk_period / 4;

		file_open(stimuli_file, stimuli_file_name, READ_MODE);

		WHILE (NOT endfile(stimuli_file)) LOOP
			-- apply stimuli read from file
			readline(stimuli_file, stimulus_line);
			hread(stimulus_line, stimulus);
			FP_A	<= stimulus;
			FP_B	<= stimulus;

			WAIT UNTIL ((clk = '1') AND clk'event);
		END LOOP;

		file_close(stimuli_file);

		WAIT;
	END PROCESS stimuli;

	check: PROCESS
		FILE expected_file:	TEXT;
		VARIABLE expected_line:	LINE;
		VARIABLE expected:	std_logic_vector(FP_Z'range);
	BEGIN
		file_open(expected_file, expected_file_name, READ_MODE);

		-- wait for latency + 1 (for the reset) clock cycles
		FOR i IN 1 TO (latency + 1) LOOP
			WAIT UNTIL ((clk = '1') AND clk'event);
		END LOOP;

		WHILE (NOT endfile(expected_file)) LOOP
			WAIT FOR clk_period / 4;

			-- compare actual output with expected output
			readline(expected_file, expected_line);
			hread(expected_line, expected);
			ASSERT (FP_Z = expected)
			REPORT "Unexpected FP_Z value detected";

			WAIT UNTIL ((clk = '1') AND clk'event);
		END LOOP;

		file_close(expected_file);

		-- force the simulation to complete (useful when running with "run -all")
		REPORT "Simulation completed." SEVERITY FAILURE;
	END PROCESS check;
END tb_arch;

