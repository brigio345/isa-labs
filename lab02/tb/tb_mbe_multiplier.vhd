library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mbe_constants.all;

entity tb_mbe_multiplier is
end entity tb_mbe_multiplier;

architecture tb_arch of tb_mbe_multiplier is
	component mbe_multiplier is
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_x:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_p:	out std_logic_vector(((2 * N_BITS) - 1) downto 0)
		);
	end component mbe_multiplier;

	component lfsr32 is
		port (
			i_seed:	in std_logic_vector(31 downto 0);
			i_ld:	in std_logic;
			i_clk:	in std_logic;
			i_rst:	in std_logic;
			o_d:	out std_logic_vector(31 downto 0)
		);
	end component lfsr32;

	constant CLK_PERIOD:	time := 10 ns;

	signal a:	std_logic_vector((N_BITS - 1) downto 0);
	signal x:	std_logic_vector((N_BITS - 1) downto 0);
	signal seed_a:	std_logic_vector((N_BITS - 1) downto 0);
	signal seed_x:	std_logic_vector((N_BITS - 1) downto 0);
	signal p:	std_logic_vector((2 * N_BITS - 1) downto 0);
	signal ld:	std_logic;
	signal clk:	std_logic;
	signal rst:	std_logic;
begin
	dut: mbe_multiplier
		port map (
			i_a	=> a,
			i_x	=> x,
			o_p	=> p	
		);

	lfsr_a: lfsr32
		port map (
			i_seed	=> seed_a,
			i_ld	=> ld,
			i_clk	=> clk,
			i_rst	=> rst,
			o_d	=> a
		);

	lfsr_x: lfsr32
		port map (
			i_seed	=> seed_x,
			i_ld	=> ld,
			i_clk	=> clk,
			i_rst	=> rst,
			o_d	=> x
		);

	clk_proc: process
	begin
		clk <= '0';
		wait for (CLK_PERIOD / 2);
		clk <= '1';
		wait for (CLK_PERIOD / 2);
	end process clk_proc;

	seed_a	<= ((N_BITS - 1) downto 1 => '0') & '1';
	seed_x	<= (others => '1');
	rst	<= '0';
	ld	<= '1', '0' after CLK_PERIOD;

	check: process
	begin
		if (ld /= '0') then
			wait until (ld = '0');
		end if;

		wait until (clk'event AND clk = '0');

		assert (unsigned(p) = unsigned(a) * unsigned(x))
		report "Wrong product detected.";
	end process check;
end architecture tb_arch;

