library ieee;
use ieee.std_logic_1164.all;

entity lfsr32 is
	port (
		i_seed:	in std_logic_vector(31 downto 0);
		i_ld:	in std_logic;
		i_clk:	in std_logic;
		i_rst:	in std_logic;
		o_d:	out std_logic_vector(31 downto 0)
	);
end lfsr32;

architecture behavioral of lfsr32 is
	signal st_curr: std_logic_vector(31 downto 0);
	signal st_next: std_logic_vector(31 downto 0);
	signal feedback: std_logic;
begin

	state_reg: process (i_clk,i_rst)
	begin
		if (i_rst = '1') then
			st_curr <= (0 => '1', others =>'0');
		elsif (i_clk = '1' and i_clk'event) then
			if (i_ld = '1') then
				st_curr <= i_seed;
			else
				st_curr <= st_next;
			end if;
		end if;
	end process state_reg;

	feedback <= st_curr(31) xor st_curr(22) xor st_curr(2) xor st_curr(0);
	st_next <= feedback & st_curr(31 downto 1);

	o_d <= st_curr;
end behavioral;

