Warning: There are infeasible paths detected in your design that were ignored during optimization. Please run 'report_timing -attributes' and/or 'create_qor_snapshot/query_qor_snapshot -infeasible_paths' to identify these paths.  (OPT-1721)
 
****************************************
Report : constraint
Design : FPmul
Version: O-2018.06-SP4
Date   : Fri Dec  4 13:28:01 2020
****************************************


                                                   Weighted
    Group (max_delay/setup)      Cost     Weight     Cost
    -----------------------------------------------------
    my_clock                     3.89      1.00      3.89
    default                      0.00      1.00      0.00
    -----------------------------------------------------
    max_delay/setup                                  3.89

                              Total Neg  Critical
    Group (critical_range)      Slack    Endpoints   Cost
    -----------------------------------------------------
    my_clock                   265.53         1      3.89
    default                      0.00         0      0.00
    -----------------------------------------------------
    critical_range                                   3.89

                                                   Weighted
    Group (min_delay/hold)       Cost     Weight     Cost
    -----------------------------------------------------
    my_clock (no fix_hold)       0.00      1.00      0.00
    default                      0.00      1.00      0.00
    -----------------------------------------------------
    min_delay/hold                                   0.00


    Constraint                                       Cost
    -----------------------------------------------------
    max_transition                                   0.00 (MET)
    max_capacitance                                  0.00 (MET)
    max_delay/setup                                  3.89 (VIOLATED)
    critical_range                                   3.89 (VIOLATED)


1
