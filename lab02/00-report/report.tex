\documentclass[12pt]{article}

\usepackage{listings} 
\usepackage[table]{xcolor}
\usepackage{float}
\usepackage{hyperref}
\usepackage{babel}
\usepackage{graphicx}

\graphicspath{{./graphics/}}

\hypersetup {
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=blue
}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=8
}

\begin{document}
\begin{titlepage}
	\vspace{2cm}
	\centerline{
	\includegraphics[width=.3\textwidth]{./logopoli}}  
	\centerline{\Large Politecnico di Torino}
	\vspace{1.5cm}
	\centerline{\Huge\bfseries\sf Integrated Systems Architecture}
	\bigskip
	\centerline{\Huge\sf Lab 2: digital arithmetic}
	\vspace{2cm}
	\centerline{\LARGE Master degree in Electronic Engineering}
	\vspace{1.5 cm}
	\centerline{\Large Authors}
	\vspace{0.3 cm}
	\centerline{Brignone Giovanni (274148)}
	\centerline{Buccellato Pietro (263416)}
	\centerline{Vacca Eleonora (263570)}
	\centerline{Vossoughian Mohammadamin (257679)}
	\vspace{2cm}
	\centerline{\today}
	\vspace{1cm}
	\centerline{\textbf{Full sources:} \url{https://gitlab.com/brigio345/isa-labs/-/tree/master/lab02}}
\end{titlepage}

\section{Digital arithmetic and logic synthesis}
A possible architecture of a floating point (\texttt{IEEE 754-2008}) multiplier
is shown in figure~\ref{fig:fpmul_arch}. It is composed of 4 stages:
\begin{enumerate}
	\item main function of this stage is \textbf{unpack}: separate sign,
		exponent and significand of each operand and reinstate the
		``hidden \texttt{1}'', after checking for special values and
		exceptions.
	\item this stage is composed of two blocks:
		\begin{itemize}
			\item \textbf{exponents adder} 
			\item \textbf{significands multiplier}: it is the most
				complex part of this stage. The product of the
				significands, each in the range $[1,2)$, will be
				in range $[1, 4)$. Thus, the result may have to
				be normalized by shifting it by one position to
				the right and incrementing the exponent.
		\end{itemize}
	\item the result is \textbf{normalized}. In addition, \textbf{rounding}
		is needed for converting intermediate computation results with
		additional digits, to lower precision formats.
	\item the rounding of previous stage may require another
		\textbf{normalizing shift}. Finally, \textbf{packing} the result
		involves combining the sign, exponent, and significand for the
		result and removing the ``hidden \texttt{1}''.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width=.25\textwidth]{FPMultiplier.png}
	\caption{Floating point multiplier architecture}
	\label{fig:fpmul_arch}
\end{figure}

In the following activities, a \textit{VHDL} implementation of such an architecture
downloaded from \textit{opencores.org} has been used.

\subsection{Simulation}
In order to verify the unit, a test-bench has been designed. It reads test patterns
from file, applied them to the multiplier and compares outputs with the expected
ones (which are also read from file). In case of mismatch the testbench reports
the error, thus the simulation can be performed without a GUI.

\bigskip
The test have been performed with two different units under test:
\begin{itemize}
	\item \underline{floating point multiplier}: latency of 4 clock cycles
	\item \underline{floating point multiplier with registers at the input}:
		latency of 5 clock cycles
\end{itemize}

In both cases no mismatch has been detected by the test bench.

\subsection{Synthesis}
During the synthesis process, \textit{Design Compiler} translates the HDL
description to components extracted from the generic technology library
(\textit{GTECH}) and \textit{DesignWare} library. The \textit{GTECH} library
consists of basic logic gates and flip-flops. The \textit{DesignWare} library
contains more complex blocks such as adders and comparators.
Both the \textit{GTECH} and \textit{DesignWare} libraries
are technology independent, that is, they are not mapped to a specific
technology library.

\bigskip
Since the significand multiplier in stage 2 is described behaviorally, it is
possible to specify the multiplier block from \textit{DesignWare} library before
compiling. In the following experiments, after finding the maximum frequency for
floating point multiplier, we forced the design compiler to use CSA multiplier
and PPARCH multiplier to compare the result of synthesis from maximum frequency
and area perspective.

The constraints for this synthesis are:
\lstset{style=mystyle}
\begin{lstlisting}[language=tcl, tabsize=4]{Synthesis Constraint}
### set constraints ###
create_clock -name "my_clock" -period 0.0 clk
set_dont_touch_network my_clock
set_clock_uncertainty 0.07 [get_clocks my_clock]
set_input_delay 0.5 -max -clock my_clock [remove_from_collection [all_inputs] clk]
set_output_delay 0.5 -max -clock my_clock [all_outputs]

set OLOAD [load_of NangateOpenCellLibrary/BUF_X4/A]
set_load $OLOAD [all_outputs]
\end{lstlisting}

\bigskip
The process for finding the maximum frequency is made up of two steps:
\begin{enumerate}
	\item constrain clock period to 0 and perform a synthesis.
		This forces the compiler to optimize performance as much as
		possible. The minumum achievable clock period is approximatedly
		equal to the slack obtained with this synthesis.
	\item constrain clock period to a value close to slack obtained in step 1
		and perform synthesis. Repeat this step adjusting the clock
		constraint until positive slack is achieved.
\end{enumerate}

Table~\ref{tab:syn_res} shows the result of the synthesis.
\textit{CSA} multiplier lead to significanlty worse results both in terms of
performance and area.
\textit{Synopsys choice} implementation, compared to \textit{PPARCH} is better
by small margin in terms of performance, while its area is slightly bigger.
By looking at the resource report of these two implementation it is obvious that
both use the PPARCH implementation, although the used module is different.
\vspace{0.5 cm}
\begin{table}[H]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{cccccc}
			\hline
			\rowcolor{gray!50}
			\textbf{Architecture} & \textbf{Clock Period} &
			\textbf{Maximum Frequency} & \textbf{Arrival time} &
			\textbf{Slack} & \textbf{Total cell Area} \\
			\hline
			Synopsys Choice	& 1.53 ns & 653 MHz & -1.42 ns & 0.00 MET & 4168.75 $\mu m^2$ \\
			\rowcolor{gray!25}
			CSA & 4.27 ns & 234 MHz & -4.16 ns & 0.00 MET & 4857.95 $\mu m^2$ \\
			PPARCH & 1.56 ns & 641 MHz & -1.45 ns & 0.00 MET & 4097.19 $\mu m^2$ \\
			\hline
		\end{tabular}
	}
	\caption{Synthesis summary}
	\label{tab:syn_res}
\end{table}

\section{Optimization}
The \textbf{throughput} of an arithmetic unit can be increased by:
\begin{itemize}
	\item \textbf{fine-grain pipelining}: add \underline{pipeline registers
		at the gate level}, within the unit itself
	\item \textbf{performance-oriented architectures}
\end{itemize}

Since the most critical part of the floating point multiplier is the
\textbf{significands multiplier}, both techniques can be applied to it:
the behavioral description of the multiplication have been replaced by a
fine-grain pipelined Modified Booth's Encoding multiplier.

\subsection{Fine-Grain Pipelining}
\subsubsection{Implementation}
\textit{Design Compiler} provides the \texttt{optimize\_registers} command, which
\textit{``Performs  retiming of sequential cells (edge-triggered registers or
level-sensitive latches) on a mapped gate-level netlist; determines the
placement of sequential cells in a design to achieve a target clock period)''}.

Thus a circuit can be automatically pipelined by adding some additional registers
to the design and then issuing the \texttt{optimize\_registers} command.

\bigskip
This approach have been applied to the significands multiplier, adding one register
to its output. In order to preserve synchronization between different signals,
for each output signal of stage 1 a further register is added. 
Figure~\ref{fig:fpmul_st2} shows the stage 2 of multiplier at RTL level, where
the blue registers are the ones added to implement the pipelining.

\begin{figure}[H]
	\centering
	\includegraphics[width=.8\textwidth]{stage2_pipelined.jpg}
	\caption{FP multiplier: pipelined stage 2}
	\label{fig:fpmul_st2}
\end{figure}

\subsubsection{Simulation}
Before synthesizing the circuit, the previous test bench architecture have been
run on the modified circuit, taking into account the additional cycle of latency.
Also in this case no mismatch between expected and generated output has been
detected.

\subsubsection{Synthesis}
Once again the usual steps to find the maximum frequency have been performend in
\textit{Design Compiler} enviroment. 
Additionally to the usual synthesis performed by the \texttt{compile} command,
synthesis with \texttt{compile\_ultra} command have also been performed, aiming
to achieve the best possible performance.

A summary of the achieved results is displayed in table~\ref{tab:pip_syn}.

\begin{table}[H]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{cccccc}
			\hline
			\rowcolor{gray!50}
			\textbf{Command} & \textbf{Clock Period} & \textbf{Maximum Frequency} &
			\textbf{Arrival time} & \textbf{Slack} & \textbf{Total cell Area} \\
			\hline
			\texttt{compile} & 0.78 ns & 1.28 GHz & -0.68 ns & 0.00 MET & 5152.41 $\mu m^2$ \\
			\rowcolor{gray!25}
			\texttt{compile\_ultra}	& 0.80 ns & 1.25 GHz &  -70 ns & 0.00 MET & 5038.33 $\mu m^2$  \\
			\hline
		\end{tabular}
	}
	\caption{Synthesis summary}
	\label{tab:pip_syn}
\end{table}

\subsection{Unsigned Full-Tree MBE Multiplier}
A \textbf{Full-Tree Multiplier} is a performance-oriented multiplier made up of
three main components:
\begin{enumerate}
	\item \underline{Operands generator:} generate all the partial products
	\item \underline{Carry-Save Adder Tree:} efficiently sum the partial
		products
	\item \underline{Final 2-operands Adder:} merge the sum and the carry
		vectors provided by the tree
\end{enumerate}

\bigskip
The configuration of the developed multiplier is shown in table~\ref{tab:mul_conf}.

\begin{table}[H]
	\centering
	\begin{tabular}{cc}
		\hline
		\rowcolor{gray!50}
		\textbf{Component} & \textbf{Implementation} \\
		\hline
		\textbf{Operands generator} & Radix-4 Modified Booth's Encoder \\
		\rowcolor{gray!25}
		\textbf{Carry-Save Adder Tree} & Dadda tree \\
		\textbf{Final 2-operands Adder} & Ripple Carry Adder \\
		\hline
	\end{tabular}
	\caption{Multiplier characteristics}
	\label{tab:mul_conf}
\end{table}

\subsubsection{Operands generator}
Given a multiplicand $A$ and a multiplier $X$, both $N_{bits}$ long, in a
\textbf{Radix-4 Modified Booth's Encoder} operand generator, for each 3 bits
of the $X$ (plus one trailing 0 and two leading 0s) there are:
\begin{itemize}
	\item \underline{Booth's Encoding Unit:}\\
		generate the selection signal for multiplexer and \texttt{s}
		signal, according to table~\ref{tab:booth_enc}
	\item \underline{Multiplexer:}\\
		select between $[0, A, -A, 2A, -2A]$
\end{itemize}

\begin{table}[H]
	\centering
	\begin{tabular}{ccc}
		\hline
		\rowcolor{gray!50}
		\textbf{Multiplier bits} & \textbf{Selection} & \textbf{\texttt{s}}\\
		\hline
		\texttt{000}	& 0	& 0 \\
		\rowcolor{gray!25}
		\texttt{001}	& $A$	& 0 \\
		\texttt{010}	& $A$	& 0 \\
		\rowcolor{gray!25}
		\texttt{011}	& $2A$	& 0 \\
		\texttt{100}	& $-2A$	& 1 \\
		\rowcolor{gray!25}
		\texttt{101}	& $-A$	& 1 \\
		\texttt{110}	& $-A$	& 1 \\
		\rowcolor{gray!25}
		\texttt{111}	& $0$	& 0 \\
		\hline
	\end{tabular}
	\caption{Booth's Encoding}
	\label{tab:booth_enc}
\end{table}

Therefore the operand generator generates $N_{bits} / 2 + 1$ partial products,
each $N_{bits} + 1$ bits long to fit the left shifts, except for the last one
(the 00 padding makes the left shift impossible). 

\bigskip
After the generation of the partial products, they are put in a structure
equivalent to figure~\ref{fig:booth_pp}. The \texttt{s} signals are inserted for
the correct generation of the 2s complement of multiplicand $A$, since the
multiplexers are fed with 1s complement of $A$ and $2A$, thus requiring a $+1$
to correctly form $-A$ and $-2A$.

\begin{figure}[H]
	\centering
	\includegraphics[width=.8\textwidth]{booth_operands.png}
	\caption{Booth's partial products}
	\label{fig:booth_pp}
\end{figure}

\paragraph{Implementation} \hspace{0pt} \\
A \textit{VHDL} implementation is located in \texttt{src/mbe/op\_gen/}.

\bigskip
It is configurable in the number of $A$ and $X$ bits.\\
The output structure is stored in a matrix $2 N_{bits} \times (N_{bits} / 2 + 1)$;
cells in which there is no dot are not driven.

\subsubsection{Carry-Save Adder Tree \& Final 2-operands Adder}
A \textbf{Dadda Tree} Carry-Save Adder follows an ``As Late As Possible'' resource
allocation approach: at every tree level $j$ only the strictly necessary half-adders
and full-adders are inserted in the tree in order to reach $d_{j - 1}$ rows of
dots, according to the formula:
\[
	\begin{cases}
		d_{j} := \left\lfloor \frac{3}{2} d_{j - 1} \right\rfloor \\
		d_1 := 2
	\end{cases}
\]

Figure~\ref{fig:dadda_tree} shows the Dadda tree structure when $N_{bits} = 8$.

\bigskip
Due to the irregularity of the last operand, the 2-operands final adder is a
modified Ripple Carry Adder embedded in the tree component, taking care of the
missing dot in second position.

\begin{figure}[H]
	\centering
	\includegraphics[width=.5\textwidth]{Dadda_tree_8x8.pdf}
	\caption{Dadda tree with $N_{bits} = 8$}
	\label{fig:dadda_tree}
\end{figure}

\paragraph{Implementation} \hspace{0pt} \\
A \textit{VHDL} implementation is located it \texttt{/src/mbe/tree}.

\bigskip
In order to keep the code clean the \texttt{compute\_tree} function have been
developed: it is in charge of computing a data structure containing the type and
the position of each component to be added to the tree, which is then used in
the tree entity to actually build the tree. This allowed to easily make the tree
configurabile in the number of $A$ and $X$ bits.

\paragraph{Simulation} \hspace{0pt} \\
The test bench for testing the multiplier has been designed in order to be
automatic and run without a GUI:
\begin{itemize}
	\item \underline{input generation:} pseudo-random inputs are generated
		by one LFSR for each input, initialized with different seeds
	\item \underline{output check:} output products are checked against
		products generated by means of \texttt{numeric\_std} operators:
		if mismatches are detected, error messages are generated
\end{itemize}

After the MBE multiplier have been verified, it has been embedded in the floating
point multiplier, replacing the default significands multiplier.
Also this unit have been then tested with the automated testbench.

\paragraph{Synthesis} \hspace{0pt} \\
Also in this synthesis step multiple compilations (both with \texttt{compile}
and \texttt{compile\_ultra} commands) have been issued in order to find maximum
clock frequency achievable by the circuit, until a non-negative slack is obtained.

Table~\ref{tab:mbe_syn} summarizes the synthesis results.

\begin{table}[H]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{cccc}
			\hline
			\rowcolor{gray!50}
			\textbf{Command} & \textbf{Clock Period} & 
			\textbf{Arrival time} & \textbf{Total cell Area} \\
			\hline
			\texttt{compile} & 0.94 ns & -0.83 ns &  6701.33 $\mu m^2$ \\
			\rowcolor{gray!25}
			\texttt{compile\_ultra}	& 0.87 ns & -0.77  ns & 6760.12 $\mu m^2$  \\
			\hline
		\end{tabular}
	}
	\caption{FP multiplier with MBE multiplier synthesis summary}
	\label{tab:mbe_syn}
\end{table}
\end{document}

