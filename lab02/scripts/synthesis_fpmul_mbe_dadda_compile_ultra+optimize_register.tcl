source .synopsys_dc.setup
set tcl_precision 5

set proj_root "/home/isa29/isa-labs/lab02"

set src_vhdl [list				\
	"/src/mbe/basic_blocks/full_adder.vhd"	\
	"/src/mbe/basic_blocks/half_adder.vhd"	\
	"/src/mbe/basic_blocks/mux5to1.vhd"	\
	"/src/mbe/utils/mbe_constants.vhd"	\
	"/src/mbe/utils/mbe_types.vhd"		\
	"/src/mbe/op_gen/mbe_encoder.vhd"	\
	"/src/mbe/op_gen/mbe_op_gen.vhd"		\
	"/src/mbe/tree/mbe_d_funcs.vhd"		\
	"/src/mbe/tree/mbe_functions.vhd"	\
	"/src/mbe/tree/mbe_dadda_tree.vhd"	\
	"/src/mbe/mbe_multiplier.vhd"		\
	"/src/fpmul/common/reg.vhd"\
	"/src/fpmul/common/fpnormalize_fpnormalize.vhd"\
	"/src/fpmul/common/fpround_fpround.vhd"\
	"/src/fpmul/common/packfp_packfp.vhd"\
	"/src/fpmul/common/unpackfp_unpackfp.vhd"\
	"/src/fpmul/fpmul_stage4_struct.vhd"\
	"/src/fpmul/fpmul_stage3_struct.vhd"\
	"/src/fpmul/fpmul_stage2_struct_dadda.vhd"\
	"/src/fpmul/fpmul_stage1_struct.vhd"\
	"/src/fpmul/fpmul_pipeline_reg_dadda.vhd"\
	
]

set top_entity "FPmul_dadda"
set archictecture "pipeline"
set out_dir "reports"
set library "work"
set syn_dir "/syn/04-fpmul_MBE_dadda/compile_optimize_registers"
set clk_period 0.00

for {set i 0} {$i < 1} {incr i} {

if {[file exist $library] ==0} {
     exec mkdir $library
	 }
if {[file exist $proj_root$syn_dir/$out_dir] ==1} {
     exec rm -r $proj_root$syn_dir/$out_dir
	 }


### setup synthesis ###

foreach vhdl_file $src_vhdl {
	analyze -library WORK -format vhdl  $proj_root$vhdl_file
}

### Elaborate ####

elaborate $top_entity -architecture $archictecture -library $library
echo "------------------------End of Elaborating--------------------- "
#unique the instances of the same refrence during synthesis allow to apply different constraint to each instance
#uniquify

##checks to make sure all parts in the current design are available in designe analyzer ## memory
link


set max_delay $clk_period
set in_delay 0.5
set out_delay 0.5
set out_load [load_of NangateOpenCellLibrary/BUF_X4/A]
set clk_uncertainty 0.07
echo "------------------------SET Constraints--------------------- "
#### set constraints ####
create_clock -name "my_clock" -period $clk_period clk
set_dont_touch_network my_clock
set_clock_uncertainty $clk_uncertainty [get_clocks my_clock]
set_input_delay $in_delay -max -clock my_clock [remove_from_collection [all_inputs] clk]
set_output_delay $out_delay -max -clock my_clock [all_outputs]
set_load $out_load [all_outputs]

ungroup -all -flatten

#### compile synthesis ###
echo "------------------------Compile synthesis--------------------- "
compile_ultra
optimize_register

### Report generation ###

echo "------------------------Report Reneration----------------------"
if {[file exist $proj_root$syn_dir/$out_dir] ==0} {
     exec mkdir $proj_root$syn_dir/$out_dir
         }

report_clock > $proj_root$syn_dir/$out_dir/report_clock.rpt
report_timing > $proj_root$syn_dir/$out_dir/report_timing.rpt
report_area > $proj_root$syn_dir/$out_dir/report_area.rpt
report_resources > $proj_root$syn_dir/$out_dir/report_resources.rpt
check_design > $proj_root$syn_dir/$out_dir/check_design.rpt

exec grep -w "data arrival time"  $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "clock my_clock"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "data required time"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "slack"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt 

if {[file exist $library] ==1} {
     exec rm -r $library
	 }
if {[file exist alib-52] ==1} {
     exec rm -r alib-52
         }
if {[file exist command.log] ==1} {
     exec rm -r command.log
	 }
if {[file exist default.svf] ==1} {
     exec rm -r default.svf
	 }

set new_clk [expr $clk_period + 0.01]
set clk_period $new_clk
puts $clk_period
}
exit
