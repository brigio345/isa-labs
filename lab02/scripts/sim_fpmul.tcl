set proj_root ".." 

# Uncomment following lines for simulating FPmul entity
#set stage2_file "/src/fpmul/fpmul_stage2_struct.vhd"
#set top_file "/src/fpmul/fpmul_pipeline.vhd"
#set tb_file "/tb/tb_fpmul_pipeline.vhd"
#set tb_entity "TB_FPmul"

# Uncomment following lines for simulating FPmul_reg entity
#set stage2_file "/src/fpmul/fpmul_stage2_struct.vhd"
#set top_file "/src/fpmul/fpmul_pipeline_reg.vhd"
#set tb_file "/tb/tb_fpmul_pipeline_reg.vhd"
#set tb_entity "TB_FPmul_reg"

# Uncomment following lines for simulating FPmul_reg_fine entity
#set stage2_file "/src/fpmul/fpmul_stage2_struct_reg.vhd"
#set top_file "/src/fpmul/fpmul_pipeline_reg_fine.vhd"
#set tb_file "/tb/tb_fpmul_pipeline_reg_fine.vhd"
#set tb_entity "TB_FPmul_reg_fine"

# Uncomment following lines for simulating FPmul_dadda entity
#set stage2_file "/src/fpmul/fpmul_stage2_struct_dadda.vhd"
#set top_file "/src/fpmul/fpmul_pipeline_reg_dadda.vhd"
#set tb_file "/tb/tb_fpmul_pipeline_dadda.vhd"
#set tb_entity "TB_FPmul_dadda"

# Uncomment following lines for simulating FPmul_dadda_fine entity
set stage2_file "/src/fpmul/fpmul_stage2_struct_dadda_reg.vhd"
set top_file "/src/fpmul/fpmul_pipeline_reg_dadda_fine.vhd"
set tb_file "/tb/tb_fpmul_pipeline_dadda_fine.vhd"
set tb_entity "TB_FPmul_dadda_fine"

set src_vhdl [list                     		\
	"/src/mbe/basic_blocks/full_adder.vhd"	\
	"/src/mbe/basic_blocks/half_adder.vhd"	\
	"/src/mbe/basic_blocks/mux5to1.vhd"	\
	"/src/mbe/utils/mbe_constants.vhd"	\
	"/src/mbe/utils/mbe_types.vhd"		\
	"/src/mbe/op_gen/mbe_encoder.vhd"	\
	"/src/mbe/op_gen/mbe_op_gen.vhd"	\
	"/src/mbe/tree/mbe_d_funcs.vhd"		\
	"/src/mbe/tree/mbe_functions.vhd"	\
	"/src/mbe/tree/mbe_dadda_tree.vhd"	\
	"/src/mbe/mbe_multiplier.vhd"		\
	"/src/fpmul/common/register.vhd"\
	"/src/fpmul/common/register_single_bit.vhd"\
	"/src/fpmul/common/fpnormalize_fpnormalize.vhd"\
	"/src/fpmul/common/fpround_fpround.vhd"\
	"/src/fpmul/common/packfp_packfp.vhd"\
	"/src/fpmul/common/unpackfp_unpackfp.vhd"\
	"/src/fpmul/fpmul_stage4_struct.vhd"\
	"/src/fpmul/fpmul_stage3_struct.vhd"\
	$stage2_file\
	"/src/fpmul/fpmul_stage1_struct.vhd"\
	$top_file\
	$tb_file
]

foreach vhdl_file $src_vhdl {
	vcom $proj_root$vhdl_file
}

vsim $tb_entity
add wave *
run -all

