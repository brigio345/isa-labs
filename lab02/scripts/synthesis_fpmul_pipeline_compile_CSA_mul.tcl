source .synopsys_dc.setup
set tcl_precision 5


set proj_root "/home/isa29/isa-labs/lab02" 
set src_vhdl [list                     \
	"/src/fpmul/common/register.vhd"\
	"/src/fpmul/common/fpnormalize_fpnormalize.vhd"\
	"/src/fpmul/common/fpround_fpround.vhd"\
	"/src/fpmul/common/packfp_packfp.vhd"\
	"/src/fpmul/common/unpackfp_unpackfp.vhd"\
	"/src/fpmul/fpmul_stage4_struct.vhd"\
	"/src/fpmul/fpmul_stage3_struct.vhd"\
	"/src/fpmul/fpmul_stage2_struct.vhd"\
	"/src/fpmul/fpmul_stage1_struct.vhd"\
	"/src/fpmul/fpmul_pipeline_reg.vhd"\
	]

set top_entity "FPMUL"
set archictecture "PIPELINE"
set out_dir "reports"
set library "work"
set syn_dir "/syn/01-csa_mul"
set clk_period 4.2725

for {set i 0} {$i < 1} {incr i} {
if {[file exist ./$library] ==0} {
     exec mkdir ./$library
	 }
if {[file exist $proj_root$syn_dir/$out_dir] ==1} {
     exec rm -r $proj_root$syn_dir/$out_dir
	 }


### synthesis ###

foreach vhdl_file $src_vhdl {
analyze -library WORK -format vhdl $proj_root$vhdl_file
}

### Elaborate ####

elaborate $top_entity -architecture $archictecture -library $library

#uniquify
link

set max_delay $clk_period
set in_delay 0.5
set out_delay 0.5
set OLOAD [load_of NangateOpenCellLibrary/BUF_X4/A]
set clk_uncertainty 0.07

#### set constraints ####
create_clock -name "my_clock" -period $clk_period clk
set_dont_touch_network my_clock
set_clock_uncertainty $clk_uncertainty [get_clocks my_clock]
set_input_delay $in_delay -max -clock my_clock [remove_from_collection [all_inputs] clk]
set_output_delay $out_delay -max -clock my_clock [all_outputs]
set_load $OLOAD [all_outputs]

ungroup -all -flatten

echo "------------ Carry Save Adder Multiplier------------------"
set_implementation DW02_mult/csa [find cell *mult*]

#### compile synthesis ###
echo "------------------compile synthesis-----------------------"
compile 

### Report generation ###

if {[file exist $proj_root$syn_dir/$out_dir] ==0} {
     exec mkdir $proj_root$syn_dir/$out_dir
	 }

report_clock > $proj_root$syn_dir/$out_dir/report_clock.rpt
report_timing > $proj_root$syn_dir/$out_dir/report_timing.rpt
report_area > $proj_root$syn_dir/$out_dir/report_area.rpt
report_resources > $proj_root$syn_dir/$out_dir/report_resources.rpt
check_design > $proj_root$syn_dir/$out_dir/check_design.rpt

exec grep -w "data arrival time"  $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "clock my_clock"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "data required time"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt
exec grep -w "slack"   $proj_root$syn_dir/$out_dir/report_timing.rpt >> $proj_root$syn_dir/slack_report.txt 

if {[file exist $library] ==1} {
     exec rm -r $library
	 }
if {[file exist alib-52] ==1} {
     exec rm -r alib-52
	 }
if {[file exist command.log] ==1} {
     exec rm -r command.log
	 }
if {[file exist default.svf] ==1} {
     exec rm -r default.svf
	 }

set new_clk [expr $clk_period + 0.01]
set clk_period $new_clk
puts $clk_period
}
exit
