library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg is
  generic(NBIT: integer := 8);
  Port (EN:	In	std_logic;
   	D:	In	std_logic_vector(NBIT-1 downto 0);
	CK:	In	std_logic;
	RESET:	In	std_logic;
	Q:	Out	std_logic_vector(NBIT-1 downto 0));
end reg;

architecture beh of reg is
	signal data: std_logic_vector(NBIT-1 downto 0);
begin
	process(ck, RESET)
	begin
		if (RESET = '1') then
			data <= (others => '0');
		elsif (ck'event and ck = '1') then 
			if ( EN = '1' ) then
				data <= D;
			end if;
		end if;
	end process;

	Q <= data;
end beh;

