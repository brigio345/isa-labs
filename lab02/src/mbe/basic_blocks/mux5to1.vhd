library ieee;
use ieee.std_logic_1164.all;

-- mux5to1: 5-inputs multiplexer
entity mux5to1 is
	generic (
		N_BIT:	natural := 8
	);
	port (
		i_d0:	in std_logic_vector((N_BIT - 1) downto 0);	
		i_d1:	in std_logic_vector((N_BIT - 1) downto 0);	
		i_d2:	in std_logic_vector((N_BIT - 1) downto 0);	
		i_d3:	in std_logic_vector((N_BIT - 1) downto 0);	
		i_d4:	in std_logic_vector((N_BIT - 1) downto 0);	
		i_s:	in std_logic_vector(2 downto 0);

		o_d:	out std_logic_vector((N_BIT - 1) downto 0)
	);
end mux5to1;

architecture rtl of mux5to1 is
begin
	with i_s select o_d <=
		i_d0		when "000",
		i_d1		when "001",
		i_d2		when "010",
		i_d3		when "011",
		i_d4		when "100",
		(others => '0')	when others;
end architecture rtl;

