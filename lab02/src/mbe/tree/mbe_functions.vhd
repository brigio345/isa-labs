library ieee;
use ieee.std_logic_1164.all;
use work.mbe_types.all;
use work.mbe_constants.all;
use work.mbe_d_funcs.all;

package mbe_functions is
	constant N_LAYERS:	natural := j_of_d(N_ROWS);

	type cell_type_t is (
		void,
		fa,
		ha,
		wire
	);

	type cell_t is record
		c_type:	cell_type_t;	-- type of the cell
		i_0:	integer;	-- row of first input
		i_1:	integer;	-- row of second input
		i_2:	integer;	-- row of third input
		o_0:	integer;	-- row of first output
		o_1:	integer;	-- row of second output
	end record cell_t;
		
	type layer_t is array (0 to (N_ROWS - 1), (N_COLS - 1) downto 0) of cell_t;
	type tree_t is array ((N_LAYERS - 2) downto 0) of layer_t;

	function get_init_valid_matrix return booth_mat_t;
	function compute_tree return tree_t;
end package mbe_functions;

package body mbe_functions is
	-- return a matrix which is set to '1' if a dot is present, '0' otherwise
	-- (according to figure A.5 of paper sign_extension_booth_multiplier_Stanford.pdf)
	function get_init_valid_matrix return booth_mat_t is
		variable ivm:	booth_mat_t;
	begin
		ivm := (others => (others => '0'));

		for i in 0 to (N_ROWS - 1) loop
			if (i < (N_ROWS - 1)) then
				-- all but last rows are N_BITS + 1 bits long
				ivm(i)(N_BITS + 2 * i downto 2 * i) :=
					(N_BITS + 2 * i downto 2 * i => '1');

				if (i = 0) then
					ivm(i)(N_BITS + 3 downto N_BITS + 1) := "111";
				else
					ivm(i)(2 * (i - 1)) := '1';
					ivm(i)(N_BITS + 1 + 2 * i) := '1';
					if (i < (N_ROWS - 2)) then
						ivm(i)(N_BITS + 2 + 2 * i) := '1';
					end if;
				end if;
			else
				-- last row is N_BITS bits long
				ivm(i)(N_BITS + 2 * i - 1 downto N_BITS) :=
					(N_BITS + 2 * i - 1 downto N_BITS => '1');

				ivm(i)(2 * (i - 1)) := '1';
			end if;
		end loop;

		return ivm;
	end function get_init_valid_matrix;

	-- compute a 3D array containing data useful to build a Dadda tree CSA
	function compute_tree return tree_t is
		variable tree:	tree_t := (others => (others => (others => (
					c_type	=> void,
					i_0	=> -1,
					i_1	=> -1,
					i_2	=> -1,
					o_0	=> -1,
					o_1	=> -1
				))));
		variable valid:		booth_mat_t := (others => (others => '0'));
		variable valid_next:	booth_mat_t := get_init_valid_matrix;
		variable d:		natural := N_ROWS;
		variable dot_cnt_curr:	integer := 0;
		variable dot_cnt_next:	integer := 0;
		variable n_fa:		integer := 0;
		variable n_ha:		integer := 0;
		variable dot_curr:	integer := 0;
		variable dot_next:	integer := 0;
		variable dot_next_next:	integer := 0;
	begin
		for layer in tree'range loop
			-- d(layer + 1) = d(j - 1)
			d := d_of_j(layer + 1);
			
			valid := valid_next;
			valid_next := (others => (others => '0'));
			dot_cnt_next := 0;
			dot_next_next := 0;

			for col in 0 to (N_COLS - 1) loop
				-- count dots of column col
				-- take into account carries of previous column
				dot_cnt_curr := dot_cnt_next;
				for row in 0 to (N_ROWS - 1) loop
					if (valid(row)(col) = '1') then
						dot_cnt_curr := dot_cnt_curr + 1;
					end if;
				end loop;

				-- compute FAs and HAs to be added to column col
				if (dot_cnt_curr - d > 0) then
					n_fa := (dot_cnt_curr - d) / 2;
					n_ha := (dot_cnt_curr - d) mod 2;
					dot_cnt_next := n_fa + n_ha;
				else
					n_fa := 0;
					n_ha := 0;
					dot_cnt_next := 0;
				end if;

				dot_curr := 0;
				-- in first layer there are some holes at the top:
				-- increment dot_curr until valid dots are found
				while (valid(dot_curr)(col) = '0') loop
					dot_curr := dot_curr + 1;
				end loop;

				dot_next := dot_next_next;
				dot_next_next := 0;
				-- insert all needed FAs
				for k in 0 to (n_fa - 1) loop
					tree(layer)(dot_next, col) := (
						c_type	=> fa,
						i_0	=> dot_curr,
						i_1	=> dot_curr + 1,
						i_2	=> dot_curr + 2,
						o_0	=> dot_next,
						o_1	=> dot_next_next
					);

					valid_next(dot_next)(col) := '1';
					if (layer > 0) then
						valid_next(dot_next_next)(col + 1) := '1';
					end if;

					dot_curr := dot_curr + 3;
					dot_next := dot_next + 1;
					dot_next_next := dot_next_next + 1;
				end loop;

				-- insert HA if needed
				if (n_ha > 0) then
					tree(layer)(dot_next, col) := (
						c_type	=> ha,
						i_0	=> dot_curr,
						i_1	=> dot_curr + 1,
						i_2	=> -1,
						o_0	=> dot_next,
						o_1	=> dot_next_next
					);

					valid_next(dot_next)(col) := '1';
					valid_next(dot_next_next)(col + 1) := '1';

					dot_curr := dot_curr + 2;
					dot_next := dot_next + 1;
					dot_next_next := dot_next_next + 1;
				end if;

				-- map all remaining dots to short circuits
				while ((dot_curr < N_ROWS) and (valid(dot_curr)(col) = '1')) loop
					tree(layer)(dot_next, col) := (
						c_type	=> wire,
						i_0	=> dot_curr,
						i_1	=> -1,
						i_2	=> -1,
						o_0	=> dot_next,
						o_1	=> -1
					);

					valid_next(dot_next)(col) := '1';

					dot_curr := dot_curr + 1;
					dot_next := dot_next + 1;
				end loop;
			end loop;
		end loop;

		return tree;
	end function compute_tree;
end package body mbe_functions;

