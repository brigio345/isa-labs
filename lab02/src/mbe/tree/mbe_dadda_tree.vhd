library ieee;
use ieee.std_logic_1164.all;
use work.mbe_constants.all;
use work.mbe_types.all;
use work.mbe_functions.all;

-- mbe_dadda_tree: radix-4 modified Booth's encoding Dadda tree CSA
entity mbe_dadda_tree is
	port (
		i_op:	in booth_mat_t;

		o_s:	out std_logic_vector(2 * N_BITS - 1 downto 0)
	);
end mbe_dadda_tree;

architecture structural of mbe_dadda_tree is
	component full_adder is
		port (
			i_a:	in std_logic;
			i_b:	in std_logic;
			i_c:	in std_logic;

			o_s:	out std_logic;
			o_c:	out std_logic
		);
	end component full_adder;

	component half_adder is
		port (
			i_a:	in std_logic;
			i_b:	in std_logic;

			o_s:	out std_logic;
			o_c:	out std_logic
		);
	end component half_adder;

	constant tree:	tree_t := compute_tree;

	type dots_t is array (N_LAYERS downto 0) of booth_mat_t;

	signal dots:	dots_t;
begin
	dots(N_LAYERS) <= i_op;

	-- build the Dadda tree
	layer_loop: for i in tree'range generate
		row_loop: for j in 0 to (N_ROWS - 1) generate
			column_loop: for k in 0 to (N_COLS - 1) generate
				fa_check: if (tree(i)(j, k).c_type = fa) generate
					fa_full: if (k + 1 < N_COLS) generate
						fa_f: full_adder
							port map (
								i_a	=> dots(i + 2)(tree(i)(j, k).i_0)(k),
								i_b	=> dots(i + 2)(tree(i)(j, k).i_1)(k),
								i_c	=> dots(i + 2)(tree(i)(j, k).i_2)(k),
								o_s	=> dots(i + 1)(tree(i)(j, k).o_0)(k),
								o_c	=> dots(i + 1)(tree(i)(j, k).o_1)(k + 1)
							);
					end generate;

					-- discard last carry out
					fa_half: if (k + 1 >= N_COLS) generate
						fa_h: full_adder
							port map (
								i_a	=> dots(i + 2)(tree(i)(j, k).i_0)(k),
								i_b	=> dots(i + 2)(tree(i)(j, k).i_1)(k),
								i_c	=> dots(i + 2)(tree(i)(j, k).i_2)(k),
								o_s	=> dots(i + 1)(tree(i)(j, k).o_0)(k),
								o_c	=> open
							);
					end generate;
				end generate fa_check;

				ha_check: if (tree(i)(j, k).c_type = ha) generate
					ha: half_adder
						port map (
							i_a	=> dots(i + 2)(tree(i)(j, k).i_0)(k),
					       		i_b	=> dots(i + 2)(tree(i)(j, k).i_1)(k),
							o_s	=> dots(i + 1)(tree(i)(j, k).o_0)(k),
					       		o_c	=> dots(i + 1)(tree(i)(j, k).o_1)(k + 1)
						);
				end generate ha_check;

				wire_check: if (tree(i)(j, k).c_type = wire) generate
					dots(i + 1)(tree(i)(j, k).o_0)(k) <=
						dots(i + 2)(tree(i)(j, k).i_0)(k);
				end generate wire_check;
			end generate column_loop;
		end generate row_loop;
	end generate layer_loop;

	-- build the final 2 operands adder
	-- sum first 2 bits, without a carry-in
	f_ha_0: half_adder
		port map (
			i_a	=> dots(1)(0)(0),
			i_b	=> dots(1)(1)(0),
			o_s	=> dots(0)(0)(0),
			o_c	=> dots(0)(1)(1)
		);

	-- sum second bit of first operand and first carry
	-- (there is no bit coming from second operand since there is no dot
	-- in this position)	
	f_ha_1: half_adder
		port map (
			i_a	=> dots(1)(0)(1),
			i_b	=> dots(0)(1)(1),
			o_s	=> dots(0)(0)(1),
			o_c	=> dots(0)(1)(2)
		);

	-- central part is a normal RCA
	final_adder_loop: for i in 2 to (N_COLS - 2) generate
		f_fa: full_adder
			port map (
				i_a	=> dots(1)(0)(i),
				i_b	=> dots(1)(1)(i),
				i_c	=> dots(0)(1)(i),
				o_s	=> dots(0)(0)(i),
				o_c	=> dots(0)(1)(i + 1)
			);
	end generate final_adder_loop;

	-- discard last carry out
	f_fa_last: full_adder
		port map (
			i_a	=> dots(1)(0)(N_COLS - 1),
			i_b	=> dots(1)(1)(N_COLS - 1),
			i_c	=> dots(0)(1)(N_COLS - 1),
			o_s	=> dots(0)(0)(N_COLS - 1),
			o_c	=> open
		);

	o_s	<= dots(0)(0)(o_s'range);
end architecture structural;

