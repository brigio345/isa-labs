package mbe_d_funcs is
	function d_of_j (
		j:	natural
	) return natural;

	function j_of_d (
		d:	natural
	) return natural;
end package mbe_d_funcs;

package body mbe_d_funcs is
	-- compute number of maximum tree depth at level j
	function d_of_j (
		j:	natural
	) return natural is
	begin
		if (j <= 1) then
			return 2;
		end if;

		return (3 * d_of_j(j - 1) / 2);
	end function d_of_j;

	-- compute tree level corresponding to tree depth d
	function j_of_d (
		d:	natural
	) return natural is
	begin
		if (d <= 2) then
			return 1;
		end if;

		return (j_of_d((2 * d + 2) / 3) + 1);
	end function j_of_d;
end package body mbe_d_funcs;

