library ieee;
use ieee.std_logic_1164.all;
use work.mbe_constants.all;
use work.mbe_types.all;

-- mbe_multiplier: high performance 2 operands multiplier with:
--	* partial products generation: radix-4 modified Booth's encoding algorithm
--	* partial products sum: Dadda tree CSA
entity mbe_multiplier is
	port (
		i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_x:	in std_logic_vector((N_BITS - 1) downto 0);	

		o_p:	out std_logic_vector(((2 * N_BITS) - 1) downto 0)
	);
end mbe_multiplier;

architecture structural of mbe_multiplier is
	component mbe_op_gen is
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_x:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_op:	out booth_mat_t
		);
	end component mbe_op_gen;

	component mbe_dadda_tree is
		port (
			i_op:	in booth_mat_t;

			o_s:	out std_logic_vector(2 * N_BITS - 1 downto 0)
		);
	end component mbe_dadda_tree;

	signal op:	booth_mat_t;
begin
	op_gen: mbe_op_gen
		port map (
			i_a	=> i_a,
			i_x	=> i_x,
			o_op	=> op
		);

	tree: mbe_dadda_tree
		port map (
			i_op	=> op,
	       		o_s	=> o_p		
		);
end architecture structural;

