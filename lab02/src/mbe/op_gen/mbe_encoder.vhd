library ieee;
use ieee.std_logic_1164.all;

-- mbe_encoder: radix-4 Booth's encoding unit:
--	convert 3 bits of multiplier to 3 selection bits
entity mbe_encoder is
	port (
		i_x:	in std_logic_vector(2 downto 0);	

		o_s:	out std_logic_vector(2 downto 0)
	);
end mbe_encoder;

architecture rtl of mbe_encoder is
begin
	with i_x select o_s <=
		"001" when "001" | "010",	--   A
		"010" when "011",		--  2A
		"011" when "101" | "110",	--  -A
		"100" when "100",		-- -2A
		"000" when others;		--   0
end architecture rtl;

