library ieee;
use ieee.std_logic_1164.all;
use work.mbe_constants.all;
use work.mbe_types.all;

-- mbe_op_gen: radix-4 modified Booth's encoding operands generator:
--	generate a matrix equivalent to figure A.5 of paper
--	sign_extension_booth_multiplier_Stanford.pdf
--	N.B. cells corresponding to not present dots are not driven
entity mbe_op_gen is
	port (
		i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_x:	in std_logic_vector((N_BITS - 1) downto 0);	

		o_op:	out booth_mat_t
	);
end mbe_op_gen;

architecture mixed of mbe_op_gen is
	component mbe_encoder is
		port (
			i_x:	in std_logic_vector(2 downto 0);	

			o_s:	out std_logic_vector(2 downto 0)
		);
	end component mbe_encoder;

	component mux5to1 is
		generic (
			N_BIT:	natural := 8
		);
		port (
			i_d0:	in std_logic_vector((N_BIT - 1) downto 0);	
			i_d1:	in std_logic_vector((N_BIT - 1) downto 0);	
			i_d2:	in std_logic_vector((N_BIT - 1) downto 0);	
			i_d3:	in std_logic_vector((N_BIT - 1) downto 0);	
			i_d4:	in std_logic_vector((N_BIT - 1) downto 0);	
			i_s:	in std_logic_vector(2 downto 0);

			o_d:	out std_logic_vector((N_BIT - 1) downto 0)
		);
	end component mux5to1;

	type enc_t is array (0 to (N_ROWS - 1)) of std_logic_vector(2 downto 0);
	type op_t is array (0 to (N_ROWS - 1)) of std_logic_vector(N_BITS downto 0);

	signal sel:	enc_t;
	signal x:	enc_t;
	signal s:	std_logic_vector(0 to (N_ROWS - 2));
	signal s_neg:	std_logic_vector(0 to (N_ROWS - 2));
	signal a:	std_logic_vector(N_BITS downto 0);
	signal a2:	std_logic_vector(N_BITS downto 0);
	signal a_neg:	std_logic_vector(N_BITS downto 0);
	signal a2_neg:	std_logic_vector(N_BITS downto 0);
	signal op:	op_t;
begin
	-- generate encoders input signals
	x(0) <= i_x(1 downto 0) & '0';
	x_gen: for i in 1 to (N_ROWS - 2) generate
		x(i) <= i_x((2 * i + 1) downto (2 * i - 1));
	end generate x_gen;
	x(N_ROWS - 1) <= "00" & i_x(N_BITS - 1);

	s_gen: process(x)
	begin
		for i in s'range loop
			if (x(i) = "111") then
				s(i) <= '0';
			else
				s(i) <= x(i)(2);
			end if;
		end loop;
	end process s_gen;
	s_neg <= not s;

	enc_gen: for i in x'range generate
		enc: mbe_encoder
			port map (
				i_x	=> x(i),
				o_s	=> sel(i)
			);
	end generate enc_gen;

	-- generate muxes input signals
	a	<= '0' & i_a;
	a2	<= i_a & '0';
	a_neg	<= not a;
	a2_neg	<= not a2;

	mux_gen: for i in o_op'range generate
		mux: mux5to1
			generic map (
				N_BIT => a'length
			)
			port map (
				i_d0	=> (others => '0'),
				i_d1	=> a,
				i_d2	=> a2,
				i_d3	=> a_neg,
				i_d4	=> a2_neg,
				i_s	=> sel(i),
				o_d	=> op(i)
			);
	end generate mux_gen;

	build_op: process(op, s, s_neg)
	begin
		-- build first row
		o_op(0)(N_BITS downto 0) <= op(0);
		-- insert leading s_neg, s, s
		o_op(0)(N_BITS + 3 downto N_BITS + 1) <= s_neg(0) & s(0) & s(0);

		-- build central rows
		for i in 1 to (N_ROWS - 2) loop
			-- all but last rows are N_BITS + 1 bits long
			o_op(i)((N_BITS + 2 * i) downto (2 * i)) <= op(i);
			-- insert trailing s
			o_op(i)(2 * (i - 1)) <= s(i - 1);
			-- insert leading s_neg
			o_op(i)(N_BITS + 1 + 2 * i) <= s_neg(i);
			-- last but one row do not have a leading 1
			if (i < (N_ROWS - 2)) then
				-- insert leading 1
				o_op(i)(N_BITS + 2 + 2 * i) <= '1';
			end if;
		end loop;

		-- build last row
		-- last row is N_BITS bits long
		o_op(N_ROWS - 1)(2 * N_BITS - 1 downto N_BITS) <=
			op(N_ROWS - 1)(N_BITS - 1 downto 0);
		-- insert trailing s
		o_op(N_ROWS - 1)(N_BITS - 2) <= s(N_ROWS - 2);
	end process build_op;
end architecture mixed;

