package mbe_constants is
	constant N_BITS:	natural := 32;
	constant N_ROWS:	natural := (N_BITS / 2 + 1);
	constant N_COLS:	natural := (2 * N_BITS);
end package mbe_constants;

