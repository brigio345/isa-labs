module tb_fir8_unfold3;
	localparam N_BITS = 11;
	localparam N_ORDER = 8;
	localparam SAMPLES_FILE_NAME = "./samples.txt";
	localparam OUTPUTS_FILE_NAME = "./outputs-fir8_unfold3.txt";
	localparam CLOCK_PERIOD = 20;

	reg [(N_BITS - 1) : 0] d0_in;
	reg [(N_BITS - 1) : 0] d1_in;
	reg [(N_BITS - 1) : 0] d2_in;
	reg v_in;
	reg [(N_BITS - 1) : 0] b0;
	reg [(N_BITS - 1) : 0] b1;
	reg [(N_BITS - 1) : 0] b2;
	reg [(N_BITS - 1) : 0] b3;
	reg [(N_BITS - 1) : 0] b4;
	reg [(N_BITS - 1) : 0] b5;
	reg [(N_BITS - 1) : 0] b6;
	reg [(N_BITS - 1) : 0] b7;
	reg [(N_BITS - 1) : 0] b8;
	reg rst_n;
	reg clk;
	wire signed [(N_BITS - 1) : 0] d0_out;
	wire signed [(N_BITS - 1) : 0] d1_out;
	wire signed [(N_BITS - 1) : 0] d2_out;
	wire v_out;

	fir8_unfold3 DUT (
		.i_d0(d0_in),
		.i_d1(d1_in),
		.i_d2(d2_in),
		.i_v(v_in),
		.i_b0(b0),
		.i_b1(b1),
		.i_b2(b2),
		.i_b3(b3),
		.i_b4(b4),
		.i_b5(b5),
		.i_b6(b6),
		.i_b7(b7),
		.i_b8(b8),
		.i_rst_n(rst_n),
		.i_clk(clk),
		.o_d0(d0_out),
		.o_d1(d1_out),
		.o_d2(d2_out),
		.o_v(v_out)
	);

	always #(CLOCK_PERIOD / 2) clk = ~clk;	// generate clock

	integer samples_file;
	integer outputs_file;
	integer sample0;
	integer sample1;
	integer sample2;

	initial
	begin
		samples_file = $fopen(SAMPLES_FILE_NAME, "r");
		outputs_file = $fopen(OUTPUTS_FILE_NAME, "w");

		// initialize FIR
		clk 	= 0;
		rst_n	= 0;
		v_in	= 1;
		b0	= -7;
		b1	= -14;
		b2	= 52;
		b3	= 272;
		b4	= 415;
		b5	= 272;
		b6	= 52;
		b7	= -14;
		b8	= -7;

		#CLOCK_PERIOD rst_n	= 1;
	end

	always @(posedge clk)
	begin
		// read new samples from file and apply them
		if (($fscanf(samples_file, "%d", sample0) == 1) &&
				($fscanf(samples_file, "%d", sample1) == 1) &&
				($fscanf(samples_file, "%d", sample2) == 1))
		begin
			d0_in = sample0;
			d1_in = sample1;
			d2_in = sample2;
		end
		else
		begin
			$stop;
		end

		if (v_out == 1)
			$fdisplay(outputs_file, "%0d\n%0d\n%0d",
				d0_out, d1_out, d2_out);
	end
endmodule // tb_fir8_unfold3

