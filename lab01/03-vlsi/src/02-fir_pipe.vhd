library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.port_types.all;

-- fir: finite impulse response filter which loads new values when i_v = '1';
--	o_v = '1' when o_d contains valid data.
-- The architecture is fully pipelined.
entity fir_pipe is
	port (
		i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_v:	in std_logic;
		i_b:	in b_vector;
		i_rst_n:in std_logic;
		i_clk:	in std_logic;

		o_d:	out std_logic_vector((N_BITS - 1) downto 0);
		o_v:	out std_logic
	);
end fir_pipe;

architecture structural of fir_pipe is
	component reg is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_clk:	in std_logic;
			i_ld:	in std_logic;
			i_rst:	in std_logic;	

			o_d:	out std_logic_vector((N_BITS - 1) downto 0)
		);
	end component reg;

	component shift_register is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_d:	in std_logic;
			i_clk:	in std_logic;
			i_rst:	in std_logic;

			o_d:	out std_logic
		);
	end component shift_register;

	component multiplier is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_p:	out std_logic_vector(((2 * N_BITS) - 1) downto 0)
		);
	end component multiplier;

	component adder is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_s:	out std_logic_vector((N_BITS - 1) downto 0)
		);
	end component adder;

	type n_bits_vector is array (0 to N_ORDER) of
		std_logic_vector((N_BITS - 1) downto 0);
	type n2_bits_vector is array (0 to N_ORDER) of
		std_logic_vector(((2 * N_BITS) - 1) downto 0);
	type pipe_vector is array (0 to (N_ORDER - 1)) of n2_bits_vector;

	signal x:	n_bits_vector;
	signal b:	n_bits_vector;
	signal p:	n2_bits_vector;
	signal p_norm:	n2_bits_vector;
	signal pipe: 	pipe_vector;
	signal s:	n2_bits_vector;
	signal rst:	std_logic;
begin
	-- convert active-low reset to active-high reset (used by sub-blocks)
	rst <= (not i_rst_n);

	-- registers storing b coefficients
	b_reg_gen: for i in b'range generate
		b_reg: reg
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_d	=> i_b(i),
				i_clk	=> i_clk,
				i_rst	=> '0',
				i_ld	=> rst,
				o_d	=> b(i)
			);
	end generate b_reg_gen;

	-- x registers containing current and previous data values
	x_reg_0: reg
		generic map (
			N_BITS	=> N_BITS
		)
		port map (
			i_d	=> i_d,
			i_clk	=> i_clk,
			i_rst	=> rst,
			i_ld	=> i_v,
			o_d	=> x(0)
		);

	x_reg_gen: for i in 1 to N_ORDER generate
		x_reg: reg
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_d	=> x(i - 1),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> x(i)
			);
	end generate x_reg_gen;

	p_mult_gen: for i in p'range generate
		p_mult: multiplier
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_a	=> x(i),
				i_b	=> b(i),
				o_p	=> p(i)
			);
	end generate p_mult_gen;

	-- products normalization (data and b coefficients are fixed point numbers
	--	left-shifted by N_BITS - 1)
	p_norm_gen: for i in p'range generate
		p_norm(i) <= std_logic_vector(shift_right(signed(p(i)), (N_BITS - 1)));
	end generate p_norm_gen;

	s(0) <= p_norm(0);
	s_add_gen: for i in 1 to p'right generate
		s_add: adder
			generic map (
				N_BITS	=> (2 * N_BITS)
			)
			port map (
				i_a	=> pipe(i - 1)(i - 1),
				i_b	=> pipe(i - 1)(i),
				o_s	=> s(i)
			);
	end generate s_add_gen;

	sum_reg_0: reg
		generic map (
			N_BITS	=> (2 * N_BITS)
		)
		port map (
			i_d	=> s(0),
			i_clk	=> i_clk,
			i_rst	=> rst,
			i_ld	=> i_v,
			o_d	=> pipe(0)(0)
		);

	mul_pipe_0: for i in 1 to N_ORDER generate
		mul_reg: reg
			generic map (
				N_BITS	=> (2 * N_BITS)
			)
			port map (
				i_d	=> p_norm(i),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> pipe(0)(i)
			);
	end generate mul_pipe_0;
	
	pipe_gen: for i in 1 to (N_ORDER - 1) generate
		-- pipeline registers insertion between adders
		sum_reg: reg
			generic map (
				N_BITS	=> (2 * N_BITS)
			)
			port map (
				i_d	=> s(i),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> pipe(i)(i)
			);

		-- pipeline registers insertion between multipliers and adders
		mul_pipe: for j in (i + 1) to N_ORDER generate
			mul_reg: reg
				generic map (
					N_BITS	=> (2 * N_BITS)
				)
				port map (
					i_d	=> pipe(i - 1)(j),
					i_clk	=> i_clk,
					i_rst	=> rst,
					i_ld	=> i_v,
					o_d	=> pipe(i)(j)
				);
		end generate mul_pipe;
	end generate pipe_gen;

	d_reg: reg
		generic map (
			N_BITS	=> o_d'length
		)
		port map (
			i_d	=> s(s'right)(o_d'range),
			i_clk	=> i_clk,
			i_rst	=> rst,
			i_ld	=> i_v,
			o_d	=> o_d
		);

	-- any data entering at time T will generate a valid output N_ORDER + 1
	--	cycles after
	v_shift_register: shift_register
		generic map (
			N_BITS	=> (N_ORDER + 2)
		)
		port map (
			i_d	=> i_v,
			i_clk	=> i_clk,
			i_rst	=> rst,
			o_d	=> o_v
		);
end architecture structural;

