library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.port_types.all;

-- fir_unfold_pipe: finite impulse response filter which loads new values when i_v = '1';
--	o_v = '1' when o_d contains valid data
-- The architecture is N_UNFOLD-unfolded and fully pipelined.
entity fir_unfold_pipe is
	port (
		i_d:	in d_vector;
		i_v:	in std_logic;
		i_b:	in b_vector;
		i_rst_n:in std_logic;
		i_clk:	in std_logic;

		o_d:	out d_vector;
		o_v:	out std_logic
	);
end fir_unfold_pipe;

architecture structural of fir_unfold_pipe is
	component reg is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_clk:	in std_logic;
			i_ld:	in std_logic;
			i_rst:	in std_logic;	

			o_d:	out std_logic_vector((N_BITS - 1) downto 0)
		);
	end component reg;

	component shift_register is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_d:	in std_logic;
			i_clk:	in std_logic;
			i_rst:	in std_logic;

			o_d:	out std_logic
		);
	end component shift_register;

	component multiplier is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_p:	out std_logic_vector(((2 * N_BITS) - 1) downto 0)
		);
	end component multiplier;

	component adder is
		generic (
			N_BITS:	integer := 8
		);
		port (
			i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

			o_s:	out std_logic_vector((N_BITS - 1) downto 0)
		);
	end component adder;

	type n_bits_vector is array (0 to N_ORDER) of
		std_logic_vector((N_BITS - 1) downto 0);
	type n_bits_matrix is array (0 to (N_UNFOLDING - 1)) of
		n_bits_vector;
	type n2_bits_vector is array (0 to N_ORDER) of
		std_logic_vector(((2 * N_BITS) - 1) downto 0);
	type n2_bits_matrix is array (0 to (N_UNFOLDING - 1)) of
		n2_bits_vector;
	type prod_pipe_vector is array (0 to (N_ORDER - 1)) of n2_bits_vector;
	type prod_pipe_matrix is array (0 to (N_UNFOLDING - 1)) of prod_pipe_vector;

	signal x:	n_bits_matrix;
	signal x_del:	n_bits_vector;
	signal b:	b_vector;
	signal p:	n2_bits_matrix;
	signal p_norm:	n2_bits_matrix;
	signal p_pipe:	prod_pipe_matrix;
	signal s:	n2_bits_matrix;
	signal s_pipe:	n2_bits_matrix;
	signal rst:	std_logic;
begin
	rst <= (not i_rst_n);

	-- registers storing b coefficients
	b_reg_gen: for i in b'range generate
		b_reg: reg
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_d	=> i_b(i),
				i_clk	=> i_clk,
				i_rst	=> '0',
				i_ld	=> rst,
				o_d	=> b(i)
			);
	end generate b_reg_gen;

	-- registers which introduce a delay of 1 to last stage values
	x_del_gen: for i in x_del'range generate
		x_del_0: reg
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_d	=> x(x'right)(i),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> x_del(i)
			);
	end generate x_del_gen;

	unfold_gen: for i in x'range generate
		d_in_reg: reg
			generic map (
				N_BITS	=> N_BITS
			)
			port map (
				i_d	=> i_d(i),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> x(i)(0)
			);

		-- interconnections used by following multipliers
		x_net_gen: for j in 1 to x(i)'right generate
			-- first unfolded stage is connected to last stage
			--	values, delayed by 1
			x_net_0: if (i = 0) generate
				x(i)(j) <= x_del(j - 1);
			end generate x_net_0;

			-- non-first unfolded stages are connected to previous
			--	stage values
			x_net_1: if (i > 0) generate
				x(i)(j) <= x(i - 1)(j - 1);
			end generate x_net_1;
		end generate x_net_gen;

		p_mult_gen: for j in p(i)'range generate
			p_mult: multiplier
				generic map (
					N_BITS	=> N_BITS
				)
				port map (
					i_a	=> x(i)(j),
					i_b	=> b(j),
					o_p	=> p(i)(j)
				);
		end generate p_mult_gen;

	-- products normalization (data and b coefficients are fixed point numbers
	--	left-shifted by N_BITS - 1)
		p_norm_gen: for j in p(i)'range generate
			p_norm(i)(j) <= std_logic_vector(shift_right(signed(p(i)(j)), (N_BITS - 1)));
		end generate p_norm_gen;

		-- pipeline registers insertion between multipliers and adders
		p_pipe_gen: for j in 0 to (N_ORDER - 1) generate
			p_pipe_0: reg
				generic map (
					N_BITS	=> (2 * N_BITS)
				)
				port map (
					i_d	=> p_norm(i)(j),
					i_clk	=> i_clk,
					i_rst	=> rst,
					i_ld	=> i_v,
					o_d	=> p_pipe(i)(j)(N_ORDER - j - 1)
				);

			p_pipe_j_gen: for k in (N_ORDER - j - 2) downto 0 generate
				p_pipe_reg: reg
					generic map (
						N_BITS	=> (2 * N_BITS)
					)
					port map (
						i_d	=> p_pipe(i)(j)(k + 1),
						i_clk	=> i_clk,
						i_rst	=> rst,
						i_ld	=> i_v,
						o_d	=> p_pipe(i)(j)(k)
					);
				end generate p_pipe_j_gen;
		end generate p_pipe_gen;

		s(i)(s(i)'right) <= p_norm(i)(p_norm(i)'right);
		s_add_gen: for j in (s(i)'right - 1) downto 0 generate
			s_add: adder
				generic map (
					N_BITS	=> (2 * N_BITS)
				)
				port map (
					i_a	=> p_pipe(i)(j)(0),
					i_b	=> s_pipe(i)(j + 1),
					o_s	=> s(i)(j)
				);
		end generate s_add_gen;

		-- pipeline registers insertion between adders
		s_pipe_gen: for j in 1 to N_ORDER generate
			s_pipe_reg: reg
				generic map (
					N_BITS	=> (2 * N_BITS)
				)
				port map (
					i_d	=> s(i)(j),
					i_clk	=> i_clk,
					i_rst	=> rst,
					i_ld	=> i_v,
					o_d	=> s_pipe(i)(j)
				);
		end generate s_pipe_gen;

		d_out_reg: reg
			generic map (
				N_BITS	=> o_d(i)'length
			)
			port map (
				i_d	=> s(i)(0)(o_d(i)'range),
				i_clk	=> i_clk,
				i_rst	=> rst,
				i_ld	=> i_v,
				o_d	=> o_d(i)
			);
	end generate unfold_gen;

	-- any data entering at time T will generate a valid output N_ORDER + 2
	--	cycles after
	v_shift_register: shift_register
		generic map (
			N_BITS	=> (N_ORDER + 2)
		)
		port map (
			i_d	=> i_v,
			i_clk	=> i_clk,
			i_rst	=> rst,
			o_d	=> o_v
		);
end architecture structural;

