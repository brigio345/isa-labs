library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;
use work.port_types.all;

-- fir8_unfold3: wrapper of fir, of order 8 (needed for instantiating in Verilog tb),
--	3-unfolded
entity fir8_unfold3 is
	port (
		i_d0:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_d1:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_d2:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_v:	in std_logic;
		i_b0:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b1:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b2:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b3:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b4:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b5:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b6:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b7:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b8:	in std_logic_vector((N_BITS - 1) downto 0);
		i_rst_n:in std_logic;
		i_clk:	in std_logic;

		o_d0:	out std_logic_vector((N_BITS - 1) downto 0);
		o_d1:	out std_logic_vector((N_BITS - 1) downto 0);
		o_d2:	out std_logic_vector((N_BITS - 1) downto 0);
		o_v:	out std_logic
	);
end fir8_unfold3;

architecture structural of fir8_unfold3 is
	component fir_unfold is
		port (
			i_d:	in d_vector;	
			i_v:	in std_logic;
			i_b:	in b_vector;
			i_rst_n:in std_logic;
			i_clk:	in std_logic;

			o_d:	out d_vector;
			o_v:	out std_logic
		);
	end component fir_unfold;

	signal b:	b_vector;
	signal d_in:	d_vector;
	signal d_out:	d_vector;
begin
	fir_0: fir_unfold
		port map (
			i_d	=> d_in,
			i_v	=> i_v,
			i_b	=> b,
			i_rst_n	=> i_rst_n,
			i_clk	=> i_clk,
			o_d	=> d_out,
			o_v	=> o_v
		);

	b(0)	<= i_b0;
	b(1)	<= i_b1;
	b(2)	<= i_b2;
	b(3)	<= i_b3;
	b(4)	<= i_b4;
	b(5)	<= i_b5;
	b(6)	<= i_b6;
	b(7)	<= i_b7;
	b(8)	<= i_b8;

	d_in(0)	<= i_d0;
	d_in(1)	<= i_d1;
	d_in(2)	<= i_d2;

	o_d0	<= d_out(0);
	o_d1	<= d_out(1);
	o_d2	<= d_out(2);
end structural;

