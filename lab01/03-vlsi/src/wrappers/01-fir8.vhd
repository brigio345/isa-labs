library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;
use work.port_types.all;

-- fir8: wrapper of fir, of order 8 (this wrapper is needed because Verilog
--	do not support custom type ports when instantiating VHDL components)
entity fir8 is
	port (
		i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_v:	in std_logic;
		i_b0:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b1:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b2:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b3:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b4:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b5:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b6:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b7:	in std_logic_vector((N_BITS - 1) downto 0);
		i_b8:	in std_logic_vector((N_BITS - 1) downto 0);
		i_rst_n:in std_logic;
		i_clk:	in std_logic;

		o_d:	out std_logic_vector((N_BITS - 1) downto 0);
		o_v:	out std_logic
	);
end fir8;

architecture structural of fir8 is
	component fir is
		port (
			i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
			i_v:	in std_logic;
			i_b:	in b_vector;
			i_rst_n:in std_logic;
			i_clk:	in std_logic;

			o_d:	out std_logic_vector((N_BITS - 1) downto 0);
			o_v:	out std_logic
		);
	end component fir;

	signal b:	b_vector;
begin
	fir_0: fir
		port map (
			i_d	=> i_d,
			i_v	=> i_v,
			i_b	=> b,
			i_rst_n	=> i_rst_n,
			i_clk	=> i_clk,
			o_d	=> o_d,
			o_v	=> o_v
		);

	b(0)	<= i_b0;
	b(1)	<= i_b1;
	b(2)	<= i_b2;
	b(3)	<= i_b3;
	b(4)	<= i_b4;
	b(5)	<= i_b5;
	b(6)	<= i_b6;
	b(7)	<= i_b7;
	b(8)	<= i_b8;
end structural;

