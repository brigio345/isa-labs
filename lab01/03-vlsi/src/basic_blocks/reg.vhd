library ieee;
use ieee.std_logic_1164.all;

entity reg is
	generic (
		N_BITS:	integer := 8
	);
	port (
		i_d:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_clk:	in std_logic;
		i_ld:	in std_logic;
		i_rst:	in std_logic;	

		o_d:	out std_logic_vector((N_BITS - 1) downto 0)
	);
end reg;

architecture behavioral of reg is
begin
	process(i_clk)
	begin
		if (i_clk = '1' and i_clk'event) then
			if (i_rst = '1') then
				o_d	<= (others => '0');
			elsif (i_ld = '1') then
				o_d	<= i_d;
			end if;
		end if;
	end process;
end behavioral;

