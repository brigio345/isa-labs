library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiplier is
	generic (
		N_BITS:	integer := 8
	);
	port (
		i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

		o_p:	out std_logic_vector(((2 * N_BITS) - 1) downto 0)
	);
end multiplier;

architecture behavioral of multiplier is
begin
	o_p <= std_logic_vector(signed(i_a) * signed(i_b));
end behavioral;

