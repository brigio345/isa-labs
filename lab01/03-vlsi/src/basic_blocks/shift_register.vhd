library ieee;
use ieee.std_logic_1164.all;

entity shift_register is
	generic (
		N_BITS:	integer := 8
	);
	port (
		i_d:	in std_logic;
		i_clk:	in std_logic;
		i_rst:	in std_logic;

		o_d:	out std_logic
	);
end shift_register;

architecture behavioral of shift_register is
	signal reg_curr:	std_logic_vector(0 to (N_BITS - 1));
	signal reg_next:	std_logic_vector(0 to (N_BITS - 1));
begin
	process(i_clk)
	begin
		if (i_clk = '1' and i_clk'event) then
			if (i_rst = '1') then
				reg_curr <= (others => '0');
			else
				reg_curr <= reg_next;
			end if;
		end if;
	end process;

	reg_next <= (i_d & reg_curr(0 to (reg_curr'right - 1)));

	o_d <= reg_curr(reg_curr'right);
end behavioral;

