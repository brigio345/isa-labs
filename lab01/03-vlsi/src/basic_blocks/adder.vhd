library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
	generic (
		N_BITS:	integer := 8
	);
	port (
		i_a:	in std_logic_vector((N_BITS - 1) downto 0);	
		i_b:	in std_logic_vector((N_BITS - 1) downto 0);	

		o_s:	out std_logic_vector((N_BITS - 1) downto 0)
	);
end adder;

architecture behavioral of adder is
begin
	o_s <= std_logic_vector(signed(i_a) + signed(i_b));
end behavioral;

