package constants is
	constant N_ORDER:	natural := 8;
	constant N_BITS:	natural := 11;
	constant N_UNFOLDING:	natural := 3;
end package constants;

