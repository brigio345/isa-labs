library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;

package port_types is
	type b_vector is
		array (0 to N_ORDER) of
		std_logic_vector((N_BITS - 1) downto 0);

	type d_vector is
		array (0 to (N_UNFOLDING - 1)) of
		std_logic_vector((N_BITS - 1) downto 0);
end package port_types;

