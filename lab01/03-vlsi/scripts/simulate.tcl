set proj_root "~/giovanni/lab01/03-vlsi/"

set src_vhdl [list				\
	"src/basic_blocks/adder.vhd"		\
	"src/basic_blocks/multiplier.vhd"	\
	"src/basic_blocks/reg.vhd"		\
	"src/basic_blocks/shift_register.vhd"	\
	"src/utilities/constants.vhd"		\
	"src/utilities/port_types.vhd"		\
	"src/01-fir.vhd"			\
	"src/02-fir_pipe.vhd"			\
	"src/03-fir_unfold.vhd"			\
	"src/04-fir_unfold_pipe.vhd"		\
	"src/wrappers/01-fir8.vhd"		\
	"src/wrappers/02-fir8_pipe.vhd"		\
	"src/wrappers/03-fir8_unfold3.vhd"	\
	"src/wrappers/04-fir8_unfold3_pipe.vhd"
]

set src_verilog [list				\
	"tb/01-tb_fir8.v"			\
	"tb/02-tb_fir8_pipe.v"			\
	"tb/03-tb_fir8_unfold3.v"		\
	"tb/04-tb_fir8_unfold3_pipe.v"
]

set testbenches [list				\
	"tb_fir8"				\
	"tb_fir8_pipe"				\
	"tb_fir8_unfold3"			\
	"tb_fir8_unfold3_pipe"
]

foreach vhdl_file $src_vhdl {
	vcom $proj_root$vhdl_file
}

foreach verilog_file $src_verilog {
	vlog $proj_root$verilog_file
}

foreach tb $testbenches {
	vsim -c $tb
	run -all
}

exit

