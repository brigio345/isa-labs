source .synopsys_dc.setup

set proj_root "~/giovanni/lab01/03-vlsi/"

set src_vhdl [list				\
	"src/basic_blocks/adder.vhd"		\
	"src/basic_blocks/multiplier.vhd"	\
	"src/basic_blocks/reg.vhd"		\
	"src/basic_blocks/shift_register.vhd"	\
	"src/utilities/constants.vhd"		\
	"src/utilities/port_types.vhd"		\
	"src/01-fir.vhd"			\
	"src/02-fir_pipe.vhd"			\
	"src/03-fir_unfold.vhd"			\
	"src/04-fir_unfold_pipe.vhd"
]

set top_entity "fir"

set out_dir "netlist"

# setup synthesis
foreach vhdl_file $src_vhdl {
	analyze -format vhdl -lib WORK $proj_root$vhdl_file
}

set power_preserve_rtl_hier_names true
elaborate $top_entity -arch structural -lib WORK

uniquify
link
ungroup -all -flatten

set t_clk 0.0
set clk_uncertainty 0.07
set in_delay 0.5
set out_delay 0.5
set out_load [load_of NangateOpenCellLibrary/BUF_X4/A]

create_clock -name myclock -period $t_clk  i_clk 
set_dont_touch_network myclock
set_clock_uncertainty $clk_uncertainty [get_clocks myclock]
set_fix_hold [get_clocks myclock]
set_input_delay $in_delay -max -clock myclock [remove_from_collection [all_inputs] i_clk]
set_output_delay $out_delay -max -clock myclock [all_outputs]
set_load $out_load [all_outputs]
set_max_delay -from [all_inputs] -to [all_outputs] $t_clk

# execute synthesis
compile_ultra

# write outputs
if {[file exist $out_dir] == 0} {
	exec mkdir $out_dir
}

report_area > $out_dir/area_$top_entity-t_clk_$t_clk.rpt
report_timing > $out_dir/timing_$top_entity-t_clk_$t_clk.rpt

change_names -hierarchy -rules verilog
write_sdf  $out_dir/$top_entity-t_clk_$t_clk.sdf
write -f verilog -hierarchy -output $out_dir/$top_entity-t_clk_$t_clk.v
write_sdc $out_dir/$top_entity-t_clk_$t_clk.sdc

