###################################################################

# Created by write_sdc on Tue Nov 17 11:17:13 2020

###################################################################
set sdc_version 2.1

set_units -time ns -resistance MOhm -capacitance fF -voltage V -current mA
set_load -pin_load 3.40189 [get_ports {o_d[32]}]
set_load -pin_load 3.40189 [get_ports {o_d[31]}]
set_load -pin_load 3.40189 [get_ports {o_d[30]}]
set_load -pin_load 3.40189 [get_ports {o_d[29]}]
set_load -pin_load 3.40189 [get_ports {o_d[28]}]
set_load -pin_load 3.40189 [get_ports {o_d[27]}]
set_load -pin_load 3.40189 [get_ports {o_d[26]}]
set_load -pin_load 3.40189 [get_ports {o_d[25]}]
set_load -pin_load 3.40189 [get_ports {o_d[24]}]
set_load -pin_load 3.40189 [get_ports {o_d[23]}]
set_load -pin_load 3.40189 [get_ports {o_d[22]}]
set_load -pin_load 3.40189 [get_ports {o_d[21]}]
set_load -pin_load 3.40189 [get_ports {o_d[20]}]
set_load -pin_load 3.40189 [get_ports {o_d[19]}]
set_load -pin_load 3.40189 [get_ports {o_d[18]}]
set_load -pin_load 3.40189 [get_ports {o_d[17]}]
set_load -pin_load 3.40189 [get_ports {o_d[16]}]
set_load -pin_load 3.40189 [get_ports {o_d[15]}]
set_load -pin_load 3.40189 [get_ports {o_d[14]}]
set_load -pin_load 3.40189 [get_ports {o_d[13]}]
set_load -pin_load 3.40189 [get_ports {o_d[12]}]
set_load -pin_load 3.40189 [get_ports {o_d[11]}]
set_load -pin_load 3.40189 [get_ports {o_d[10]}]
set_load -pin_load 3.40189 [get_ports {o_d[9]}]
set_load -pin_load 3.40189 [get_ports {o_d[8]}]
set_load -pin_load 3.40189 [get_ports {o_d[7]}]
set_load -pin_load 3.40189 [get_ports {o_d[6]}]
set_load -pin_load 3.40189 [get_ports {o_d[5]}]
set_load -pin_load 3.40189 [get_ports {o_d[4]}]
set_load -pin_load 3.40189 [get_ports {o_d[3]}]
set_load -pin_load 3.40189 [get_ports {o_d[2]}]
set_load -pin_load 3.40189 [get_ports {o_d[1]}]
set_load -pin_load 3.40189 [get_ports {o_d[0]}]
set_load -pin_load 3.40189 [get_ports o_v]
create_clock [get_ports i_clk]  -name myclock  -period 0  -waveform {0 0}
set_clock_uncertainty 0.07  [get_clocks myclock]
set_max_delay 0  -from [list [get_ports {i_d[32]}] [get_ports {i_d[31]}] [get_ports {i_d[30]}] \
[get_ports {i_d[29]}] [get_ports {i_d[28]}] [get_ports {i_d[27]}] [get_ports   \
{i_d[26]}] [get_ports {i_d[25]}] [get_ports {i_d[24]}] [get_ports {i_d[23]}]   \
[get_ports {i_d[22]}] [get_ports {i_d[21]}] [get_ports {i_d[20]}] [get_ports   \
{i_d[19]}] [get_ports {i_d[18]}] [get_ports {i_d[17]}] [get_ports {i_d[16]}]   \
[get_ports {i_d[15]}] [get_ports {i_d[14]}] [get_ports {i_d[13]}] [get_ports   \
{i_d[12]}] [get_ports {i_d[11]}] [get_ports {i_d[10]}] [get_ports {i_d[9]}]    \
[get_ports {i_d[8]}] [get_ports {i_d[7]}] [get_ports {i_d[6]}] [get_ports      \
{i_d[5]}] [get_ports {i_d[4]}] [get_ports {i_d[3]}] [get_ports {i_d[2]}]       \
[get_ports {i_d[1]}] [get_ports {i_d[0]}] [get_ports i_v] [get_ports           \
{i_b[98]}] [get_ports {i_b[97]}] [get_ports {i_b[96]}] [get_ports {i_b[95]}]   \
[get_ports {i_b[94]}] [get_ports {i_b[93]}] [get_ports {i_b[92]}] [get_ports   \
{i_b[91]}] [get_ports {i_b[90]}] [get_ports {i_b[89]}] [get_ports {i_b[88]}]   \
[get_ports {i_b[87]}] [get_ports {i_b[86]}] [get_ports {i_b[85]}] [get_ports   \
{i_b[84]}] [get_ports {i_b[83]}] [get_ports {i_b[82]}] [get_ports {i_b[81]}]   \
[get_ports {i_b[80]}] [get_ports {i_b[79]}] [get_ports {i_b[78]}] [get_ports   \
{i_b[77]}] [get_ports {i_b[76]}] [get_ports {i_b[75]}] [get_ports {i_b[74]}]   \
[get_ports {i_b[73]}] [get_ports {i_b[72]}] [get_ports {i_b[71]}] [get_ports   \
{i_b[70]}] [get_ports {i_b[69]}] [get_ports {i_b[68]}] [get_ports {i_b[67]}]   \
[get_ports {i_b[66]}] [get_ports {i_b[65]}] [get_ports {i_b[64]}] [get_ports   \
{i_b[63]}] [get_ports {i_b[62]}] [get_ports {i_b[61]}] [get_ports {i_b[60]}]   \
[get_ports {i_b[59]}] [get_ports {i_b[58]}] [get_ports {i_b[57]}] [get_ports   \
{i_b[56]}] [get_ports {i_b[55]}] [get_ports {i_b[54]}] [get_ports {i_b[53]}]   \
[get_ports {i_b[52]}] [get_ports {i_b[51]}] [get_ports {i_b[50]}] [get_ports   \
{i_b[49]}] [get_ports {i_b[48]}] [get_ports {i_b[47]}] [get_ports {i_b[46]}]   \
[get_ports {i_b[45]}] [get_ports {i_b[44]}] [get_ports {i_b[43]}] [get_ports   \
{i_b[42]}] [get_ports {i_b[41]}] [get_ports {i_b[40]}] [get_ports {i_b[39]}]   \
[get_ports {i_b[38]}] [get_ports {i_b[37]}] [get_ports {i_b[36]}] [get_ports   \
{i_b[35]}] [get_ports {i_b[34]}] [get_ports {i_b[33]}] [get_ports {i_b[32]}]   \
[get_ports {i_b[31]}] [get_ports {i_b[30]}] [get_ports {i_b[29]}] [get_ports   \
{i_b[28]}] [get_ports {i_b[27]}] [get_ports {i_b[26]}] [get_ports {i_b[25]}]   \
[get_ports {i_b[24]}] [get_ports {i_b[23]}] [get_ports {i_b[22]}] [get_ports   \
{i_b[21]}] [get_ports {i_b[20]}] [get_ports {i_b[19]}] [get_ports {i_b[18]}]   \
[get_ports {i_b[17]}] [get_ports {i_b[16]}] [get_ports {i_b[15]}] [get_ports   \
{i_b[14]}] [get_ports {i_b[13]}] [get_ports {i_b[12]}] [get_ports {i_b[11]}]   \
[get_ports {i_b[10]}] [get_ports {i_b[9]}] [get_ports {i_b[8]}] [get_ports     \
{i_b[7]}] [get_ports {i_b[6]}] [get_ports {i_b[5]}] [get_ports {i_b[4]}]       \
[get_ports {i_b[3]}] [get_ports {i_b[2]}] [get_ports {i_b[1]}] [get_ports      \
{i_b[0]}] [get_ports i_rst_n] [get_ports i_clk]]  -to [list [get_ports {o_d[32]}] [get_ports {o_d[31]}] [get_ports {o_d[30]}]   \
[get_ports {o_d[29]}] [get_ports {o_d[28]}] [get_ports {o_d[27]}] [get_ports   \
{o_d[26]}] [get_ports {o_d[25]}] [get_ports {o_d[24]}] [get_ports {o_d[23]}]   \
[get_ports {o_d[22]}] [get_ports {o_d[21]}] [get_ports {o_d[20]}] [get_ports   \
{o_d[19]}] [get_ports {o_d[18]}] [get_ports {o_d[17]}] [get_ports {o_d[16]}]   \
[get_ports {o_d[15]}] [get_ports {o_d[14]}] [get_ports {o_d[13]}] [get_ports   \
{o_d[12]}] [get_ports {o_d[11]}] [get_ports {o_d[10]}] [get_ports {o_d[9]}]    \
[get_ports {o_d[8]}] [get_ports {o_d[7]}] [get_ports {o_d[6]}] [get_ports      \
{o_d[5]}] [get_ports {o_d[4]}] [get_ports {o_d[3]}] [get_ports {o_d[2]}]       \
[get_ports {o_d[1]}] [get_ports {o_d[0]}] [get_ports o_v]]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[32]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[31]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[30]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[29]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[28]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[27]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[26]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[25]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[24]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[23]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[22]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[21]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[20]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[19]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[18]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[17]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[16]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[15]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[14]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[13]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[12]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[11]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[10]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[9]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[8]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[7]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[6]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[5]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[4]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[3]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[2]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[1]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_d[0]}]
set_input_delay -clock myclock  -max 0.5  [get_ports i_v]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[98]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[97]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[96]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[95]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[94]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[93]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[92]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[91]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[90]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[89]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[88]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[87]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[86]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[85]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[84]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[83]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[82]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[81]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[80]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[79]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[78]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[77]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[76]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[75]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[74]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[73]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[72]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[71]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[70]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[69]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[68]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[67]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[66]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[65]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[64]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[63]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[62]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[61]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[60]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[59]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[58]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[57]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[56]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[55]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[54]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[53]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[52]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[51]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[50]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[49]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[48]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[47]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[46]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[45]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[44]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[43]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[42]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[41]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[40]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[39]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[38]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[37]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[36]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[35]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[34]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[33]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[32]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[31]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[30]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[29]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[28]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[27]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[26]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[25]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[24]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[23]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[22]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[21]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[20]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[19]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[18]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[17]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[16]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[15]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[14]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[13]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[12]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[11]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[10]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[9]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[8]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[7]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[6]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[5]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[4]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[3]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[2]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[1]}]
set_input_delay -clock myclock  -max 0.5  [get_ports {i_b[0]}]
set_input_delay -clock myclock  -max 0.5  [get_ports i_rst_n]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[32]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[31]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[30]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[29]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[28]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[27]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[26]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[25]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[24]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[23]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[22]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[21]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[20]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[19]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[18]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[17]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[16]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[15]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[14]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[13]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[12]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[11]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[10]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[9]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[8]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[7]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[6]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[5]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[4]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[3]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[2]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[1]}]
set_output_delay -clock myclock  -max 0.5  [get_ports {o_d[0]}]
set_output_delay -clock myclock  -max 0.5  [get_ports o_v]
