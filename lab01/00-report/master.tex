\chapter{Introduction to digital filters design}
Digital filters are systems which perform mathematical operations on \textbf{sampled},
\textbf{discrete-time signals}.

The type of digital filter designed in this laboratory is linear and specifically
it is a \textbf{FIR filter} (whose impulse response is nonzero for only a finite
number of samples), instead of an IIR filter (whose impulse response has an
infinite number of nonzero samples). 

The equation implemented by a FIR filter is:
\[y[n] = \sum\limits_{i = 0}^{N} (x[n - i] \cdot b[i])\]

\vspace{0.8 cm}
The followed design flow involved:
  \begin{enumerate}
	  \item \textbf{Defining the filter specifications:} \\
		  filter \underline{cut-off frequency}, \underline{sampling frequency},
		  \underline{order} and \underline{number of bits} are defined
	  \item \textbf{Finding the coefficients:} \\
		 the filter coefficients are obtained by means of MATLAB
		 library functions
	 \item \textbf{Implementing the reference model:} \\
		 a reference model of the filter is implemented in software
		 (in MATLAB and C)
	 \item \textbf{Implementing the hardware filter:}
		 \begin{itemize}
			 \item \underline{Choosing the structure:}
				 one of the many structures which can implement
				 the FIR transfer function is choosen
				 (for instance direct I, direct II, cascade,
				 parallel or transposed form)
			 \item \underline{Design:}
				 the circuit is designed in VHDL
			 \item \underline{Architecture optimization:}
				 the designed circuit is optimized by means
				 of generic optimization techniques such as
				 pipelining and unfolding
			 \item \underline{Simulation:}
				 the testbench is designed in Verilog.
				 The ouputs of the simulation are compared
				 the ones of the C software implementation
			 \item \underline{Synthesis and physical design:}
				 the hardware design is synthesized by Synopsys
				 Design Compiler, and placed and routed by
				 Cadence SOC Innovus
		 \end{itemize}
\end{enumerate}

\chapter{From specifications to reference model}
\section{Defining the filter specifications}
The assignment specifications require a filter with:
\[
	\begin{cases}
		f_{cut-off} & = 2 kHz \\
		f_{sampling} & = 10 kHz
	\end{cases}
\]
The order of the filter and the number of bits (to represent the coefficients,
the input and the output) are determined by a function depending on group number
and member surnames:
	\begin{listato}
	\lstinputlisting{./code/Order-bits.m}
	\end{listato}

\[
	\Rightarrow
	\begin{cases}
		N & = 8 \\
		n_{bits} & = 11
	\end{cases}
\]

\section{Finding the coefficients}
The coefficients of the filter complying with the specifications are obtained
by means of MATLAB \texttt{fir1} function and are then converted to fixed point
numbers on $n_{bits}$ bits (stored in integer variables), in order to be easily
used in an hardware digital implementation:
\lstset{style=mystyle}
\begin{lstlisting}[language=matlab]
b = fir1(N, f0);		% filter coefficients
bi = floor(b * 2 ^ (nb - 1));	% quantized fixed point filter coefficients
\end{lstlisting}

\section{Implementing the reference model}
\subsection{Pseudo-fixed-point MATLAB model}
MATLAB language provides the library function \texttt{filter} which implements
digital filtering.

This function is therefore used to generate reference outputs useful to check
correctness and accuracy of other implementations.

\subsubsection{Testing}
Two sinusoidal waves at two different frequencies, one in band and the other one
out of the band, have been combined together and then applied to the MATLAB filter.

\[
	\begin{cases}
		x = \sin (2 \pi f_1 T) + \sin (2 \pi f_2 T) \\
		f_1 = 500 Hz \\
		f_2 = 4500 Hz
	\end{cases}
\]
    
\bigskip
Figure~\ref{fig:matlab_plot} shows the output of the filter, alongside with the
signal applied to the filter.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{Test-matlab.jpg}
	\caption{Applying test signals to filter}
	\label{fig:matlab_plot}
	\end{figure}

\subsection{Fixed-point C model}
Since the hardware implementation of the filter will be done using fixed-point
arithmetics (due to the high convenience in terms of area, performance, power
and ease of design) and the MATLAB \texttt{filter} function implements the
filtering with arbitrary-precision arithmetics, a C fixed-point implementation
of the filter is useful to directly compare the single samples.

This C implementation is a more OOP-oriented code equivalent to the one provided
by assignment, with the only difference that outputs are truncated on $n_{bits}$
bits.
The most relevant part of the code is the \texttt{fir\_sample} function:

\begin{lstlisting}[language=C, tabsize=4]
int fir_sample(fir_t fir, int x)
{
	int i;
	int y;

	// shift all x samples by 1 position
	for (i = fir->n_order; i > 0; i--)
		fir->x[i] = fir->x[i - 1];
	fir->x[0] = x;

	// compute output at full precision
	// (since coefficients were obtained by: b_int = floor(b_float << (n_bits - 1))
	// products are shifted back to the right to recover original interval)
	y = 0;
	for (i = 0; i <= fir->n_order; i++)
		y += ((fir->b[i] * fir->x[i]) >> (fir->n_bits - 1));

	// truncate y to fir->n_bits bits:
	// if y >= 0 set y MSBs to 0, else set them to 1
	y = (y >= 0) ? (y & (~fir->mask)) : (y | fir->mask);

	// N.B.: fir->mask was computed by:
	// set all to '1' except for n_bits LSBs
	// fir->mask	= (-1 << n_bits);

	return y;
}
\end{lstlisting}

\bigskip
It is worth noting that:
\begin{itemize}
	\item \texttt{b} coefficients should be computed externally and provided
		as inputs
	\item fixed-point values of \texttt{x} and \texttt{b} are obtained from
		left-shifting by \texttt{n\_bits - 1} positions, thus products
		are brought back to original interval by right-shifting
	\item truncation on \texttt{n\_bits} bits is performed only on the final
		result: partial sums are stored in the whole \texttt{int} variable,
		thus overflow probability is reduced
\end{itemize}

\subsubsection{Testing}
The same \texttt{b} coefficients and the same samples which had been applied to
the MATLAB \texttt{filter} (but this time quantized) are applied to this implementation.

Figure~\ref{fig:c_matlab_plot} shows that the result is slightly different and
less precise in comparison with MATLAB result.
Nevertheless the total harmonic distortion computed by MATLAB function \texttt{thd}
is about -57 dB, which is more than acceptable.

  \begin{figure}[H]
	\centering
	\includegraphics[scale=0.2]{resultcomp.jpg}
	\caption{Comparison between C and MATLAB outputs}
	\label{fig:c_matlab_plot}
  \end{figure}
  
\chapter{VLSI Implementation}
\section{Choosing the structure}
Since a FIR filter could be implemented in different forms (each of them with
advantages and disadvantages in terms of area, performance, number of resources, etc.),
it is necessary to choose a realization structure.

\bigskip
The assignment requires to use \textbf{direct form}: the structure of a FIR filter
in that form, with order $N = 8$ is shown in figure~\ref{fig:fir_direct_form}.

\begin{figure}[H]
	\centering
	\includegraphics[width=.25\linewidth]{fir_no-opt.pdf}
	\caption{FIR Filter - Direct form structure}
	\label{fig:fir_direct_form}
\end{figure}

\section{Design}
A generic direct form FIR filter architecture, compliant with the interface shown
in figure~\ref{fig:fir_interface}, has been described in VHDL.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{Interface.png}
	\caption{Filter interface}
	\label{fig:fir_interface}
\end{figure}
	
This architecture implements the structure shown in figure~\ref{fig:fir_direct_form}
plus some additional parts:
\begin{itemize}
	\item one register at the \texttt{DIN} input and one at the \texttt{DOUT}
		output, used to keep data stable during the whole clock cycle
	\item \texttt{N\_ORDER} registers at the \texttt{b} inputs, loaded during
		reset phase only
	\item \texttt{VIN} signal: when set to \texttt{'1'} a new \texttt{DIN}
		value is loaded into register, otherwise it is ignored
	\item \texttt{VOUT} signal: set to \texttt{'1'} only when a new
		\texttt{DOUT} value is present (this is implemented by a 2-bits
		shift register, having \texttt{VIN} as input and \texttt{VOUT}
		as output)
\end{itemize}

Critical parts of the design include:
\begin{itemize}
	\item \underline{Multiplication:}
		the products has to be performed between two fixed point numbers,
		each with an integer part of 1 bit and a fractional part of
		\texttt{N\_BITS - 1}.
		In order to limit the fractional part to \texttt{N\_BITS - 1}
		a right shift of \texttt{N\_BITS - 1} bits is performed after
		each multiplication (note that this do not limit the integer
		part, which can grow up to \texttt{N\_BITS} bits)
	\item \underline{Truncation:}
		filter output must be on \texttt{N\_BITS} bits, but there is no
		constraint on internal representation of data: instead of
		truncating products as soon as they come out of the multiplier
		(which may lead to losing of integer part significant bits),
		adders on \texttt{2 * N\_BITS} bits are used: adding together
		differently-signed numbers may bring out-of-range data back to
		the range: only the final sum is therefore truncated on
		\texttt{n\_BITS} bits
\end{itemize}

\section{Architecture optimization}
\subsection{Pipelining}
The coarse grain pipelining technique have been applied on the FIR VHDL architecture,
in order to reduce as much as possible the critical path: the original FIR critical
path is passing through 1 multiplier and \texttt{N\_ORDER} adders, while the fully
pipelined FIR critical is passing through a multiplier or an adder only, as shown in
figure~\ref{fig:fir_pipe}.

The \texttt{VOUT} shift register have been enlarged to \texttt{N\_ORDER + 2} bits, since
computation of each sample have a latency of \texttt{N\_ORDER} clock cycles.
\begin{figure}[H]
	\centering
	\includegraphics[width=.25\linewidth]{fir_pipelined.pdf}
	\caption{Pipelined FIR architecture (\texttt{N\_ORDER = 8})}
	\label{fig:fir_pipe}
\end{figure}

\subsection{Unfolding}
The unfolding technique have been applied on the FIR VHDL architecture,
in order to increase the number of samples processed per clock cycle.
The VHDL code is designed in such a way that the unfolding order can be tuned
by simply setting the \texttt{N\_UNFOLD} constant. Figure~\ref{fig:fir_unfold} shows
an example architecture obtained with \texttt{N\_ORDER = 4} and \texttt{N\_UNFOLD = 3}.
Note that at each clock cycle \texttt{N\_UNFOLD} samples must be provided as input.
\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{fir_unfolded.pdf}
	\caption{Unfolded FIR architecture (\texttt{N\_ORDER = 4}, \texttt{N\_UNFOLD = 3})}
	\label{fig:fir_unfold}
\end{figure}

\subsection{Unfolding \& pipelining}
In order to maximize the throughput both the unfolding and the pipelining
techniques have been applied to the VHDL design: the implemented architecture
is shown in figure~\ref{fig:fir_unfold_pipe}.
\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{fir_unfolded_pipelined.pdf}
	\caption{Unfolded and pipelined FIR architecture (\texttt{N\_ORDER = 4}, \texttt{N\_UNFOLD = 3})}
	\label{fig:fir_unfold_pipe}
\end{figure}

\section{Simulation}
A Verilog testbench, for each FIR architecture, has been designed for testing
the filter: figure~\ref{fig:tb_interface} shows the tb structure from an
high-level point of view.

Since Verilog does not support VHDL custom types ports, the generically-described
filters have been inserted in VHDL wrappers with fixed-size ports.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{Testbench-architecture.png}
	\caption{Test-bench architecture}
	\label{fig:tb_interface}
\end{figure}

It is worth noting that \texttt{VOUT} values are written to file: this avoids
the need of going through the GUI to manually check the waveforms: in order to
check the outputs correctness it is enough to \texttt{diff} the
testbench-generated files against the file containing C outputs, in order to make
sure there is no difference among them.

In order to furtherly automate the simulation process, the \texttt{simulate.tcl}
script has been written: it compiles all the VHDL and Verilog files and then
runs the simulations of every designed architecture.

\bigskip
A simpler testbench showcasing the valid input and output signals have been
developed: its output waveforms are shown in figure~\ref{fig:valid_waves}.

\begin{figure}[H]
	\centering
	\includegraphics[width=.8\linewidth]{vhdl_fir_waves.png}
	\caption{Valid signals showcase}
	\label{fig:valid_waves}
\end{figure}

\section{Implementation}
\subsection{Logic synthesis}
The \textbf{standard FIR} and the \textbf{fully optimized FIR} (unfolded and pipelined)
have been synthesized using \textit{Synopsys Design Compiler Version O-2018.06-SP4}
and the \textit{Nandgate 45nm Open Cell Library}.

Since our main figure of merit was performance, the synthesis, for both the
architectures, have been performed in two steps:
\begin{enumerate}
	\item \textbf{Find the minimum clock period:}
		\begin{itemize}
			\item \underline{Clock period constraint}: 0
			\item \underline{Purpose}: force Design Compiler to put the
				highest effort in optimizing performance:
				the highest arrival time obtained with this
				synthesis is the minimum achievable clock period
				$T_{min}$
		\end{itemize}
	\item \textbf{Perform the actual synthesis:}
		\begin{itemize}
			\item \underline{Clock period constraint}: $T_{min}$
			\item \underline{Purpose}: check the absence of timing
				violations (since the tool works with heuristics
				it may not achieve same performance obtained in
				previous synthesis and the clock period constraint
				might require some tuning)
		\end{itemize}
\end{enumerate}

The whole synthesis process has been automated with the \texttt{synthesize.tcl}
script, located in \texttt{03-vlsi/scripts}, while the files generated by the
synthesis are located in \texttt{03-vlsi/syn/netlist}.
Table~\ref{tab:syn_summary} summarizes the synthesis outcome: it is clear that
the advantage in a lower clock period of the optimized FIR has been paid in
in terms of area (it contains 3 replicas of the FIR and all the additional
pipeline registers).

\begin{table}[H]
	\centering
	\begin{tabular}{lll}
		\hline
		\rowcolor{gray!50}
		& \textbf{Standard FIR}	& \textbf{Optimized FIR}	\\
		\hline
		Clock period [ns]	& 1.8		& 1.3		\\
		\rowcolor{gray!25}
		Area [$\mu m^2$]	& 6330.8	& 27141.3	\\
		\hline
	\end{tabular}
	\caption{Synthesis summary}
	\label{tab:syn_summary}
\end{table}

\subsubsection{Post-synthesis simulation}
The Verilog netlists generated by the synthesis have been then simulated,
for two reasons:
\begin{enumerate}
	\item \textbf{Verification}: check that the functional behaviour of the
		circuit has been preserved
	\item \textbf{Switching activity recording}: it is later used for the
		power consumption estimation
\end{enumerate}
Since Verilog do not support VHDL custom data types for modules ports, all ports
which in the original design were bi-dimentional logic vectors have been
incorporated into single-dimentional logic vectors by the synthesis.
For this reason the testbenches had been modified, to comply with the new interface.

\bigskip
The simulation has been perfomed using \textit{Modelsim 6.2}, after setting up
the \texttt{value-change-dump} on all the signals of the unit under test.
Notice that the clock was set to $4 T_{min}$, depending on the specific unit
under test.

The \texttt{vcd} file has been then converted to \texttt{saif} and used for the
power estimation by \textit{Design Compiler}.

\bigskip
Table~\ref{tab:pow_summary} displays the obtained values of power consumption.
It is worth noting that in the opimized FIR the static power consumption is
higher due to the higher amount of allocated modules and the dynamic power
consumption is higher due to higher clock frequency.

\begin{table}[H]
	\centering
	\begin{tabular}{lll}
		\hline
		\rowcolor{gray!50}
		& \textbf{Standard FIR}	& \textbf{Optimized FIR} \\
		\hline
		Clock period [ns]		& 7.2		& 5.2	\\
		\rowcolor{gray!25}
		Internal Power [$\mu W$]	& 578.06 	& 4525.4\\
		Switching Power [$\mu W$]	& 359.51 	& 2554.9\\
		\rowcolor{gray!25}
		Leakage Power [$\mu W$]		& 131.78	& 578.62\\
		Total Power [$\mu W$]		& 1069.3	& 7659.0\\
		\hline
	\end{tabular}
	\caption{Power consumption summary}
	\label{tab:pow_summary}
\end{table}

\subsection{Place \& Route}
The physical design is the process of turning the design into manufacturable
geometries; it comprises a number of steps:
\begin{enumerate}
		\item \underline{Floorplanning:}
			define some general parameters about the chip, such as
			the chip area, the fill ratio (ratio between cells area
			and total area), the die boundaries (which preserve the
			area that will be used to root the power supply)
		\item \underline{Power routing:}
			provide the connections for power supply and ground
			organized in rows.
		\item \underline{Placement:}
			determine the location of each component or block on the
			die, considering timing and interconnect length.
		\item \underline{Clock tree synthesis}
			try to achieve the required timing constraint. 
		\item \underline{Routing:}
			determine the paths of interconnects, completing all the
			connections defined in the netlist
		\item \underline{Optimization}
\end{enumerate}

\bigskip
In the case of the original FIR filter we have an area of $13240 \mu m^2$ and the
obtainend layout is reported in figure~\ref{fig:fir_layout}.

\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{non_opt-layout.png}
	\caption{Non-optimized FIR layout}
	\label{fig:fir_layout}
\end{figure}
For what concerns the unfolded and pipelined FIR the post route area is of
$27062.3 \mu um^2$ and its layout is reported in figure~\ref{fig:opt_fir_layout}.
\begin{figure}[H]
       \centering
       \includegraphics[width=.4\linewidth]{opt_fir-layout.png}
       \caption{Unfolded and Pipelined FIR layout}
       \label{fig:opt_fir_layout}
\end{figure}
\subsubsection{Post place \& route simulation}
As the physical design is completed, it's possible to generate a further netlist
post place \& route used to verify that the behavior of the design is still the
expected one and also to perform a more accurate power estimation.
The procedure requires again to launch Modelsim and to run a simulation during
which we have to generate a \texttt{vcd} file where the switching activity of
the nodes will be annotated; then going back to Cadence Innovus, we perform a
power analysis using the switching activity recorded.
Table~\ref{tab:pr_pow} displays the obtained values of power consumption.

\begin{table}[H]
	\centering
	\begin{tabular}{lll}
		\hline
		\rowcolor{gray!50}
		& \textbf{Standard FIR} & \textbf{Optimized FIR} \\
		\hline
		Clock period [ns]	& 7.2	& 5.2 \\
		\rowcolor{gray!25}
		Internal Power [mW]	&	& 7.18 \\
		Switching Power [mW]	&	& 4.78 \\
		\rowcolor{gray!25}
		Leakage Power [mW]	&	& 0.5734 \\
		Total Power   [mW]	&	& 12.46 \\
		\hline
	\end{tabular}
	\caption{Post place and route power consumption summary}
	\label{tab:pr_pow} 
\end{table}
 
