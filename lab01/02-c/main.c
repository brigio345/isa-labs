#include <stdio.h>
#include "fir.h"

#define N_ORDER 8
#define N_BITS 11

int main (int argc, char **argv)
{
	FILE *fp_in;
	FILE *fp_out;
	fir_t fir;
	int b[N_ORDER + 1] = {-7, -14, 52, 272, 415, 272, 52, -14, -7};
	int x;
	int y;

	/// check the command line
	if (argc != 3) {
		printf("Use: %s <input_file> <output_file>\n", argv[0]);
		return 1;
	}

	/// open files
	if ((fp_in = fopen(argv[1], "r")) == NULL) {
		printf("Error: cannot open %s\n", argv[1]);
		return 2;
	}
	fp_out = fopen(argv[2], "w");

	if ((fir = fir_alloc(N_ORDER, N_BITS, b)) == NULL) {
		printf("Error: cannot allocate FIR\n");
		return 3;
	};

	/// get samples and apply filter
	while ((fscanf(fp_in, "%d", &x)) == 1) {
		y = fir_sample(fir, x);
		fprintf(fp_out,"%d\n", y);
	}

	fir_free(fir);
	fclose(fp_in);
	fclose(fp_out);

	return 0;
}

