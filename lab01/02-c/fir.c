#include "fir.h"

#include <stdlib.h>

struct fir_s {
	unsigned int 	n_order;
	unsigned int 	n_bits;
	int 		*b;
	int 		*x;
	int 		mask;
};

fir_t fir_alloc(unsigned int n_order, unsigned int n_bits, int *b)
{
	int i;
	fir_t fir;

	if ((n_bits < 1) || (n_bits > (sizeof(int) * 8)))
		return NULL;

	if ((fir = malloc(sizeof(struct fir_s))) == NULL)
		return NULL;

	fir->n_order	= n_order;
	fir->n_bits 	= n_bits;
	if ((fir->b = malloc((n_order + 1) * sizeof(int))) == NULL) {
		free(fir);
		return NULL;
	}
	for (i = 0; i <= n_order; i++)
		fir->b[i] = b[i];
	if ((fir->x = calloc((n_order + 1), sizeof(int))) == NULL) {
		free(fir->b);
		free(fir);
		return NULL;
	}
	// set all to '1' except for n_bits LSBs
	fir->mask	= (-1 << n_bits);

	return fir;
}

void fir_free(fir_t fir)
{
	free(fir->b);
	free(fir->x);
	free(fir);
}

void fir_reset(fir_t fir)
{
	int i;

	for (i = 0; i <= fir->n_order; i++)
		fir->x[i] = 0;
}

int fir_sample(fir_t fir, int x)
{
	int i;
	int y;

	// shift all x samples by 1 position
	for (i = fir->n_order; i > 0; i--)
		fir->x[i] = fir->x[i - 1];
	fir->x[0] = x;

	// compute output at full precision
	// (since coefficients were obtained by: b_int = floor(b_float << (n_bits - 1))
	// products are shifted back to the right to recover original interval)
	y = 0;
	for (i = 0; i <= fir->n_order; i++)
		y += ((fir->b[i] * fir->x[i]) >> (fir->n_bits - 1));

	// truncate y to fir->n_bits bits:
	// if y >= 0 set y MSBs to 0, else set them to 1
	y = (y >= 0) ? (y & (~fir->mask)) : (y | fir->mask);

	return y;
}

