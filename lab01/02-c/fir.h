#ifndef FIR_H
#define FIR_H

typedef struct fir_s *fir_t;

fir_t	fir_alloc(unsigned int n_order, unsigned int n_bits, int *b);
void	fir_free(fir_t fir);
void	fir_reset(fir_t fir);
int	fir_sample(fir_t fir, int x);

#endif /* FIR_H */

